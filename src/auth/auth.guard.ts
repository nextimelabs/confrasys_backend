import {
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import * as jwt from 'jsonwebtoken';
import { EntityEnum } from './auth.decorator';
import { RuoloEnum } from '../core/membro/enums/ruolo.enum';
import { MembroEntity } from '../core/membro/entities/membro.entity';
import { MembroService } from '../core/membro/membro.service';
// @ts-ignore
import * as environment from '../../environment/dev.environment';
import {
  IActiveSubscription,
  SubscriptionPlanTypes,
} from './interfaces/IWordPressSubscriptionResponse';
import { AuthService, ITokenPayload } from './auth.service';
import { getLowerPlan, getPlansByKeys } from '../shared/shared.utils';

const { performance } = require('perf_hooks');

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private reflector: Reflector,
    private membroService: MembroService,
    private authService: AuthService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    const plans = this.reflector.get<SubscriptionPlanTypes[]>(
      'plans',
      context.getHandler(),
    );
    const rolesOrPrivate = this.reflector.get<{
      roles: RuoloEnum[];
      entity: EntityEnum;
    }>('rolesOrPrivate', context.getHandler());
    const rolesAndPrivate = this.reflector.get<{
      roles: RuoloEnum[];
      entity: EntityEnum;
    }>('rolesAndPrivate', context.getHandler());
    const privateRoute = this.reflector.get<boolean>(
      'private',
      context.getHandler(),
    );
    const blockForDemo = this.reflector.get<boolean>(
      'blockForDemo',
      context.getHandler(),
    );
    const guest = this.reflector.get<boolean>('guest', context.getHandler());

    if (guest !== undefined) {
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const headers = request.headers;
    const params = request.params;

    // Se non e' presente la chiave di autorizzazione nell'header allora non si autorizza la richiesta
    if (headers['authorization'] === undefined) {
      Logger.error("Chiave di autenticazione non presente nell'header");
      return false;
    }

    const token = headers['authorization'].split(' ')[1];
    if (!token || token.length == 0) {
      Logger.error("Si e' verificato un errore nella lettura del token");
      return false;
    }

    let payload: ITokenPayload;
    if (
      !(payload = jwt.verify(token, process.env.SECRET || environment.SECRET, {
        ignoreExpiration: true,
      }))
    ) {
      Logger.error('Token non valido');
      return false;
    }

    const loggedUser: MembroEntity = await this.membroService.findOne(
      payload.idMembro,
      payload.idAssociazione,
    );
    const activeSubscription: IActiveSubscription = await this.authService.getActiveSubscription(
      loggedUser.associazione.wpUserId,
    );

    if (!loggedUser)
      throw new HttpException('Utente non trovato', HttpStatus.FORBIDDEN);

    // Controllo se è l'account DEMO
    if (
      blockForDemo &&
      (loggedUser.id == 'aa4f9ca2-66b0-440f-a8e8-e6081288edfb' /* PROD */ ||
        loggedUser.id == 'd5a38c7a-569a-41da-a7de-d8f40b3fa7f3') /* TEST */
    ) {
      throw new HttpException(
        "L'utente DEMO non può effettuare questa operazione.",
        HttpStatus.FORBIDDEN,
      );
    }

    // Roles: se l'utente loggato non fa parte dei ruoli ammessi ritorna false
    // Controllo se il ruolo dell'utente e' incluso nell'elenco dei ruoli a cui e' autorizzata la rotta o se la rotta e' autorizzata a tutti
    if (
      roles &&
      roles.length > 0 &&
      !roles.includes(loggedUser.account.ruolo)
    ) {
      Logger.warn(
        `Il ruolo dell'utente e' non autorizzato alla rotta: [ruoloUtente][ruoliAmmessi] [${loggedUser.account.ruolo}] [${roles}]`,
      );
      return false;
    }

    //rolesOrPrivate: se l'utente loggato non fa parte dei ruoli ammessi e non e' lo stess odel parametro allora ritorna false
    if (rolesOrPrivate) {
      let valido = false;
      Logger.log('Roles or Private');
      //Controllo dei ruoli
      if (
        rolesOrPrivate.roles.length > 0 &&
        !rolesOrPrivate.roles.includes(loggedUser.account.ruolo)
      ) {
        Logger.log("Ruolo dell'utente non autorizzato");
        Logger.warn(
          `Il ruolo dell'utente non e' autorizzato alla rotta oppure non fa parte dei ruoli ammessi: [ruoloUtente][ruoliAmmessi] [${loggedUser.qualifica}] [${roles}]`,
        );
      } else {
        Logger.log("Ruolo dell'utente autorizzato");
        Logger.warn(
          `Il ruolo dell'utente e' autorizzato alla rotta oppure non fa parte dei ruoli ammessi: [ruoloUtente][ruoliAmmessi] [${loggedUser.qualifica}] [${roles}]`,
        );
        valido = true;
      }
      Logger.log("Ruolo dell'utente autorizzato");
      //Controllo proprieta' dell'entita
      Logger.log('Controllo Entita ' + rolesOrPrivate.entity);
      switch (rolesOrPrivate.entity) {
        case EntityEnum.Membro: {
          if (params.id === loggedUser.id) {
            Logger.log('Entita membro autorizzato');
            valido = true;
          }
          break;
        }
        case EntityEnum.Associazione: {
          if (params.id.toString() === loggedUser.associazione.toString()) {
            Logger.log('Entita associazione autorizzato');
            valido = true;
          }
          break;
        }
        case EntityEnum.QuotaAssociativa: {
          if (params.idMembro.toString() === loggedUser.id.toString()) {
            Logger.log('Entita quota associativa autorizzato');
            valido = true;
          }
          break;
        }
      }
      return valido;
    }

    //rolesAndPrivate: se l'utente loggato non fa parte dei ruoli ammessi e non e' lo stesso del parametro allora ritorna false
    if (rolesAndPrivate) {
      Logger.log('Roles And Private');
      //Controllo dei ruoli
      if (
        rolesAndPrivate.roles.length > 0 &&
        !rolesAndPrivate.roles.includes(loggedUser.account.ruolo)
      ) {
        Logger.log("Ruolo dell'utente non autorizzato");
        Logger.warn(
          `Il ruolo dell'utente non e' autorizzato alla rotta oppure non fa parte dei ruoli ammessi: [ruoloUtente][ruoliAmmessi] [${loggedUser.qualifica}] [${rolesAndPrivate.roles}]`,
        );
        return false;
      }
      Logger.log("Ruolo dell'utente autorizzato");
      //Controllo proprieta' dell'entita
      Logger.log('Controllo Entita ' + rolesAndPrivate.entity);
      switch (rolesAndPrivate.entity) {
        case EntityEnum.Membro: {
          if (params.id !== loggedUser.id) {
            Logger.log('Entita membro non autorizzato');
            return false;
          }
          break;
        }
        case EntityEnum.Associazione: {
          if (params.id.toString() !== loggedUser.associazioneId.toString()) {
            Logger.warn(
              `Entita associazione non autorizzato. Id associazione, id associazione utente [${params.id}] [${loggedUser.associazioneId}]`,
            );
            return false;
          }
          break;
        }
        case EntityEnum.QuotaAssociativa: {
          if (params.idMembro.toString() === loggedUser.id.toString()) {
            Logger.log('Entita quota associativa non autorizzato');
            return false;
          }
          break;
        }
      }
    }

    //plans: controlla se l'utente ha un abbonamento attivo di quel piano
    if (plans) {
      if (!plans.includes(activeSubscription.type.type)) {
        Logger.warn(
          `Il piano dell'associazione non e' autorizzato alla rotta: [pianoAssociazione][pianiAmmessi] [${plans}] [${activeSubscription.type.type}]`,
        );
        throw new HttpException(
          {
            code: 'INVALID_PLAN',
            requiredPlan: getLowerPlan(getPlansByKeys(plans)),
          },
          HttpStatus.FORBIDDEN,
        );
      }
    }
    return true;
  }
}
