import { HttpException, Injectable, Logger } from '@nestjs/common';
import { LoginUserDto } from './dto/login-user.dto';
import * as jwt from 'jsonwebtoken';
import * as bcryptjs from 'bcryptjs';
import { MembroEntity } from '../core/membro/entities/membro.entity';
import { Repository } from 'typeorm';
import { AssociazioneEntity } from '../core/associazione/entities/associazione.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { AccountEntity } from '../core/membro/entities/account.entity';
import { StatoEnum } from '../core/membro/enums/stato.enum';
import * as environment from '../../environment/dev.environment.json';
import { HttpService } from '@nestjs/axios';
import {
  IActiveSubscription,
  IWordPressSubscription,
  IWordPressSubscriptionResponse,
  SubscriptionInfo,
  SubscriptionPlans,
} from './interfaces/IWordPressSubscriptionResponse';

export interface ILogin {
  membro: MembroEntity;
  subscription: IWordPressSubscription;
  subscriptionType: SubscriptionInfo;
}

export interface ITokenPayload {
  idMembro: string;
  idAssociazione: string;
  exp: number;
}

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(MembroEntity)
    private membroEntityRepository: Repository<MembroEntity>,
    @InjectRepository(AccountEntity)
    private accountEntityRepository: Repository<AccountEntity>,
    @InjectRepository(AssociazioneEntity)
    private associazioneEntityRepository: Repository<AssociazioneEntity>,
    private httpService: HttpService,
  ) {}

  async login({
    aliasAssociazione,
    email,
    password,
  }: LoginUserDto): Promise<ILogin> {
    let associazioneEntity: AssociazioneEntity = await this.associazioneEntityRepository.findOne(
      {
        alias: aliasAssociazione,
      },
    );
    if (!associazioneEntity)
      throw new HttpException('Alias, email o password errati', 401);

    const activeSubscription: IActiveSubscription = await this.getActiveSubscription(
      associazioneEntity.wpUserId,
    );
    if (!activeSubscription) {
      throw new HttpException({ code: 'Abbonamento non valido' }, 426);
    }

    const accountEntity: AccountEntity = await this.accountEntityRepository
      .createQueryBuilder('account')
      .leftJoinAndSelect('account.membro', 'membro')
      .leftJoinAndSelect('membro.residenza', 'residenza')
      .leftJoinAndSelect('membro.domicilio', 'domicilio')
      .leftJoinAndSelect('membro.account', 'membroAccount')
      .where('membro.associazioneId = :idAssociazione', {
        idAssociazione: associazioneEntity.id,
      })
      .andWhere('account.email = :email', { email: email })
      .andWhere('account.stato NOT IN (:stati) ', {
        stati: [StatoEnum.AttesaDiConferma, StatoEnum.Sospeso],
      })
      .getOne();

    if (!accountEntity) {
      throw new HttpException('Alias, email o password errati', 401);
    }

    if (accountEntity.stato === StatoEnum.AttesaDiConferma) {
      throw new HttpException('Account in attesa di conferma', 400);
    }

    if (await bcryptjs.compare(password, accountEntity.password)) {
      return {
        membro: accountEntity.membro,
        subscription: activeSubscription.subscription,
        subscriptionType: activeSubscription.type,
      };
    } else {
      throw new HttpException('Alias, email o password errati', 401);
    }
  }

  public generateJWT(tokenPayload: ITokenPayload) {
    return jwt.sign(tokenPayload, process.env.SECRET || environment.SECRET);
  }

  async getActiveSubscription(wpUserId: number): Promise<IActiveSubscription> {
    const res = await this.httpService
      .get<IWordPressSubscriptionResponse>(
        `${process.env.WP_SUBSCRIPTION_API ||
          environment.WP_SUBSCRIPTION_API}/${wpUserId}`,
      )
      .toPromise();

    let activeSubscription: {
      subscription: IWordPressSubscription;
      type: SubscriptionInfo;
    } = null;

    if (res.data && res.data.length > 0) {
      const subscriptions: IWordPressSubscriptionResponse = res.data;
      for (const currentSubscription of subscriptions) {
        if (currentSubscription.status === 'active') {
          if (activeSubscription === null) {
            for (const subCode of Object.keys(SubscriptionPlans)) {
              if (
                SubscriptionPlans[subCode].id ==
                currentSubscription.subscription_plan_id
              ) {
                activeSubscription = {
                  subscription: currentSubscription,
                  type: SubscriptionPlans[subCode],
                };
              }
            }
          } else {
            let activeSubscriptionInfo: SubscriptionInfo;
            let currentSubscriptionInfo: SubscriptionInfo;

            Object.keys(SubscriptionPlans).map(subCode => {
              if (
                SubscriptionPlans[subCode].id ==
                currentSubscription.subscription_plan_id
              ) {
                currentSubscriptionInfo = SubscriptionPlans[subCode];
              }
            });
            Object.keys(SubscriptionPlans).map(subCode => {
              if (
                SubscriptionPlans[subCode].id ==
                activeSubscription.subscription.subscription_plan_id
              ) {
                activeSubscriptionInfo = SubscriptionPlans[subCode];
              }
            });
            // Seleziona l'abbonamento più importante
            if (currentSubscriptionInfo.order > activeSubscriptionInfo.order) {
              activeSubscription = {
                subscription: currentSubscription,
                type: currentSubscriptionInfo,
              };
            } else if (
              // Seleziona l'abbonamento più importante
              currentSubscriptionInfo.order === activeSubscriptionInfo.order &&
              currentSubscription.start_date >
                activeSubscription.subscription.start_date
            ) {
              activeSubscription = {
                subscription: currentSubscription,
                type: currentSubscriptionInfo,
              };
            }
          }
        }
      }
    }
    return activeSubscription;
  }

  async generateAutoLoginUrl(wpUserId) {
    const secretkey =
      process.env.WP_TOKEN_SECRET || environment.WP_TOKEN_SECRET;
    const secretBase64 = Buffer.from(
      process.env.WP_TOKEN_SECRET || environment.WP_TOKEN_SECRET,
    ).toString('base64');
    Logger.log(secretkey);
    Logger.log(secretBase64);
    const jwtToken = jwt.sign(
      {
        userId: wpUserId,
      },
      secretkey,
      {
        algorithm: 'HS512',
      },
    );
    const url = `https://www.confrasys.com/?rest_route=/simple-jwt-login/v1/autologin&JWT=${jwtToken}&AUTH_KEY=${process
      .env.WP_API_KEY || environment.WP_API_KEY}`;
    return url;
  }
}
