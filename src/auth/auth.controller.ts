import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { LoginUserDto } from './dto/login-user.dto';
import { AuthService, ILogin, ITokenPayload } from './auth.service';
import { SigninAssociazioneDto } from './dto/signin-associazione.dto';
import { AssociazioneService } from '../core/associazione/associazione.service';
import { MembroService } from '../core/membro/membro.service';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { LoginUserResponseDto } from './dto/login-user-response.dto';
import { MembroConverterDto } from '../core/membro/membro.converter';
import { GetAssociazioneDto } from '../core/associazione/dto/get-associazione.dto';
import { AssociazioneConverterDto } from '../core/associazione/associazione.converter';
import { GetMembroDto } from '../core/membro/dto/get-membro.dto';
import {
  BlockForDemo,
  IdAssociazione,
  IdMembro,
  Private,
} from './auth.decorator';
import { AuthGuard } from './auth.guard';
import {
  ChangePasswordDto,
  ResetPasswordDto,
  SendVerificationCodeDto,
} from './dto/change-password.dto';
import { QuotaAssociativaService } from '../core/quota-associativa/quota-associativa.service';
import { AssociazioneEntity } from '../core/associazione/entities/associazione.entity';
import { EmailAccountsDto } from './dto/email-accounts.dto';
import { IActiveSubscription } from './interfaces/IWordPressSubscriptionResponse';
import { GetCurrentSubscriptionResponseDto } from './dto/get-current-subscription-response.dto';
import { GetAutologinResponseDto } from './dto/get-autologin-response.dto';

@Controller()
@ApiTags('Auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly associazioneService: AssociazioneService,
    private readonly quotaAssociativaService: QuotaAssociativaService,
    private readonly membroService: MembroService,
  ) {}

  @Post('login')
  @HttpCode(200)
  @ApiOkResponse({
    description: 'Registrazione effettuata correttamente',
    type: LoginUserResponseDto,
  })
  async login(@Body() loginUserDto: LoginUserDto): Promise<any> {
    const login: ILogin = await this.authService.login(loginUserDto);

    // Calculate token payload
    let today = new Date();
    let exp = new Date(today);
    exp.setDate(today.getDate() + 60);
    const tokenPayload: ITokenPayload = {
      idMembro: login.membro.id,
      idAssociazione: login.membro.associazioneId,
      exp: exp.getTime() / 1000,
    };
    const token = await this.authService.generateJWT(tokenPayload);

    const res = { membro: await MembroConverterDto(login.membro), token };
    return res;
  }

  @Get('check-alias')
  @HttpCode(200)
  @ApiOkResponse({
    description: 'Alias disponibile',
    type: Boolean,
  })
  async checkAlias(@Query('alias') alias: string) {
    const associazioneEntity = await this.associazioneService.checkAlias(alias);

    if (!associazioneEntity) {
      return true;
    } else {
      throw new HttpException('Alias non disponibile', HttpStatus.BAD_REQUEST);
    }
  }

  @Post('signin')
  @HttpCode(200)
  @ApiOkResponse({
    description: 'Registrazione effettuata correttamente',
    type: GetAssociazioneDto,
  })
  async signinAssociazione(
    @Body() signinAssociazioneDto: SigninAssociazioneDto,
  ): Promise<GetAssociazioneDto> {
    if (
      signinAssociazioneDto.membro.account === undefined ||
      signinAssociazioneDto.membro.account === null
    )
      throw new HttpException(
        'Informazioni account obbligatorie!',
        HttpStatus.BAD_REQUEST,
      );

    let associazione: AssociazioneEntity = {
      ...signinAssociazioneDto.associazione,
      dataIscrizione: new Date().toISOString(),
      statuto: null,
      logo: null,
    };
    const createdAssociazione = await this.associazioneService.create(
      associazione,
    );

    const createdMembro = await this.membroService.create(
      signinAssociazioneDto.membro,
      createdAssociazione.id,
    );

    return AssociazioneConverterDto(createdAssociazione);
  }

  @Post('/change-password')
  @ApiOkResponse({
    description: "Modifica della password dell'utente loggato",
    type: GetMembroDto,
  })
  @Private()
  @BlockForDemo()
  @UseGuards(AuthGuard)
  @HttpCode(200)
  async changePassword(
    @IdMembro() idMembro: string,
    @Body() changePassword: ChangePasswordDto,
  ) {
    const membro = await this.membroService.changePassword(
      idMembro,
      changePassword.oldPassword,
      changePassword.password,
    );
    return await MembroConverterDto(membro);
  }

  @Get('activate-account/:idAccount')
  @ApiOkResponse({
    description: 'Attivazione account',
    type: Boolean,
  })
  @HttpCode(200)
  async activateAccount(
    @Param('idAccount') idAccount: string,
    @Query('code') code,
  ) {
    return await this.membroService.activateAccount(idAccount, code);
  }

  @Get('check-pre-signup/:wpUserId')
  @ApiOkResponse({
    description: 'Controllo pre iscrizione',
    type: Boolean,
  })
  @HttpCode(200)
  async checkPreSignup(@Param('wpUserId') wpUserId: number) {
    return await this.associazioneService.checkPreSignup(wpUserId);
  }

  @Post('email-accounts')
  @HttpCode(200)
  @ApiOkResponse({
    description:
      "Invia tramite email l'elenco di account registrati un determinato indirizzo email",
    type: Boolean,
  })
  async emailAccounts(@Body() emailAccounts: EmailAccountsDto) {
    await this.membroService.emailAccounts(emailAccounts);
    return true;
  }

  @Post('reset-password/send-verification-code')
  @HttpCode(200)
  @ApiOkResponse({
    description: 'Invia tramite email il codice di verifica al membro',
    type: Boolean,
  })
  async sendVerificationCode(
    @Body() sendVerificationCodeDto: SendVerificationCodeDto,
  ) {
    await this.membroService.resetPasswordVerificationCode(
      sendVerificationCodeDto.aliasAssociazione,
      sendVerificationCodeDto.email,
    );
    return true;
  }

  @Post('reset-password/change-password')
  @HttpCode(200)
  @ApiOkResponse({
    description: 'Modifica la password del membro',
    type: Boolean,
  })
  async sendNewPassword(@Body() resetPasswordDto: ResetPasswordDto) {
    await this.membroService.resetPasswordChangePassword(
      resetPasswordDto.aliasAssociazione,
      resetPasswordDto.email,
      resetPasswordDto.verificationCode,
      resetPasswordDto.password,
    );
    return true;
  }

  @Get('subscription')
  @ApiOkResponse({
    description: "Restituisce l'abbonamento attivo dell'associazione",
    type: GetCurrentSubscriptionResponseDto,
  })
  @UseGuards(AuthGuard)
  async getCurrentSubscription(
    @IdAssociazione() idAssociazione: string,
  ): Promise<GetCurrentSubscriptionResponseDto> {
    const associazione = await this.associazioneService.findOne(idAssociazione);
    if (!associazione)
      throw new HttpException('Associazione non trovata', HttpStatus.NOT_FOUND);
    const activeSubscription: IActiveSubscription = await this.authService.getActiveSubscription(
      associazione.wpUserId,
    );
    if (!activeSubscription)
      throw new HttpException(
        'Non è stato trovato alcun abbonamento valido!',
        HttpStatus.NOT_FOUND,
      );
    return {
      title: activeSubscription.type.title,
      type: activeSubscription.type.type,
      startDate: activeSubscription.subscription.start_date,
      expirationDate: activeSubscription.subscription.expiration_date,
      status: activeSubscription.subscription.status,
    };
  }

  @Get('autologin')
  @ApiOkResponse({
    description: 'Restituisce il link di autologin per wordpress',
    type: GetAutologinResponseDto,
  })
  @UseGuards(AuthGuard)
  async getAutoLoginUrl(@IdAssociazione() idAssociazione: string) {
    const associazione = await this.associazioneService.findOne(idAssociazione);
    if (!associazione)
      throw new HttpException('Associazione non trovata', HttpStatus.NOT_FOUND);
    return {
      url: await this.authService.generateAutoLoginUrl(associazione.wpUserId),
    };
  }
}
