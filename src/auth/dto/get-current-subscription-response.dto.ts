import { ApiProperty } from '@nestjs/swagger';
import {
  SubscriptionPlanEnum,
  SubscriptionPlanTypes,
  SubscriptionStatus,
} from '../interfaces/IWordPressSubscriptionResponse';

export class GetCurrentSubscriptionResponseDto {
  @ApiProperty()
  title: string;

  @ApiProperty({ enum: SubscriptionPlanEnum })
  type: SubscriptionPlanTypes;

  @ApiProperty()
  startDate: Date;

  @ApiProperty()
  expirationDate: Date;

  @ApiProperty()
  status: SubscriptionStatus;
}
