import { ApiProperty } from '@nestjs/swagger';
import {
  IsAlphanumeric,
  IsEmail,
  IsNotEmpty,
  MaxLength,
  MinLength,
} from 'class-validator';

export class ChangePasswordDto {
  @ApiProperty()
  @IsNotEmpty()
  @MinLength(7)
  oldPassword: string;

  @ApiProperty()
  @IsNotEmpty()
  @MinLength(7)
  password: string;
}

export class SendVerificationCodeDto {
  @ApiProperty()
  @IsAlphanumeric()
  @MinLength(5)
  @MaxLength(20)
  aliasAssociazione: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  email: string;
}

export class ResetPasswordDto {
  @ApiProperty()
  @IsAlphanumeric()
  @MinLength(5)
  @MaxLength(20)
  aliasAssociazione: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  verificationCode: string;

  @ApiProperty()
  @IsNotEmpty()
  @MinLength(7)
  password: string;
}
