import { IsNotEmpty, ValidateNested } from 'class-validator';
import { CreateAssociazioneDto } from '../../core/associazione/dto/create-associazione.dto';
import { CreateMembroDto } from '../../core/membro/dto/create-membro.dto';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class SigninAssociazioneDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateAssociazioneDto)
  associazione: CreateAssociazioneDto;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateMembroDto)
  membro: CreateMembroDto;
}
