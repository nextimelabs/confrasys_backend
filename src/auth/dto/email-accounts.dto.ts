import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class EmailAccountsDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  email: string;
}
