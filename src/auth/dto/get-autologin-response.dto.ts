import { ApiProperty } from '@nestjs/swagger';

export class GetAutologinResponseDto {
  @ApiProperty()
  url: string;
}
