import { ApiProperty } from '@nestjs/swagger';
import { GetMembroDto } from '../../core/membro/dto/get-membro.dto';

export class LoginUserResponseDto {
  @ApiProperty()
  membro: GetMembroDto;

  @ApiProperty()
  token: string;
}
