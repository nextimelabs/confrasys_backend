import { DynamicModule, Global, HttpModule, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { MembroModule } from '../core/membro/membro.module';
import { AssociazioneModule } from '../core/associazione/associazione.module';
import { QuotaAssociativaModule } from '../core/quota-associativa/quota-associativa.module';
import { AssociazioneEntity } from '../core/associazione/entities/associazione.entity';
import { MembroEntity } from '../core/membro/entities/membro.entity';
import { QuotaAssociativaEntity } from '../core/quota-associativa/entities/quota-associativa.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountEntity } from '../core/membro/entities/account.entity';
import { MembroService } from '../core/membro/membro.service';
import { ResidenzaEntity } from '../core/membro/entities/residenza.entity';
import { DomicilioEntity } from '../core/membro/entities/domicilio.entity';
import * as ormconfig from '../wp-ormconfig';

export function DatabaseOrmModule(): DynamicModule {
  // we could load the configuration from dotEnv here,
  // but typeORM cli would not be able to find the configuration file.

  return TypeOrmModule.forRoot(ormconfig);
}

@Global()
@Module({
  imports: [
    TypeOrmModule.forFeature([
      AssociazioneEntity,
      MembroEntity,
      QuotaAssociativaEntity,
      AccountEntity,
      ResidenzaEntity,
      DomicilioEntity,
    ]),
    AssociazioneModule,
    QuotaAssociativaModule,
    MembroModule,
    HttpModule,
  ],
  providers: [AuthService, MembroService],
  controllers: [AuthController],
  exports: [MembroModule, AuthService],
})
export class AuthModule {}
