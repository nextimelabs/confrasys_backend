import {
  createParamDecorator,
  ExecutionContext,
  HttpException,
  HttpStatus,
  Logger,
  SetMetadata,
} from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { RuoloEnum } from '../core/membro/enums/ruolo.enum';
// @ts-ignore
import * as environment from '../../environment/dev.environment';
import { SubscriptionPlanTypes } from './interfaces/IWordPressSubscriptionResponse';

export enum EntityEnum {
  Membro,
  Associazione,
  Sede,
  QuotaAssociativa,
}

export const Roles = (...roles: string[]) => SetMetadata('roles', roles); //Tutti gli utenti con un determinato ruolo
export const Plans = (...plans: SubscriptionPlanTypes[]) =>
  SetMetadata('plans', plans); //Tutti gli utenti con un determinato ruolo
export const RolesOrPrivate = (auth: {
  roles: RuoloEnum[];
  entity: EntityEnum;
}) => SetMetadata('rolesOrPrivate', auth); //Tutti gli utenti con un determinato ruolo oppure che stia manipolando se stesso (tramite parametro)
export const RolesAndPrivate = (auth: {
  roles: RuoloEnum[];
  entity: EntityEnum;
}) => SetMetadata('rolesAndPrivate', auth); //Tutti gli utenti con un determinato ruolo e che stia manipolando se stesso (tramite parametro)
export const Private = () => SetMetadata('private', true); //Solo gli utenti che devono manipolare se stessi (tramite parametro)
export const BlockForDemo = () => SetMetadata('blockForDemo', true);
export const Guest = () => SetMetadata('guest', null);
export const Logged = () => SetMetadata('logged', null);

export const IdAssociazione = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const headers = ctx.switchToHttp().getRequest().headers;

    // Se non e' presente la chiave di autorizzazione nell'header allora non si autorizza la richiesta
    if (headers['authorization'] === undefined) {
      throw new HttpException(
        { code: 'INVALID_SESSION' },
        HttpStatus.UNAUTHORIZED,
      );
    }

    const token = headers['authorization'].split(' ')[1];
    let payload;
    if (
      !(payload = jwt.verify(token, process.env.SECRET || environment.SECRET, {
        ignoreExpiration: true,
      }))
    ) {
      Logger.error('Token non valido');
      return false;
    }
    return payload.idAssociazione;
  },
);
export const IdMembro = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const headers = ctx.switchToHttp().getRequest().headers;

    // Se non e' presente la chiave di autorizzazione nell'header allora non si autorizza la richiesta
    if (headers['authorization'] === undefined) {
      throw new HttpException(
        { code: 'INVALID_SESSION' },
        HttpStatus.UNAUTHORIZED,
      );
    }

    const token = headers['authorization'].split(' ')[1];
    let payload;
    if (
      !(payload = jwt.verify(token, process.env.SECRET || environment.SECRET, {
        ignoreExpiration: true,
      }))
    ) {
      Logger.error('Token non valido');
      return false;
    }
    return payload.idMembro;
  },
);
