export interface EmailAccountInterface {
  associazione: string;
  aliasAssociazione: string;
  ruolo: string;
}
