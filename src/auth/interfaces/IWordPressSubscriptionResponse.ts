export type SubscriptionPlanTypes = 'BASIC' | 'STANDARD' | 'ADVANCED';
export enum SubscriptionPlanEnum {
  BASIC = 'BASIC',
  STANDARD = 'STANDARD',
  ADVANCED = 'ADVANCED',
}
export type SubscriptionInfo = {
  id: number;
  title: string;
  order: number;
  type: SubscriptionPlanTypes;
};

export type ISubscriptionPlan = {
  [code in SubscriptionPlanTypes]: SubscriptionInfo;
};

export const SubscriptionPlans: ISubscriptionPlan = {
  ADVANCED: {
    id: 265,
    type: 'ADVANCED',
    title: 'Advanced',
    order: 3,
  },
  STANDARD: {
    id: 263,
    type: 'STANDARD',
    title: 'Standard',
    order: 2,
  },
  BASIC: {
    id: 264,
    type: 'BASIC',
    title: 'Base',
    order: 1,
  },
};

export enum SubscriptionStatus {
  active = 'active',
  canceled = 'canceled',
  expired = 'expired',
  pending = 'pending',
  abandoned = 'abandoned',
}
export type IWordPressSubscription = {
  id: number;
  user_id: number;
  start_date: Date;
  expiration_date: Date;
  subscription_plan_id: number;
  status: SubscriptionStatus;
};
export type IWordPressSubscriptionResponse = IWordPressSubscription[];

export type IActiveSubscription = {
  subscription: IWordPressSubscription;
  type: SubscriptionInfo;
};
