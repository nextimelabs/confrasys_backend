import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { config as awsConfig } from 'aws-sdk';
import * as environment from '../environment/dev.environment.json';
import { AllExceptionsFilter } from './shared/exceptions.filter';

declare const module: any;
var moment = require('moment-timezone');
<<<<<<< HEAD
var Rollbar = require('rollbar');
=======
>>>>>>> prod

moment.tz.setDefault('Europe/Rome');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  //app.enableCors();
  const config = new DocumentBuilder()
    .setTitle('Gestionale Associazioni API')
    .setVersion('2.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new AllExceptionsFilter(httpAdapter));

  awsConfig.update({
    region: process.env.REGION || environment.REGION,
    accessKeyId: 'AKIA5ISKNQJZ6IO6U6G3',
    secretAccessKey: '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',
  });

  await app.listen(3000);

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}
bootstrap();
