import { MigrationInterface, QueryRunner } from 'typeorm';

export class fixingDateContratto1672763722530 implements MigrationInterface {
  name = 'fixingDateContratto1672763722530';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "contratti" DROP COLUMN "dataContratto"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD "dataContratto" TIMESTAMP`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "dataContratto" TYPE timestamp without time zone USING "dataContratto"::timestamp`,
    );

    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "dataScadenza" TYPE timestamp without time zone USING "dataScadenza"::timestamp`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" DROP COLUMN "dataScadenza"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD "dataScadenza" TIMESTAMP`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "contratti" DROP COLUMN "dataScadenza"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD "dataScadenza" TIMESTAMP `,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" DROP COLUMN "dataContratto"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD "dataContratto" TIMESTAMP`,
    );
  }
}
