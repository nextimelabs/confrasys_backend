import { MigrationInterface, QueryRunner } from 'typeorm';

export class update1624923109977 implements MigrationInterface {
  name = 'update1624923109977';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "associazioni" ADD "importoQuotaNonResidentiDefault" double precision`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "dataAccettazione"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "dataAccettazione" TIMESTAMP WITH TIME ZONE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "dataAccettazione"`,
    );
    await queryRunner.query(`ALTER TABLE "membri" ADD "dataAccettazione" date`);
    await queryRunner.query(
      `ALTER TABLE "associazioni" DROP COLUMN "importoQuotaNonResidentiDefault"`,
    );
  }
}
