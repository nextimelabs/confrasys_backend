import { MigrationInterface, QueryRunner } from 'typeorm';

export class fixingSezione1673970062893 implements MigrationInterface {
  name = 'fixingSezione1673970062893';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "membri" DROP COLUMN "Sezione"`);
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "sezione" character varying(50)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `COMMENT ON COLUMN "contratti"."tipologiaCappella" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "tipologiaCappella" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "contratti"."tipoContratto" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "tipoContratto" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "contratti"."dataScadenza" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "dataScadenza" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "contratti"."dataContratto" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "dataContratto" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "membri"."condizioniSelected" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ALTER COLUMN "condizioniSelected" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "cappelle"."tipologiaCappella" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "cappelle" ALTER COLUMN "tipologiaCappella" DROP NOT NULL`,
    );
    await queryRunner.query(`ALTER TABLE "membri" DROP COLUMN "sezione"`);
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "Sezione" character varying(50)`,
    );
  }
}
