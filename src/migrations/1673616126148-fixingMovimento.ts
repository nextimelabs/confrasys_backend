import { MigrationInterface, QueryRunner } from 'typeorm';

export class fixingMovimento1673616126148 implements MigrationInterface {
  name = 'fixingMovimento1673616126148';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "movimenti" RENAME COLUMN "dataQuotaAssociativa" TO "annoRiferimento"`,
    );
    await queryRunner.query(
      `ALTER TABLE "quote_associative" DROP COLUMN "dataScadenza"`,
    );
    await queryRunner.query(
      `ALTER TABLE "movimenti" DROP COLUMN "annoRiferimento"`,
    );
    await queryRunner.query(
      `ALTER TABLE "movimenti" ADD "annoRiferimento" numeric`,
    );

    await queryRunner.query(
      `ALTER TABLE "contratti" DROP COLUMN "dataContratto"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD "dataContratto" TIMESTAMP`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "dataContratto" TYPE timestamp without time zone USING "dataContratto"::timestamp`,
    );

    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "dataScadenza" TYPE timestamp without time zone USING "dataScadenza"::timestamp`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" DROP COLUMN "dataScadenza"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD "dataScadenza" TIMESTAMP`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `COMMENT ON COLUMN "contratti"."tipologiaCappella" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "tipologiaCappella" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "contratti"."tipoContratto" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "tipoContratto" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "contratti"."dataScadenza" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "dataScadenza" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "contratti"."dataContratto" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "dataContratto" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "membri"."condizioniSelected" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ALTER COLUMN "condizioniSelected" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "movimenti" DROP COLUMN "annoRiferimento"`,
    );
    await queryRunner.query(
      `ALTER TABLE "movimenti" ADD "annoRiferimento" TIMESTAMP`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "cappelle"."tipologiaCappella" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "cappelle" ALTER COLUMN "tipologiaCappella" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "quote_associative" ADD "dataScadenza" TIMESTAMP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "movimenti" RENAME COLUMN "annoRiferimento" TO "dataQuotaAssociativa"`,
    );
  }
}
