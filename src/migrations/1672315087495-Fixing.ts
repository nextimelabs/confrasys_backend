import { MigrationInterface, QueryRunner } from 'typeorm';

export class Fixing1672315087495 implements MigrationInterface {
  name = 'Fixing1672315087495';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "contratti" DROP CONSTRAINT "FK_ea360ceb9aca1060a9008038345"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" DROP COLUMN "tipoContrattoId"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD "tipoContratto" character varying(30) `,
    );
    await queryRunner.query(
      `ALTER TABLE "cappelle" DROP COLUMN "tipologiaCappella"`,
    );
    await queryRunner.query(
      `ALTER TABLE "cappelle" ADD "tipologiaCappella" character varying(30) `,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "movimenti"."dataQuotaAssociativa" IS NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "membri"."condizioniSelected" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" DROP COLUMN "tipologiaCappella"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD "tipologiaCappella" character varying(30)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "contratti" DROP COLUMN "tipologiaCappella"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD "tipologiaCappella" character varying(255)`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "membri"."condizioniSelected" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ALTER COLUMN "condizioniSelected" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "movimenti"."dataQuotaAssociativa" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "movimenti" ALTER COLUMN "dataQuotaAssociativa" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "cappelle" DROP COLUMN "tipologiaCappella"`,
    );
    await queryRunner.query(
      `ALTER TABLE "cappelle" ADD "tipologiaCappella" character varying(255)`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" DROP COLUMN "tipoContratto"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD "tipoContrattoId" uuid`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD CONSTRAINT "FK_ea360ceb9aca1060a9008038345" FOREIGN KEY ("tipoContrattoId") REFERENCES "cappelle"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }
}
