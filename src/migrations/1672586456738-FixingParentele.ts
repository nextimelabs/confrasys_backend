import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixingParentele1672586456738 implements MigrationInterface {
  name = 'FixingParentele1672586456738';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "membri" DROP CONSTRAINT "FK_1306e46a982657e98ccf7746c41"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP CONSTRAINT "FK_edbe4f70bcff0d58dd494bab9ac"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "paternitaEsternaId"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "maternitaEsternaId"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "coniugeEsternoId"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "paternitaEsterna" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "maternitaEsterna" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "coniugeEsterno" character varying`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `COMMENT ON COLUMN "contratti"."tipologiaCappella" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "tipologiaCappella" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "contratti"."tipoContratto" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "tipoContratto" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "membri"."condizioniSelected" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ALTER COLUMN "condizioniSelected" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "movimenti"."dataQuotaAssociativa" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "movimenti" ALTER COLUMN "dataQuotaAssociativa" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "cappelle"."tipologiaCappella" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "cappelle" ALTER COLUMN "tipologiaCappella" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "coniugeEsterno"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "maternitaEsterna"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "paternitaEsterna"`,
    );
    await queryRunner.query(`ALTER TABLE "membri" ADD "coniugeEsternoId" uuid`);
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "maternitaEsternaId" uuid`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "paternitaEsternaId" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD CONSTRAINT "FK_edbe4f70bcff0d58dd494bab9ac" FOREIGN KEY ("coniugeEsternoId") REFERENCES "membri"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD CONSTRAINT "FK_1306e46a982657e98ccf7746c41" FOREIGN KEY ("maternitaEsternaId") REFERENCES "membri"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
  }
}
