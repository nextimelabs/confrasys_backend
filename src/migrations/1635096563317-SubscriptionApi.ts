import { MigrationInterface, QueryRunner } from 'typeorm';

export class SubscriptionApi1635096563317 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "associazioni" ADD "wpUserId" INT DEFAULT 0`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "associazioni" DROP COLUMN "wpUserId"`,
    );
  }
}
