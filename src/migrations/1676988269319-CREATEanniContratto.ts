import {MigrationInterface, QueryRunner} from "typeorm";

export class CREATEanniContratto1676988269319 implements MigrationInterface {
    name = 'CREATEanniContratto1676988269319'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "contratti" DROP COLUMN "anniContratto"`);
        await queryRunner.query(`ALTER TABLE "contratti" ADD "anniContratto" integer`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`COMMENT ON COLUMN "contratti"."tipologiaCappella" IS NULL`);
        await queryRunner.query(`ALTER TABLE "contratti" ALTER COLUMN "tipologiaCappella" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "contratti"."tipoContratto" IS NULL`);
        await queryRunner.query(`ALTER TABLE "contratti" ALTER COLUMN "tipoContratto" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "contratti"."dataScadenza" IS NULL`);
        await queryRunner.query(`ALTER TABLE "contratti" ALTER COLUMN "dataScadenza" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "contratti"."dataContratto" IS NULL`);
        await queryRunner.query(`ALTER TABLE "contratti" ALTER COLUMN "dataContratto" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "contratti" DROP COLUMN "anniContratto"`);
        await queryRunner.query(`ALTER TABLE "contratti" ADD "anniContratto" numeric`);
        await queryRunner.query(`COMMENT ON COLUMN "membri"."condizioniSelected" IS NULL`);
        await queryRunner.query(`ALTER TABLE "membri" ALTER COLUMN "condizioniSelected" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "membri" DROP COLUMN "dataAccettazione"`);
        await queryRunner.query(`ALTER TABLE "membri" ADD "dataAccettazione" TIMESTAMP`);
        await queryRunner.query(`COMMENT ON COLUMN "cappelle"."tipologiaCappella" IS NULL`);
        await queryRunner.query(`ALTER TABLE "cappelle" ALTER COLUMN "tipologiaCappella" DROP NOT NULL`);
    }

}
