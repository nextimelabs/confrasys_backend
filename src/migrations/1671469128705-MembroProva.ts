import { MigrationInterface, QueryRunner } from 'typeorm';

export class MembroProva1671469128705 implements MigrationInterface {
  name = 'MembroProva1671469128705';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "cappelle" ADD "tipologiaCappella" character varying(255) `,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD "tipologiaCappella" character varying(255) `,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" RENAME COLUMN "tipoContratto" TO "tipoContrattoId"`,
    );
    await queryRunner.query(
      `ALTER TABLE "movimenti" ADD "dataQuotaAssociativa" TIMESTAMP `,
    );
    await queryRunner.query(
      `ALTER TABLE "movimenti" ADD "idMembroTitolare" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "condizioniSelected" character varying array `,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "Sezione" character varying(50)`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "paternitaEsternaId" character varying`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "maternitaEsternaId" uuid`,
    );
    await queryRunner.query(`ALTER TABLE "membri" ADD "coniugeEsternoId" uuid`);
    await queryRunner.query(
      `ALTER TABLE "contratti" DROP COLUMN "tipoContrattoId"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD "tipoContrattoId" uuid`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD CONSTRAINT "FK_1306e46a982657e98ccf7746c41" FOREIGN KEY ("maternitaEsternaId") REFERENCES "membri"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD CONSTRAINT "FK_edbe4f70bcff0d58dd494bab9ac" FOREIGN KEY ("coniugeEsternoId") REFERENCES "membri"("id") ON DELETE SET NULL ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD CONSTRAINT "FK_ea360ceb9aca1060a9008038345" FOREIGN KEY ("tipoContrattoId") REFERENCES "cappelle"("id") ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "contratti" DROP CONSTRAINT "FK_ea360ceb9aca1060a9008038345"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP CONSTRAINT "FK_edbe4f70bcff0d58dd494bab9ac"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP CONSTRAINT "FK_1306e46a982657e98ccf7746c41"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" DROP COLUMN "tipoContrattoId"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ADD "tipoContrattoId" character varying(30) `,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "coniugeEsternoId"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "maternitaEsternaId"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "paternitaEsternaId"`,
    );
    await queryRunner.query(`ALTER TABLE "membri" DROP COLUMN "Sezione"`);
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "condizioniSelected"`,
    );
    await queryRunner.query(
      `ALTER TABLE "movimenti" DROP COLUMN "idMembroTitolare"`,
    );
    await queryRunner.query(
      `ALTER TABLE "movimenti" DROP COLUMN "dataQuotaAssociativa"`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" RENAME COLUMN "tipoContrattoId" TO "tipoContratto"`,
    );
  }
}
