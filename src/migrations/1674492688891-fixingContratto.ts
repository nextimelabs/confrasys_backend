import {MigrationInterface, QueryRunner} from "typeorm";

export class fixingContratto1674492688891 implements MigrationInterface {
    name = 'fixingContratto1674492688891'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "contratti" DROP CONSTRAINT "FK_132366f7aed3b53c6be98fdb73e"`);
        await queryRunner.query(`ALTER TABLE "contratti" DROP COLUMN "nomeId"`);
        await queryRunner.query(`ALTER TABLE "contratti" ADD "nomeCappella" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`COMMENT ON COLUMN "contratti"."tipologiaCappella" IS NULL`);
        await queryRunner.query(`ALTER TABLE "contratti" ALTER COLUMN "tipologiaCappella" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "contratti"."tipoContratto" IS NULL`);
        await queryRunner.query(`ALTER TABLE "contratti" ALTER COLUMN "tipoContratto" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "contratti"."dataScadenza" IS NULL`);
        await queryRunner.query(`ALTER TABLE "contratti" ALTER COLUMN "dataScadenza" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "contratti"."dataContratto" IS NULL`);
        await queryRunner.query(`ALTER TABLE "contratti" ALTER COLUMN "dataContratto" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "membri"."condizioniSelected" IS NULL`);
        await queryRunner.query(`ALTER TABLE "membri" ALTER COLUMN "condizioniSelected" DROP NOT NULL`);
        await queryRunner.query(`COMMENT ON COLUMN "cappelle"."tipologiaCappella" IS NULL`);
        await queryRunner.query(`ALTER TABLE "cappelle" ALTER COLUMN "tipologiaCappella" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "contratti" DROP COLUMN "nomeCappella"`);
        await queryRunner.query(`ALTER TABLE "contratti" ADD "nomeId" uuid`);
        await queryRunner.query(`ALTER TABLE "contratti" ADD CONSTRAINT "FK_132366f7aed3b53c6be98fdb73e" FOREIGN KEY ("nomeId") REFERENCES "cappelle"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

}
