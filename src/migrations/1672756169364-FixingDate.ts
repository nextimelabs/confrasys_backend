import { MigrationInterface, QueryRunner } from 'typeorm';

export class FixingDate1672756169364 implements MigrationInterface {
  name = 'FixingDate1672756169364';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "dataAccettazione"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "dataAccettazione" TIMESTAMP`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "dataProfessione"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "dataProfessione" TIMESTAMP`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `COMMENT ON COLUMN "contratti"."tipologiaCappella" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "tipologiaCappella" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "contratti"."tipoContratto" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "tipoContratto" DROP NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "dataProfessione"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "dataProfessione" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" DROP COLUMN "dataAccettazione"`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ADD "dataAccettazione" TIMESTAMP WITH TIME ZONE`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "membri"."condizioniSelected" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ALTER COLUMN "condizioniSelected" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "movimenti"."dataQuotaAssociativa" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "movimenti" ALTER COLUMN "dataQuotaAssociativa" DROP NOT NULL`,
    );
    await queryRunner.query(
      `COMMENT ON COLUMN "cappelle"."tipologiaCappella" IS NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE "cappelle" ALTER COLUMN "tipologiaCappella" DROP NOT NULL`,
    );
  }
}
