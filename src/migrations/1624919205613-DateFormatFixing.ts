import { MigrationInterface, QueryRunner } from 'typeorm';

export class DateFormatFixing1624919205613 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "associazioni" ALTER COLUMN "dataIscrizione" TYPE timestamp without time zone USING "dataIscrizione"::timestamp`,
    );
    await queryRunner.query(
      `ALTER TABLE "associazioni" ALTER COLUMN "dataFondazione" TYPE timestamp without time zone USING "dataFondazione"::timestamp`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "dataContratto" TYPE timestamp without time zone USING "dataContratto"::timestamp`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "dataScadenza" TYPE timestamp without time zone USING "dataScadenza"::timestamp`,
    );
    await queryRunner.query(
      `ALTER TABLE "movimenti" ALTER COLUMN "dataMovimento" TYPE timestamp without time zone USING "dataMovimento"::timestamp`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ALTER COLUMN "dataDiNascita" TYPE timestamp without time zone USING "dataDiNascita"::timestamp`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ALTER COLUMN "dataAccettazione" TYPE timestamp without time zone USING "dataAccettazione"::timestamp`,
    );
    await queryRunner.query(
      `ALTER TABLE "quote_associative" ALTER COLUMN "dataPagamento" TYPE timestamp without time zone USING "dataPagamento"::timestamp`,
    );
    await queryRunner.query(
      `ALTER TABLE "quote_associative" ALTER COLUMN "dataScadenza" TYPE timestamp without time zone USING "dataScadenza"::timestamp`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "associazioni" ALTER COLUMN "dataIscrizione" TYPE date USING "dataIscrizione"::date`,
    );
    await queryRunner.query(
      `ALTER TABLE "associazioni" ALTER COLUMN "dataFondazione" TYPE date USING "dataFondazione"::date`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "dataContratto" TYPE date USING "dataContratto"::date`,
    );
    await queryRunner.query(
      `ALTER TABLE "contratti" ALTER COLUMN "dataScadenza" TYPE date USING "dataScadenza"::date`,
    );
    await queryRunner.query(
      `ALTER TABLE "movimenti" ALTER COLUMN "dataMovimento" TYPE date USING "dataMovimento"::date`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ALTER COLUMN "dataDiNascita" TYPE date USING "dataDiNascita"::date`,
    );
    await queryRunner.query(
      `ALTER TABLE "membri" ALTER COLUMN "dataAccettazione" TYPE date USING "dataAccettazione"::date`,
    );
    await queryRunner.query(
      `ALTER TABLE "quote_associative" ALTER COLUMN "dataPagamento" TYPE date USING "dataPagamento"::date`,
    );
    await queryRunner.query(
      `ALTER TABLE "quote_associative" ALTER COLUMN "dataScadenza" TYPE date USING "dataScadenza"::date`,
    );
  }
}
