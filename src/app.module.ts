import { DynamicModule, Module } from '@nestjs/common';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import * as ormconfig from './ormconfig';
import * as wpo_rmconfig from './ormconfig';

export function DatabaseOrmModule(): DynamicModule {
  return TypeOrmModule.forRoot(ormconfig);
}
export function DatabaseWPOrmModule(): DynamicModule {
  return TypeOrmModule.forRoot(wpo_rmconfig);
}

@Module({
  imports: [
    DatabaseOrmModule(),
    DatabaseWPOrmModule(),
    SharedModule,
    AuthModule,
    CoreModule,
  ],
})
export class AppModule {}
