// You can load you .env file here synchronously using dotenv package (not installed here),
// import * as dotenv from 'dotenv';
// import * as fs from 'fs';
// const environment = process.env.NODE_ENV || 'development';
// const data: any = dotenv.parse(fs.readFileSync(`${environment}.env`));
// You can also make a singleton service that load and expose the .env file content.
// ...

// Check typeORM documentation for more information.
import * as environment from '../environment/dev.environment.json';
import { ConnectionOptions } from 'typeorm';
import { AssociazioneEntity } from './core/associazione/entities/associazione.entity';
import { CappellaEntity } from './core/cappella/entities/cappella.entity';
import { ContrattoEntity } from './core/contratto/entities/contratto.entity';
import { MovimentoEntity } from './core/libro-giornale/entities/movimento.entity';
import { MembroEntity } from './core/membro/entities/membro.entity';
import { IndirizzoEntity } from './core/membro/entities/indirizzo.entity';
import { ResidenzaEntity } from './core/membro/entities/residenza.entity';
import { DomicilioEntity } from './core/membro/entities/domicilio.entity';
import { AccountEntity } from './core/membro/entities/account.entity';
import { QuotaAssociativaEntity } from './core/quota-associativa/entities/quota-associativa.entity';

const config: ConnectionOptions = {
  type: 'postgres',
  host: process.env.DB_HOST || environment.DB_HOST,
  port: parseInt(process.env.DB_PORT) || environment.DB_PORT,
  username: process.env.DB_USERNAME || environment.DB_USERNAME,
  password: process.env.DB_PASSWORD || environment.DB_PASSWORD,
  database: process.env.DB_NAME || environment.DB_NAME,
  entities: [
    AssociazioneEntity,
    CappellaEntity,
    ContrattoEntity,
    MovimentoEntity,
    MembroEntity,
    IndirizzoEntity,
    ResidenzaEntity,
    DomicilioEntity,
    AccountEntity,
    QuotaAssociativaEntity,
  ],
  synchronize:
    (process.env.DB_SYNCHRONIZE || environment.DB_SYNCHRONIZE) === 'true',
  migrations: [__dirname + '/migrations/**/*{.ts,.js}'],
  cli: {
    migrationsDir: 'src/migrations',
  },
  logging: [],
};

export = config;
