import { Injectable, Logger } from '@nestjs/common';
import * as environment from '../../environment/dev.environment.json';
import { EmailAccountInterface } from '../auth/interfaces/email-accounts.interface';
import SES = require('aws-sdk/clients/ses');

@Injectable()
export class EmailService {
  constructor() {}

  async nuovoAccountMembro(
    email: string,
    nome: string,
    cognome: string,
    ruolo: string,
    alias: string,
    nomeAssociazione: string,
    password: string,
  ) {
    Logger.debug(`${email} - ${nome} - ${cognome} - ${alias} - ${password}`);
    let ses = new SES();
    var params = {
      Destination: {
        ToAddresses: [email],
      },
      Source: process.env.NO_REPLY_ADDRESS || environment.NO_REPLY_ADDRESS,
      Template: 'NuovoAccountMembro',
      TemplateData: JSON.stringify({
        appBaseUrl: process.env.APP_BASE_URL || environment.APP_BASE_URL,
        nomeCognome: `${cognome} ${nome}`,
        nomeAssociazione: nomeAssociazione,
        ruolo: ruolo,
        aliasAssociazione: alias,
        email: email,
        password: password,
      }),
    };
    await ses.sendTemplatedEmail(params).promise();
  }

  async nuovaRegistrazione(
    email: string,
    nome: string,
    cognome: string,
    nomeAssociazione: string,
    idAccount: string,
    verificationCode: string,
    aliasAssociazione: string,
  ) {
    /*
    {
      appBaseUrl: "My New Post",
      nomeCognome: "Luca Pirrone",
      nomeAssociazione: "NexTimeLabs",
      aliasAssociazione: "nextimelabs",
      idAccount: ,
      verificationCode:
    }
     */
    Logger.debug(
      `${email} - ${nome} - ${cognome} - ${nomeAssociazione} - /activate-account/${idAccount}?code=${verificationCode}`,
    );
    let activationLink = `${process.env.APP_BASE_URL ||
      environment.APP_BASE_URL}/user/activate-account/${idAccount}?code=${verificationCode}`;
    let ses = new SES();
    var params = {
      Destination: {
        ToAddresses: [email],
      },
      Source: process.env.NO_REPLY_ADDRESS || environment.NO_REPLY_ADDRESS,
      Template: 'NuovaRegistrazione',
      TemplateData: JSON.stringify({
        appBaseUrl: process.env.APP_BASE_URL || environment.APP_BASE_URL,
        nomeCognome: `${cognome} ${nome}`,
        nomeAssociazione: nomeAssociazione,
        activationLink: activationLink,
        aliasAssociazione: aliasAssociazione,
        email: email,
      }),
    };
    await ses.sendTemplatedEmail(params).promise();
  }

  async emailConfermata(email: string, nome: string, cognome: string) {
    /*
    {
      appBaseUrl: "My New Post",
      nomeCognome: "Luca Pirrone",
    }
     */
    Logger.debug(`${email}`);
    let ses = new SES();
    var params = {
      Destination: {
        ToAddresses: [email],
      },
      Source: process.env.NO_REPLY_ADDRESS || environment.NO_REPLY_ADDRESS,
      Template: 'EmailConfermata',
      TemplateData: JSON.stringify({
        appBaseUrl: process.env.APP_BASE_URL || environment.APP_BASE_URL,
        nomeCognome: `${cognome} ${nome}`,
      }),
    };
    await ses.sendTemplatedEmail(params).promise();
  }

  async resetPasswordSendVerificationCode(
    email: string,
    nome: string,
    cognome: string,
    verificationCode: string,
  ) {
    /*
    {
      appBaseUrl: "My New Post",
      nomeCognome: "Luca Pirrone",
      verificationCode:
    }
     */
    Logger.debug(`${email} - ${verificationCode}`);
    let ses = new SES();
    var params = {
      Destination: {
        ToAddresses: [email],
      },
      Source: process.env.NO_REPLY_ADDRESS || environment.NO_REPLY_ADDRESS,
      Template: 'ResetPasswordCode',
      TemplateData: JSON.stringify({
        appBaseUrl: process.env.APP_BASE_URL || environment.APP_BASE_URL,
        nomeCognome: `${cognome} ${nome}`,
        verificationCode: verificationCode,
      }),
    };
    await ses.sendTemplatedEmail(params).promise();
  }

  async resetPasswordPasswordChanged(
    email: string,
    nome: string,
    cognome: string,
  ) {
    /*
    {
      appBaseUrl: "My New Post",
      nomeCognome: "Luca Pirrone",
    }
     */
    Logger.debug(`${email}`);
    let ses = new SES();
    var params = {
      Destination: {
        ToAddresses: [email],
      },
      Source: process.env.NO_REPLY_ADDRESS || environment.NO_REPLY_ADDRESS,
      Template: 'ResetPasswordChanged',
      TemplateData: JSON.stringify({
        appBaseUrl: process.env.APP_BASE_URL || environment.APP_BASE_URL,
        nomeCognome: `${cognome} ${nome}`,
      }),
    };
    await ses.sendTemplatedEmail(params).promise();
  }

  async sendEmailAccounts(
    email: string,
    emailAccounts: EmailAccountInterface[],
  ) {
    Logger.debug(`${email}`);
    let ses = new SES();
    var params = {
      Destination: {
        ToAddresses: [email],
      },
      Source: process.env.NO_REPLY_ADDRESS || environment.NO_REPLY_ADDRESS,
      Template: 'EmailAccounts',
      TemplateData: JSON.stringify({
        appBaseUrl: process.env.APP_BASE_URL || environment.APP_BASE_URL,
        email: email,
        emailAccounts: emailAccounts,
      }),
    };
    await ses.sendTemplatedEmail(params).promise();
  }
}
