import { ArgumentsHost, Catch } from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';

@Catch()
export class AllExceptionsFilter extends BaseExceptionFilter {
  catch(exception: any, host: ArgumentsHost) {
    super.catch(exception, host);

    /*
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    var rollbar = new Rollbar({
      accessToken: 'dcf4ac7aee6a470298ee71eb2ae97fd5',
      captureUncaught: true,
      captureUnhandledRejections: true
    });

    rollbar.error(ctx, response, request);
     */
  }
}
