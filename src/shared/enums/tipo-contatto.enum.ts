export enum TipoContattoEnum {
  Cellulare = 'Cellulare',
  Email = 'Email',
  Telefono = 'Telefono',
  Fax = 'Fax',
  Altro = 'Altro',
}
export const TipoContatto = ['Cellulare', 'Email', 'Telefono', 'Fax', 'Altro'];
