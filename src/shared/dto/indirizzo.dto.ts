import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsPostalCode, IsString, MaxLength } from 'class-validator';

export class IndirizzoDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  indirizzo: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(10)
  civico: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  comune: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(2)
  provincia: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsPostalCode('IT')
  CAP: string;
}
