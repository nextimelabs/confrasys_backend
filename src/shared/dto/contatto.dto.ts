import { TipoContattoEnum } from '../enums/tipo-contatto.enum';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty } from 'class-validator';

export class ContattoDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(TipoContattoEnum)
  tipo: TipoContattoEnum;

  @ApiProperty()
  @IsNotEmpty()
  valore: string;
}
