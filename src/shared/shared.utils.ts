import {
  SubscriptionInfo,
  SubscriptionPlans,
} from '../auth/interfaces/IWordPressSubscriptionResponse';

export function makeNumericCode(length) {
  var result = '';
  var characters = '0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export function makeRandomPassword(length) {
  return Math.random()
    .toString(36)
    .slice(-length);
}

export function getPlansByKeys(planKeys: string[]) {
  const plans: SubscriptionInfo[] = [];
  for (const code of Object.keys(SubscriptionPlans)) {
    if (planKeys.includes(code)) {
      plans.push(SubscriptionPlans[code]);
    }
  }
  return plans;
}

export function getLowerPlan(plans: SubscriptionInfo[]): SubscriptionInfo {
  return plans.sort((f, s) => {
    if (f.order < s.order) return -1;
    else return 1;
  })[0];
}
export function getHigherPlan(plans: SubscriptionInfo[]): SubscriptionInfo {
  return plans.sort((f, s) => {
    if (f.order > s.order) return -1;
    else return 1;
  })[0];
}
