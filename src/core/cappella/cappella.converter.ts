<<<<<<< HEAD
import { CappellaEntity } from './entities/cappella.entity';
import { GetCappellaDto } from './dto/get-cappella.dto';
=======
import { CappellaEntity } from "./entities/cappella.entity";
import { GetCappellaDto } from "./dto/get-cappella.dto";
>>>>>>> prod

export function CappellaConverterDto(
  cappellaEntity: CappellaEntity,
): GetCappellaDto {
  return {
    id: cappellaEntity.id,
    nome: cappellaEntity.nome,
    numeroOssari: cappellaEntity.numeroOssari,
    numeroLoculi: cappellaEntity.numeroLoculi,
    tipologiaCappella: cappellaEntity.tipologiaCappella,
    /*
    filaDiPartenza: cappellaEntity.filaDiPartenza,
    filaDiArrivo: cappellaEntity.filaDiArrivo,
    postoDiPartenza: cappellaEntity.postoDiPartenza,
    postoDiArrivo: cappellaEntity.postoDiArrivo
    */
  };
}

export function CappelleConverterDto(
  cappelleEntity: CappellaEntity[],
): GetCappellaDto[] {
  let response: GetCappellaDto[] = [];
  for (let i = 0; i < cappelleEntity.length; i++) {
    response.push(CappellaConverterDto(cappelleEntity[i]));
  }
  return response;
}
