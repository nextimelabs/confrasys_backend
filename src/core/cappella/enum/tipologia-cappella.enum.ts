export enum tipologiaCappellaEnum {
  Perpetua = 'Perpetua',
  Confraternita = 'Confraternita',
}
export const tipologiaCappella = ['Perpetua', 'Confraternita'];
