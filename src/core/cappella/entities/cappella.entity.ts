<<<<<<< HEAD
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { AssociazioneEntity } from '../../associazione/entities/associazione.entity';
import { tipologiaCappellaEnum } from '../enums/tipologia-cappella.enum';
=======
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { AssociazioneEntity } from "../../associazione/entities/associazione.entity";
>>>>>>> prod

import { tipologiaCappellaEnum } from "../enum/tipologia-cappella.enum";
@Entity('cappelle')
export class CappellaEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
<<<<<<< HEAD
  @Column('varchar', { length: 255, nullable: false })
  nome: string;
  @Column('numeric', { nullable: false })
  numeroOssari: number;
  @Column('numeric', { nullable: false })
=======
  @Column("varchar", { length: 255, nullable: false })
  nome: string;
  @Column("numeric", { nullable: false })
  numeroOssari: number;
  @Column("numeric", { nullable: false })
>>>>>>> prod
  numeroLoculi: number;
  @Column('varchar', { length: 30, nullable: false })
  tipologiaCappella: tipologiaCappellaEnum;
  /*
  @Column("numeric", {nullable: false})
  filaDiPartenza: number;
  @Column("numeric", {nullable: false})
  filaDiArrivo: number;
  @Column("numeric", {nullable: false})
  postoDiPartenza: number;
  @Column("numeric", {nullable: false})
  postoDiArrivo: number;
  */
<<<<<<< HEAD
  @ManyToOne(
    () => AssociazioneEntity,
    associazione => associazione.id,
    { onDelete: 'CASCADE' },
  )
=======
  @ManyToOne(() => AssociazioneEntity, associazione => associazione.id, { onDelete: "CASCADE" })
>>>>>>> prod
  @JoinColumn()
  associazione?: AssociazioneEntity;
  @Column({ nullable: false })
  associazioneId: string;
}
