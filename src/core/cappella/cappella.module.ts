import { Module } from '@nestjs/common';
import { CappellaService } from './cappella.service';
import { CappellaController } from './cappella.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CappellaEntity } from './entities/cappella.entity';
import { AuthModule } from '../../auth/auth.module';
import { MembroEntity } from '../membro/entities/membro.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CappellaEntity, MembroEntity])],
  controllers: [CappellaController],
  providers: [CappellaService],
})
export class CappellaModule {}
