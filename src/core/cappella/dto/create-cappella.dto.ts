<<<<<<< HEAD
import {
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUUID,
  MaxLength,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { tipologiaCappellaEnum } from '../enums/tipologia-cappella.enum';

=======
import { IsEnum, IsNotEmpty, IsNumber, IsString, IsUUID, MaxLength } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import { tipologiaCappellaEnum } from "../enum/tipologia-cappella.enum";
>>>>>>> prod
export class CreateCappellaDto {
  @ApiProperty({ maxLength: 255 })
  @IsString()
  @IsNotEmpty()
  @MaxLength(255)
  nome: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  numeroOssari: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  numeroLoculi: number;

<<<<<<< HEAD
=======

>>>>>>> prod
  @ApiProperty({ enum: tipologiaCappellaEnum })
  @IsNotEmpty()
  @IsEnum(tipologiaCappellaEnum)
  tipologiaCappella: tipologiaCappellaEnum;
  /*
  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  filaDiPartenza: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  filaDiArrivo: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  postoDiPartenza: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  postoDiArrivo: number;
  */
}
