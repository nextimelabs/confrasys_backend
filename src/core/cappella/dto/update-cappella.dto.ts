import { PartialType, OmitType } from '@nestjs/swagger';
import { CreateCappellaDto } from './create-cappella.dto';

export class UpdateCappellaDto extends PartialType(CreateCappellaDto) {}
