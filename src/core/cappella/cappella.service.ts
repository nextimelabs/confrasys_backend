import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { CreateCappellaDto } from './dto/create-cappella.dto';
import { UpdateCappellaDto } from './dto/update-cappella.dto';
<<<<<<< HEAD
import { InjectRepository } from '@nestjs/typeorm';
import { CappellaEntity } from './entities/cappella.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CappellaService {
  constructor(
    @InjectRepository(CappellaEntity)
    private cappellaRepository: Repository<CappellaEntity>,
  ) {}
=======
import { InjectRepository } from "@nestjs/typeorm"
import { CappellaEntity } from "./entities/cappella.entity";
import { Repository } from "typeorm";

@Injectable()
export class CappellaService {
  constructor(@InjectRepository(CappellaEntity) private cappellaRepository: Repository<CappellaEntity>) {
>>>>>>> prod

  async create(
    createCappellaDto: CreateCappellaDto,
    idAssociazione: string,
  ): Promise<CappellaEntity> {
    const cappellaEntity: CappellaEntity = {
      nome: createCappellaDto.nome,
      numeroOssari: createCappellaDto.numeroOssari,
      numeroLoculi: createCappellaDto.numeroLoculi,
      tipologiaCappella: createCappellaDto.tipologiaCappella,
      /*
      filaDiPartenza: createCappellaDto.filaDiPartenza,
      filaDiArrivo: createCappellaDto.filaDiArrivo,
      postoDiPartenza: createCappellaDto.postoDiPartenza,
      postoDiArrivo: createCappellaDto.postoDiArrivo,
      */
      associazioneId: idAssociazione,
    };
    return this.cappellaRepository.save(cappellaEntity);
  }

<<<<<<< HEAD
  async findAll(idAssociazione: string): Promise<CappellaEntity[]> {
    return this.cappellaRepository
      .createQueryBuilder('cappella')
      .leftJoinAndSelect('cappella.associazione', 'associazione')
      .where('associazione.id = :idAssociazione', { idAssociazione })
      .getMany();
=======
  async findAll(idAssociazione: string, { tipologia }): Promise<CappellaEntity[]> {


    let cappelleQueryBuilder = await this.cappellaRepository
      .createQueryBuilder("cappella")
      .leftJoinAndSelect("cappella.associazione", "associazione")
      .where("associazione.id = :idAssociazione", { idAssociazione });

    if (tipologia)
      cappelleQueryBuilder = cappelleQueryBuilder.andWhere(
        'cappella.tipologiaCappella = :tipologia',
        { tipologia },
      );

    return cappelleQueryBuilder.getMany();

>>>>>>> prod
  }

  async findOne(id: string, idAssociazione: string): Promise<CappellaEntity> {
    const cappellaEntity = await this.cappellaRepository
<<<<<<< HEAD
      .createQueryBuilder('cappella')
      .leftJoinAndSelect('cappella.associazione', 'associazione')
      .where('associazione.id = :idAssociazione', { idAssociazione })
      .where('cappella.id = :id', { id })
=======
      .createQueryBuilder("cappella")
      .leftJoinAndSelect("cappella.associazione", "associazione")
      .where("associazione.id = :idAssociazione", { idAssociazione })
      .where("cappella.id = :id", { id })
>>>>>>> prod
      .getOne();
    if (!cappellaEntity)
      throw new HttpException('Cappella non trovata', HttpStatus.NOT_FOUND);
    return cappellaEntity;
  }

  async update(
    id: string,
    idAssociazione: string,
    updateCappellaDto: UpdateCappellaDto,
  ): Promise<CappellaEntity> {
    let cappellaEntity: CappellaEntity = await this.findOne(id, idAssociazione);
    if (!cappellaEntity)
      throw new HttpException('Cappella non trovata', HttpStatus.NOT_FOUND);

    cappellaEntity = {
      ...cappellaEntity,
      ...updateCappellaDto,
      id: id,
      associazioneId: idAssociazione,
    };
    return this.cappellaRepository.save(cappellaEntity);
  }

  async remove(id: string, idAssociazione: string): Promise<CappellaEntity> {
    let cappellaEntity: CappellaEntity = await this.findOne(id, idAssociazione);
    if (!cappellaEntity)
      throw new HttpException('Cappella non trovata', HttpStatus.NOT_FOUND);

    return this.cappellaRepository.remove(cappellaEntity);
  }
}
