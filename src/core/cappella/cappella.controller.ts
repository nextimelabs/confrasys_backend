<<<<<<< HEAD
import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { CappellaService } from './cappella.service';
import { CreateCappellaDto } from './dto/create-cappella.dto';
import { UpdateCappellaDto } from './dto/update-cappella.dto';
import {
  BlockForDemo,
  IdAssociazione,
  Plans,
  Roles,
} from '../../auth/auth.decorator';
import { RuoloEnum } from '../membro/enums/ruolo.enum';
import { AuthGuard } from '../../auth/auth.guard';
import { GetMembroDto } from '../membro/dto/get-membro.dto';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { GetCappellaDto } from './dto/get-cappella.dto';
import {
  CappellaConverterDto,
  CappelleConverterDto,
} from './cappella.converter';
import { SubscriptionPlanEnum } from '../../auth/interfaces/IWordPressSubscriptionResponse';
=======
import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, Query } from '@nestjs/common';
import { CappellaService } from './cappella.service';
import { CreateCappellaDto } from './dto/create-cappella.dto';
import { UpdateCappellaDto } from './dto/update-cappella.dto';
import { BlockForDemo, IdAssociazione, Plans, Roles } from "../../auth/auth.decorator";
import { RuoloEnum } from "../membro/enums/ruolo.enum";
import { AuthGuard } from "../../auth/auth.guard";
import { GetMembroDto } from "../membro/dto/get-membro.dto";
import { ApiOkResponse, ApiTags } from "@nestjs/swagger";
import { GetCappellaDto } from "./dto/get-cappella.dto";
import { CappellaConverterDto, CappelleConverterDto } from "./cappella.converter";
import { SubscriptionPlanEnum } from "../../auth/interfaces/IWordPressSubscriptionResponse";
import { tipologiaCappellaEnum } from './enum/tipologia-cappella.enum';
>>>>>>> prod

@ApiTags('Cappella')
@Controller('cappelle')
export class CappellaController {
  constructor(private readonly cappellaService: CappellaService) { }

  @Post()
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore)
  @BlockForDemo()
  @Plans(SubscriptionPlanEnum.STANDARD)
  @UseGuards(AuthGuard)
  @ApiOkResponse({
    description: 'Cappella creata correttamente',
    type: GetCappellaDto,
  })
  async create(
    @Body() createCappellaDto: CreateCappellaDto,
    @IdAssociazione() idAssociazione: string,
  ): Promise<GetCappellaDto> {
    const cappellaEntity = await this.cappellaService.create(
      createCappellaDto,
      idAssociazione,
    );
    return CappellaConverterDto(cappellaEntity);
  }

  @Get()
  @Roles(
    RuoloEnum.Amministratore,
    RuoloEnum.Operatore,
    RuoloEnum.Segretario,
    RuoloEnum.Tesoriere,
  )
  @Plans(SubscriptionPlanEnum.STANDARD)
  @UseGuards(AuthGuard)
  @ApiOkResponse({
    description: 'Lista cappelle',
    type: GetCappellaDto,
  })
  async findAll(
    @IdAssociazione() idAssociazione: string,
<<<<<<< HEAD
  ): Promise<GetCappellaDto[]> {
    let cappelleEntity = await this.cappellaService.findAll(idAssociazione);
=======
    @Query('tipologiaCappella') tipologia: tipologiaCappellaEnum

  ): Promise<GetCappellaDto[]> {
    let cappelleEntity = await this.cappellaService.findAll(idAssociazione, { tipologia });
>>>>>>> prod
    return CappelleConverterDto(cappelleEntity);
  }

  @Get(':id')
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore, RuoloEnum.Segretario)
  @Plans(SubscriptionPlanEnum.STANDARD)
  @UseGuards(AuthGuard)
  @ApiOkResponse({
    description: 'Informazione cappella',
    type: GetCappellaDto,
  })
  async findOne(
    @Param('id') id: string,
    @IdAssociazione() idAssociazione: string,
  ): Promise<GetCappellaDto> {
    let cappellaEntity = await this.cappellaService.findOne(id, idAssociazione);
    return CappellaConverterDto(cappellaEntity);
  }

  @Put(':id')
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore)
  @Plans(SubscriptionPlanEnum.STANDARD)
  @BlockForDemo()
  @UseGuards(AuthGuard)
  @ApiOkResponse({
    description: 'Cappella modificata',
    type: GetCappellaDto,
  })
  async update(
    @Param('id') id: string,
    @IdAssociazione() idAssociazione: string,
    @Body() updateCappellaDto: UpdateCappellaDto,
  ): Promise<GetCappellaDto> {
    let cappellaEntity = await this.cappellaService.update(
      id,
      idAssociazione,
      updateCappellaDto,
    );
    return CappellaConverterDto(cappellaEntity);
  }

  @Delete(':id')
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore)
  @Plans(SubscriptionPlanEnum.STANDARD)
  @BlockForDemo()
  @UseGuards(AuthGuard)
  @ApiOkResponse({
    description: 'Cappella eliminata',
    type: GetCappellaDto,
  })
  async remove(
    @Param('id') id: string,
    @IdAssociazione() idAssociazione: string,
  ): Promise<GetCappellaDto> {
    let cappellaEntity = await this.cappellaService.remove(id, idAssociazione);
    return CappellaConverterDto(cappellaEntity);
  }
}
