import { Module } from '@nestjs/common';
import { QuotaAssociativaService } from './quota-associativa.service';
import { QuotaAssociativaController } from './quota-associativa.controller';
import { AssociazioneEntity } from '../associazione/entities/associazione.entity';
import { MembroEntity } from '../membro/entities/membro.entity';
import { QuotaAssociativaEntity } from './entities/quota-associativa.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MembroService } from '../membro/membro.service';
import { MembroModule } from '../membro/membro.module';
import { DomicilioEntity } from '../membro/entities/domicilio.entity';
import { ResidenzaEntity } from '../membro/entities/residenza.entity';
import { MovimentoEntity } from '../libro-giornale/entities/movimento.entity';
import { LibroGiornaleModule } from '../libro-giornale/libro-giornale.module';
import { LibroGiornaleService } from '../libro-giornale/libro-giornale.service';
import { AccountEntity } from '../membro/entities/account.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      AssociazioneEntity,
      MembroEntity,
      DomicilioEntity,
      ResidenzaEntity,
      AccountEntity,
      QuotaAssociativaEntity,
      MovimentoEntity,
    ]),
    MembroModule,
    LibroGiornaleModule,
  ],
  controllers: [QuotaAssociativaController],
  providers: [QuotaAssociativaService, LibroGiornaleService, MembroService],
  exports: [QuotaAssociativaService],
})
export class QuotaAssociativaModule {}
