import { PartialType } from '@nestjs/mapped-types';
import { CreateQuotaAssociativaDto } from './create-quota-associativa.dto';

export class UpdateQuotaAssociativaDto extends PartialType(
  CreateQuotaAssociativaDto,
) {}
