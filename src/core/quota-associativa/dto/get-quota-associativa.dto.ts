import { ApiProperty } from '@nestjs/swagger';
import { IsISO8601, IsMongoId, IsNotEmpty, IsNumber } from 'class-validator';

export class GetQuotaAssociativaDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsMongoId()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsISO8601()
  dataPagamento: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  annoRiferimento: number;
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  importo: number;
}
