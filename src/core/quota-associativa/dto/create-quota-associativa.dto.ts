import * as mongoose from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsISO8601,
  IsMongoId,
  IsNotEmpty,
  IsNumber,
  IsNumberString,
} from 'class-validator';

export class CreateQuotaAssociativaDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsISO8601()
  dataPagamento: string;

  // @ApiProperty()
  // @IsNotEmpty()
  // @IsISO8601()
  // dataScadenza: string;

  @ApiProperty()
  @IsNumber()
  annoRiferimento: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumberString()
  importo: number;

  @ApiProperty()
  @IsNotEmpty()
  idMembroTitolare: string;
}
