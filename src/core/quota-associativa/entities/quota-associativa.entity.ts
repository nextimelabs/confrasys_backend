import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { MembroEntity } from '../../membro/entities/membro.entity';
import { MovimentoEntity } from '../../libro-giornale/entities/movimento.entity';

@Entity('quote_associative')
export class QuotaAssociativaEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column('timestamp without time zone', { nullable: false })
  dataPagamento: string;

  @Column("numeric", {nullable: false})
  annoRiferimento: number;
  @Column('double precision', { nullable: false })
  importo: number;
  @ManyToOne(
    type => MembroEntity,
    membro => membro.id,
    { onDelete: 'CASCADE' },
  )
  membro?: MembroEntity;
  @Column({ nullable: false })
  membroId: string;

  @OneToOne(
    () => MovimentoEntity,
    movimento => movimento.id,
    { onDelete: 'SET NULL' },
  )
  @JoinColumn()
  movimento?: MovimentoEntity;
  @Column({ nullable: true })
  movimentoId: string;
}
