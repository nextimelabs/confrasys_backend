import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { QuotaAssociativaService } from './quota-associativa.service';
import { CreateQuotaAssociativaDto } from './dto/create-quota-associativa.dto';
import { QuotaAssociativaEntity } from './entities/quota-associativa.entity';
import { GetQuotaAssociativaDto } from './dto/get-quota-associativa.dto';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import {
  QuotaAssociativaConverterDto,
  QuoteAssociativeConverterDto,
} from './quota-associativa.converter';
import {
  BlockForDemo,
  EntityEnum,
  IdAssociazione,
  Roles,
  RolesOrPrivate,
} from '../../auth/auth.decorator';
import { AuthGuard } from '../../auth/auth.guard';
import { RuoloEnum } from '../membro/enums/ruolo.enum';
import { MembroService } from '../membro/membro.service';

@Controller('membri/:idMembro/quote-associative')
@ApiTags('Quota Associativa')
export class QuotaAssociativaController {
  constructor(
    private readonly quotaAssociativaService: QuotaAssociativaService,
    private membroService: MembroService,
  ) {}

  @Post()
  @ApiOkResponse({
    description: 'Quota associativa aggiunta correttamente',
    type: GetQuotaAssociativaDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore, RuoloEnum.Segretario)
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async create(
    @Param('idMembro') idMembro: string,
    @IdAssociazione() idAssociazione: string,
    @Body() createQuotaAssociativaDto: CreateQuotaAssociativaDto,
  ): Promise<GetQuotaAssociativaDto> {
    let membro = await this.membroService.findOne(idMembro, idAssociazione);
    if (!membro)
      throw new HttpException('Membro non trovato', HttpStatus.NOT_FOUND);

    return QuotaAssociativaConverterDto(
      await this.quotaAssociativaService.create(
        createQuotaAssociativaDto,
        idMembro,
      ),
    );
  }

  @Get()
  @ApiOkResponse({
    description: 'Lista quote associative di un membro',
    type: [GetQuotaAssociativaDto],
  })
  @RolesOrPrivate({
    roles: [
      RuoloEnum.Amministratore,
      RuoloEnum.Operatore,
      RuoloEnum.Segretario,
    ],
    entity: EntityEnum.QuotaAssociativa,
  })
  @UseGuards(AuthGuard)
  async findAll(
    @Param('idMembro') idMembro: string,
    @IdAssociazione() idAssociazione: string,
  ): Promise<GetQuotaAssociativaDto[]> {
    const quoteAssociative: QuotaAssociativaEntity[] = await this.quotaAssociativaService.findAllQuoteMembro(
      idMembro,
      idAssociazione,
    );
    return QuoteAssociativeConverterDto(quoteAssociative);
  }

  @Get(':id')
  @ApiOkResponse({
    description: 'Restituisce quota associativa di un membro',
    type: GetQuotaAssociativaDto,
  })
  @RolesOrPrivate({
    roles: [
      RuoloEnum.Amministratore,
      RuoloEnum.Operatore,
      RuoloEnum.Segretario,
    ],
    entity: EntityEnum.QuotaAssociativa,
  })
  @UseGuards(AuthGuard)
  async findOne(
    @Param('idMembro') idMembro: string,
    @IdAssociazione() idAssociazione: string,
    @Param('id') id: string,
  ): Promise<GetQuotaAssociativaDto> {
    const quotaAssociativa: QuotaAssociativaEntity = await this.quotaAssociativaService.findOne(
      id,
      idMembro,
      idAssociazione,
    );
    if (!quotaAssociativa)
      throw new HttpException(
        'Quota associativa non trovata',
        HttpStatus.NOT_FOUND,
      );
    return QuotaAssociativaConverterDto(quotaAssociativa);
  }

  @Put(':id')
  @ApiOkResponse({
    description: 'Restituisce quota associativa modificata',
    type: GetQuotaAssociativaDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore, RuoloEnum.Segretario)
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async update(
    @Param('idMembro') idMembro: string,
    @IdAssociazione() idAssociazione: string,
    @Param('id') id: string,
    @Body() updateQuotaAssociativaDto: CreateQuotaAssociativaDto,
  ) {
    const quotaAssociativa: QuotaAssociativaEntity = await this.quotaAssociativaService.update(
      id,
      idMembro,
      idAssociazione,
      updateQuotaAssociativaDto,
    );
    if (!quotaAssociativa)
      throw new HttpException(
        'Quota associativa non trovata',
        HttpStatus.NOT_FOUND,
      );
    return QuotaAssociativaConverterDto(quotaAssociativa);
  }

  @Delete(':id')
  @ApiOkResponse({
    description: 'Eliminazione quota associativa',
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore, RuoloEnum.Segretario)
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async remove(
    @Param('idMembro') idMembro: string,
    @IdAssociazione() idAssociazione: string,
    @Param('id') id: string,
  ) {
    const quotaAssociativa = await this.quotaAssociativaService.remove(
      id,
      idMembro,
      idAssociazione,
    );
    if (!quotaAssociativa)
      throw new HttpException(
        'Quota associativa non trovata',
        HttpStatus.NOT_FOUND,
      );
    return true;
  }
}
