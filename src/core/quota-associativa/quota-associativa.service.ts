import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { QuotaAssociativaEntity } from './entities/quota-associativa.entity';
import { LibroGiornaleService } from '../libro-giornale/libro-giornale.service';
import { CategorieMovimentoEnum } from '../libro-giornale/enums/categorie-movimento.enum';
import { TipiMovimentoEnum } from '../libro-giornale/enums/tipi-movimento.enum';
import { MembroService } from '../membro/membro.service';
import { CreateQuotaAssociativaDto } from './dto/create-quota-associativa.dto';

@Injectable()
export class QuotaAssociativaService {
  constructor(
    @InjectRepository(QuotaAssociativaEntity)
    private quotaAssociativaEntity: Repository<QuotaAssociativaEntity>,
    private membroService: MembroService,
    private libroGiornaleService: LibroGiornaleService,
  ) {}

  async create(
    createQuotaAssociativaDto: CreateQuotaAssociativaDto,
    idMembro: string,
  ): Promise<QuotaAssociativaEntity> {
    //Creo movimento in contabilità
    let membro = await this.membroService.findOneById(idMembro);
    const movimento = await this.libroGiornaleService.create(
      {
        importo: createQuotaAssociativaDto.importo,
        causale: `QUOTA ASSOCIATIVA ANNO ${createQuotaAssociativaDto.annoRiferimento} PAGATO DA ${membro.cognome} ${membro.nome}`,
        dataMovimento: createQuotaAssociativaDto.dataPagamento,
        idMembroTitolare: createQuotaAssociativaDto.idMembroTitolare,
        categoria: CategorieMovimentoEnum.QuotaAssociativa,
        tipo: TipiMovimentoEnum.Entrata,
        annoRiferimento: createQuotaAssociativaDto.annoRiferimento,
      },
      membro.associazioneId,
    );

    const quotaAssociativa: QuotaAssociativaEntity = {
      ...createQuotaAssociativaDto,
      membroId: idMembro,
      membro: membro,
      movimentoId: movimento.id,
      annoRiferimento: movimento.annoRiferimento
    };
    return await this.quotaAssociativaEntity.save(quotaAssociativa);
  }

  async findAllQuoteMembro(
    idMembro: string,
    idAssociazione: string,
  ): Promise<QuotaAssociativaEntity[]> {
    let quoteAssociative = await this.quotaAssociativaEntity
      .createQueryBuilder('quoteAssociative')
      .leftJoinAndSelect('quoteAssociative.membro', 'membro')
      .leftJoinAndSelect('membro.associazione', 'associazione')
      .where('membro.id = :idMembro', { idMembro })
      .andWhere('associazione.id = :idAssociazione', { idAssociazione })
      .getMany();
    return quoteAssociative;
  }

  async findOne(
    id: string,
    idMembro: string,
    idAssociazione: string,
  ): Promise<QuotaAssociativaEntity> {
    let quotaAssociativa = await this.quotaAssociativaEntity
      .createQueryBuilder('quoteAssociative')
      .leftJoinAndSelect('quoteAssociative.membro', 'membro')
      .leftJoinAndSelect('membro.associazione', 'associazione')
      .where('quoteAssociative.id = :id', { id })
      .andWhere('membro.id = :idMembro', { idMembro })
      .andWhere('associazione.id = :idAssociazione', { idAssociazione })
      .getOne();
    return quotaAssociativa;
  }

  async update(
    id: string,
    idMembro: string,
    idAssociazione: string,
    updateQuotaAssociativaDto: CreateQuotaAssociativaDto,
  ): Promise<QuotaAssociativaEntity> {
    let quotaAssociativaEntity = await this.findOne(
      id,
      idMembro,
      idAssociazione,
    );
    if (!quotaAssociativaEntity)
      throw new HttpException(
        'Quota associativa non trovata',
        HttpStatus.NOT_FOUND,
      );
    quotaAssociativaEntity = {
      ...quotaAssociativaEntity,
      ...updateQuotaAssociativaDto,
      id: id,
      membroId: idMembro,
    };
    quotaAssociativaEntity = await this.quotaAssociativaEntity.save(
      quotaAssociativaEntity,
    );

    //Modifico movimento in contabilità
    if (quotaAssociativaEntity.movimentoId) {
      let membro = await this.membroService.findOne(
        quotaAssociativaEntity.membroId,
        idAssociazione,
      );
      await this.libroGiornaleService.update(
        quotaAssociativaEntity.movimentoId,
        idAssociazione,
        {
          importo: quotaAssociativaEntity.importo,
          dataMovimento: quotaAssociativaEntity.dataPagamento,
          causale: `QUOTA ASSOCIATIVA ANNO ${quotaAssociativaEntity.annoRiferimento} PAGATO DA ${membro.cognome} ${membro.nome}`,
        },
      );
    }

    return quotaAssociativaEntity;
  }

  async remove(
    id: string,
    idMembro: string,
    idAssociazione: string,
  ): Promise<QuotaAssociativaEntity> {
    let quotaAssociativaEntity = await this.findOne(
      id,
      idMembro,
      idAssociazione,
    );
    if (!quotaAssociativaEntity)
      throw new HttpException(
        'Quota associativa non trovata',
        HttpStatus.NOT_FOUND,
      );

    // Rimuovo movimento dalla contabilità
    if (quotaAssociativaEntity.movimentoId)
      await this.libroGiornaleService.remove(
        quotaAssociativaEntity.movimentoId,
        idAssociazione,
      );

    await this.quotaAssociativaEntity.remove(quotaAssociativaEntity);
    return quotaAssociativaEntity;
  }
}
