import { GetQuotaAssociativaDto } from './dto/get-quota-associativa.dto';
import { QuotaAssociativaEntity } from './entities/quota-associativa.entity';
import * as moment from 'moment-timezone';

export function QuotaAssociativaConverterDto(
  quotaAssociativa: QuotaAssociativaEntity,
): GetQuotaAssociativaDto {
  return {
    id: quotaAssociativa.id,
<<<<<<< HEAD
    dataPagamento: moment(quotaAssociativa.dataPagamento).format("YYYY-MM-DD"),
=======
    dataPagamento: moment(quotaAssociativa.dataPagamento).format('YYYY-MM-DD'),
>>>>>>> master
    annoRiferimento: quotaAssociativa.annoRiferimento,
    importo: quotaAssociativa.importo,
  };
}
export function QuoteAssociativeConverterDto(
  quoteAssociativa: QuotaAssociativaEntity[],
): GetQuotaAssociativaDto[] {
  let response: GetQuotaAssociativaDto[] = [];
  quoteAssociativa.map((quotaAssociativa: QuotaAssociativaEntity) =>
    response.push(QuotaAssociativaConverterDto(quotaAssociativa)),
  );
  return response;
}
