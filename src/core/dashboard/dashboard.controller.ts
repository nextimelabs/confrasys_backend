import { Controller, Get, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { IdAssociazione, Roles } from '../../auth/auth.decorator';
import { DashboardService } from './dashboard.service';
import { AuthGuard } from '../../auth/auth.guard';
import { RuoloEnum } from '../membro/enums/ruolo.enum';
import { GetContatoriDto } from './dto/get-contatori.dto';
import { GetDirettivoDto } from './dto/get-direttivo.dto';
import { GetStatisticheBilancioDto } from './dto/get-statistiche-bilancio.dto';

@ApiBearerAuth()
@ApiTags('Dashboard')
@Controller('dashboard')
export class DashboardController {
  constructor(private readonly dashboardService: DashboardService) {}

  @Get('contatori')
  @ApiOkResponse({
    description: 'Restituisce i contatori per la dashboard',
    type: GetContatoriDto,
  })
  @UseGuards(AuthGuard)
  async getContatori(@IdAssociazione() idAssociazione: string) {
    let response: GetContatoriDto = {
      membriTotali: null,
      membriAspiranti: null,
      membriMorosi: null,
      totaleFondoCassa: null,
      totaleEntrate: null,
      totaleUscite: null,
    };
    await Promise.all([
      new Promise(async (resolve, reject) => {
        response.membriTotali = await this.dashboardService.getMembriTotali(
          idAssociazione,
        );
        resolve(true);
      }),
      new Promise(async (resolve, reject) => {
        response.membriAspiranti = await this.dashboardService.getMembriAspiranti(
          idAssociazione,
        );
        resolve(true);
      }),
      new Promise(async (resolve, reject) => {
        response.membriMorosi = await this.dashboardService.getMembriMorosi(
          idAssociazione,
        );
        resolve(true);
      }),
      new Promise(async (resolve, reject) => {
        response.totaleFondoCassa = await this.dashboardService.getFondoCassa(
          idAssociazione,
        );
        resolve(true);
      }),
      new Promise(async (resolve, reject) => {
        response.totaleEntrate = await this.dashboardService.getTotaleEntrate(
          idAssociazione,
        );
        resolve(true);
      }),
      new Promise(async (resolve, reject) => {
        response.totaleUscite = await this.dashboardService.getTotaleUscite(
          idAssociazione,
        );
        resolve(true);
      }),
    ]);
    return response;
  }

  @Get('direttivo')
  @ApiOkResponse({
    description: 'Restituisce il direttivo per la dashboard',
    type: GetDirettivoDto,
  })
  @UseGuards(AuthGuard)
  async getDirettivo(@IdAssociazione() idAssociazione: string) {
    return this.dashboardService.getDirettivo(idAssociazione);
  }

  @Get('statistiche-bilancio')
  @ApiOkResponse({
    description: 'Restituisce le statistiche per il bilancio',
    type: GetStatisticheBilancioDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore, RuoloEnum.Tesoriere)
  @UseGuards(AuthGuard)
  async getStatisticheBilancio(@IdAssociazione() idAssociazione: string) {
    return this.dashboardService.getStatisticheBilancio(idAssociazione);
  }
}
