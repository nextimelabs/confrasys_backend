import { Module } from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import { DashboardController } from './dashboard.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MembroEntity } from '../membro/entities/membro.entity';
import { MovimentoEntity } from '../libro-giornale/entities/movimento.entity';
import { AssociazioneModule } from '../associazione/associazione.module';
import { AssociazioneService } from '../associazione/associazione.service';
import { AssociazioneEntity } from '../associazione/entities/associazione.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      MembroEntity,
      MovimentoEntity,
      AssociazioneEntity,
    ]),
    AssociazioneModule,
  ],
  providers: [DashboardService, AssociazioneService],
  controllers: [DashboardController],
  exports: [DashboardService],
})
export class DashboardModule {}
