export class GetDirettivoDto {
  qualifiche: QualificaDirettivo[];
}

class QualificaDirettivo {
  qualifica: string;
  membri: string[];
}
