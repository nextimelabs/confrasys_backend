export class GetStatisticheBilancioDto {
  entrate: {
    categorie: string[];
    valori: number[];
  };
  uscite: {
    categorie: string[];
    valori: number[];
  };
}
