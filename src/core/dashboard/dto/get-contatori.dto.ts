export class GetContatoriDto {
  membriTotali: number;
  membriAspiranti: number;
  membriMorosi: number;

  totaleFondoCassa: number;
  totaleEntrate: number;
  totaleUscite: number;
}
