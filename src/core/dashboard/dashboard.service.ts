<<<<<<< HEAD
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { MembroEntity } from '../membro/entities/membro.entity';
import { MovimentoEntity } from '../libro-giornale/entities/movimento.entity';
import { TipiMovimentoEnum } from '../libro-giornale/enums/tipi-movimento.enum';
import { GetDirettivoDto } from './dto/get-direttivo.dto';
import { AssociazioneService } from '../associazione/associazione.service';
import { Qualifica, QualificaEnum } from '../membro/enums/qualifica.enum';
import { GetStatisticheBilancioDto } from './dto/get-statistiche-bilancio.dto';
import {
  CategorieMovimentoEntrata,
  CategorieMovimentoUscita,
} from '../libro-giornale/enums/categorie-movimento.enum';
=======
import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import S3 = require("aws-sdk/clients/s3");
import { Repository } from "typeorm";
import { InjectRepository } from '@nestjs/typeorm';
import * as environment from "../../../environment/dev.environment.json";
import { MembroEntity } from "../membro/entities/membro.entity";
import { MovimentoEntity } from "../libro-giornale/entities/movimento.entity";
import { TipiMovimentoEnum } from "../libro-giornale/enums/tipi-movimento.enum";
import { GetContatoriDto } from "./dto/get-contatori.dto";
import { GetDirettivoDto } from "./dto/get-direttivo.dto";
import { AssociazioneService } from "../associazione/associazione.service";
import { Qualifica, QualificaEnum } from "../membro/enums/qualifica.enum";
import { GetStatisticheBilancioDto } from "./dto/get-statistiche-bilancio.dto";
import { CategorieMovimentoEntrata, CategorieMovimentoUscita } from "../libro-giornale/enums/categorie-movimento.enum";
>>>>>>> prod

@Injectable()
export class DashboardService {
  constructor(
    @InjectRepository(MembroEntity)
    private membroRepository: Repository<MembroEntity>,
    @InjectRepository(MovimentoEntity)
    private movimentoRepository: Repository<MovimentoEntity>,
    private associazioneService: AssociazioneService,
  ) {}

  async getMembriTotali(idAssociazione: string) {
    const membriTotali = await this.membroRepository
<<<<<<< HEAD
      .createQueryBuilder('membro')
      .where('membro.associazioneId = :idAssociazione', { idAssociazione })
=======
      .createQueryBuilder("membro")
      .where("membro.associazioneId = :idAssociazione", { idAssociazione })
>>>>>>> prod
      .getCount();
    return membriTotali;
  }

  async getMembriAspiranti(idAssociazione: string) {
    const membriAspiranti = await this.membroRepository
<<<<<<< HEAD
      .createQueryBuilder('membro')
      .where('membro.associazioneId = :idAssociazione', { idAssociazione })
      .andWhere('membro.dataAccettazione is null')
=======
      .createQueryBuilder("membro")
      .where("membro.associazioneId = :idAssociazione", { idAssociazione })
      .andWhere("membro.dataAccettazione is null")
>>>>>>> prod
      .getCount();
    return membriAspiranti;
  }

  async getMembriMorosi(idAssociazione: string) {
    const membriMorosi = await this.membroRepository
<<<<<<< HEAD
      .createQueryBuilder("membro")
      .leftJoinAndSelect("membro.quoteAssociative", "quoteAssociative")
      .where("membro.associazioneId = :idAssociazione", { idAssociazione })
      .andWhere("membro.dataAccettazione is not null")
<<<<<<< HEAD
      .andWhere('NOT EXISTS (SELECT * FROM "quote_associative" where "quote_associative"."membroId" = membro.id AND "quote_associative"."dataPagamento" < :data)', {data: new Date()})
=======
      .createQueryBuilder('membro')
      .leftJoinAndSelect('membro.quoteAssociative', 'quoteAssociative')
      .where('membro.associazioneId = :idAssociazione', { idAssociazione })
      .andWhere('membro.dataAccettazione is not null')
      // .andWhere(
      //   'NOT EXISTS (SELECT * FROM "quote_associative" where "quote_associative"."membroId" = membro.id AND "quote_associative"."dataPagamento" )',
      //   { data: new Date() },
      // )
>>>>>>> master
=======
      .andWhere('NOT EXISTS (SELECT * FROM "quote_associative" where "quote_associative"."membroId" = membro.id AND "quote_associative"."dataPagamento" < :data)', { data: new Date() })
>>>>>>> prod
      .getCount();
    return membriMorosi;
  }

  async getFondoCassa(idAssociazione: string) {
    let sommaMovimentiEntrate = await this.movimentoRepository
<<<<<<< HEAD
      .createQueryBuilder('movimento')
      .select('SUM(movimento.importo)', 'somma')
      .where('movimento.associazioneId = :idAssociazione', { idAssociazione })
      .andWhere('movimento.tipo = :tipo', { tipo: TipiMovimentoEnum.Entrata })
      .getRawOne();
    let sommaMovimentiUscite = await this.movimentoRepository
      .createQueryBuilder('movimento')
      .select('SUM(movimento.importo)', 'somma')
      .where('movimento.associazioneId = :idAssociazione', { idAssociazione })
      .andWhere('movimento.tipo = :tipo', { tipo: TipiMovimentoEnum.Uscita })
=======
      .createQueryBuilder("movimento")
      .select("SUM(movimento.importo)", "somma")
      .where("movimento.associazioneId = :idAssociazione", { idAssociazione })
      .andWhere("movimento.tipo = :tipo", { tipo: TipiMovimentoEnum.Entrata })
      .getRawOne();
    let sommaMovimentiUscite = await this.movimentoRepository
      .createQueryBuilder("movimento")
      .select("SUM(movimento.importo)", "somma")
      .where("movimento.associazioneId = :idAssociazione", { idAssociazione })
      .andWhere("movimento.tipo = :tipo", { tipo: TipiMovimentoEnum.Uscita })
>>>>>>> prod
      .getRawOne();
    return sommaMovimentiEntrate.somma - sommaMovimentiUscite.somma;
  }

  async getTotaleEntrate(idAssociazione: string) {
    let sommaMovimentiEntrate = await this.movimentoRepository
<<<<<<< HEAD
      .createQueryBuilder('movimento')
      .select('SUM(movimento.importo)', 'somma')
      .where('movimento.associazioneId = :idAssociazione', { idAssociazione })
      .andWhere('movimento.tipo = :tipo', { tipo: TipiMovimentoEnum.Entrata })
=======
      .createQueryBuilder("movimento")
      .select("SUM(movimento.importo)", "somma")
      .where("movimento.associazioneId = :idAssociazione", { idAssociazione })
      .andWhere("movimento.tipo = :tipo", { tipo: TipiMovimentoEnum.Entrata })
>>>>>>> prod
      .getRawOne();
    return sommaMovimentiEntrate.somma || 0;
  }

  async getTotaleUscite(idAssociazione: string) {
    let sommaMovimentiUscite = await this.movimentoRepository
<<<<<<< HEAD
      .createQueryBuilder('movimento')
      .select('SUM(movimento.importo)', 'somma')
      .where('movimento.associazioneId = :idAssociazione', { idAssociazione })
      .andWhere('movimento.tipo = :tipo', { tipo: TipiMovimentoEnum.Uscita })
=======
      .createQueryBuilder("movimento")
      .select("SUM(movimento.importo)", "somma")
      .where("movimento.associazioneId = :idAssociazione", { idAssociazione })
      .andWhere("movimento.tipo = :tipo", { tipo: TipiMovimentoEnum.Uscita })
>>>>>>> prod
      .getRawOne();
    return sommaMovimentiUscite.somma || 0;
  }

  async getDirettivo(idAssociazione: string) {
    let response: GetDirettivoDto = {
      qualifiche: [],
    };
    let associazione = await this.associazioneService.findOne(idAssociazione);
<<<<<<< HEAD
    await Promise.all(
      Qualifica.map(async qualifica => {
        if (qualifica === QualificaEnum.Socio) return;
        return new Promise(async (resolve, reject) => {
          const membri = await this.membroRepository
            .createQueryBuilder('membro')
            .where('membro.associazioneId = :idAssociazione', {
              idAssociazione,
            })
            .andWhere('membro.qualifica = :qualifica', { qualifica })
            .limit(5)
            .getMany();
          if (membri.length > 0)
            response.qualifiche.push({
              qualifica,
              membri: membri.map(membro => `${membro.nome} ${membro.cognome}`),
            });
          resolve(true);
        });
      }),
    );
=======
    await Promise.all(Qualifica.map(async qualifica => {
      if (qualifica === QualificaEnum.Confrate)
        return;
      return new Promise(async (resolve, reject) => {
        const membri = await this.membroRepository
          .createQueryBuilder("membro")
          .where("membro.associazioneId = :idAssociazione", { idAssociazione })
          .andWhere("membro.qualifica = :qualifica", { qualifica })
          .limit(5)
          .getMany();
        if (membri.length > 0)
          response.qualifiche.push({ qualifica, membri: membri.map(membro => `${membro.nome} ${membro.cognome}`) });
        resolve(true);
      })
    }));
>>>>>>> prod
    return response;
  }

  async getStatisticheBilancio(idAssociazione: string) {
    let response: GetStatisticheBilancioDto = {
      entrate: {
        categorie: [],
        valori: [],
      },
      uscite: {
        categorie: [],
        valori: [],
      },
    };
    await Promise.all([
<<<<<<< HEAD
      await Promise.all(
        CategorieMovimentoEntrata.map(async categoria => {
          let movimentiQueryBuilder = await this.movimentoRepository
            .createQueryBuilder('movimento')
            .select('SUM(movimento.importo)', 'somma')
            .where('movimento.associazioneId = :idAssociazione', {
              idAssociazione,
            })
            .andWhere('movimento.tipo = :tipo', {
              tipo: TipiMovimentoEnum.Entrata,
            })
            .andWhere('movimento.categoria = :categoria', {
              categoria: categoria,
            })
            .getRawOne();
          response.entrate.categorie.push(categoria);
          response.entrate.valori.push(
            movimentiQueryBuilder.somma !== null
              ? movimentiQueryBuilder.somma
              : 0,
          );
        }),
      ),
      await Promise.all(
        CategorieMovimentoUscita.map(async categoria => {
          let movimentiQueryBuilder = await this.movimentoRepository
            .createQueryBuilder('movimento')
            .select('SUM(movimento.importo)', 'somma')
            .where('movimento.associazioneId = :idAssociazione', {
              idAssociazione,
            })
            .andWhere('movimento.tipo = :tipo', {
              tipo: TipiMovimentoEnum.Uscita,
            })
            .andWhere('movimento.categoria = :categoria', {
              categoria: categoria,
            })
            .getRawOne();
          response.uscite.categorie.push(categoria);
          response.uscite.valori.push(
            movimentiQueryBuilder.somma !== null
              ? movimentiQueryBuilder.somma
              : 0,
          );
        }),
      ),
=======
      await Promise.all(CategorieMovimentoEntrata.map(async (categoria) => {
        let movimentiQueryBuilder = await this.movimentoRepository
          .createQueryBuilder("movimento")
          .select("SUM(movimento.importo)", "somma")
          .where("movimento.associazioneId = :idAssociazione", { idAssociazione })
          .andWhere("movimento.tipo = :tipo", { tipo: TipiMovimentoEnum.Entrata })
          .andWhere("movimento.categoria = :categoria", { categoria: categoria })
          .getRawOne();
        response.entrate.categorie.push(categoria);
        response.entrate.valori.push(movimentiQueryBuilder.somma !== null ? movimentiQueryBuilder.somma : 0);
      })),
      await Promise.all(CategorieMovimentoUscita.map(async (categoria) => {
        let movimentiQueryBuilder = await this.movimentoRepository
          .createQueryBuilder("movimento")
          .select("SUM(movimento.importo)", "somma")
          .where("movimento.associazioneId = :idAssociazione", { idAssociazione })
          .andWhere("movimento.tipo = :tipo", { tipo: TipiMovimentoEnum.Uscita })
          .andWhere("movimento.categoria = :categoria", { categoria: categoria })
          .getRawOne();
        response.uscite.categorie.push(categoria);
        response.uscite.valori.push(movimentiQueryBuilder.somma !== null ? movimentiQueryBuilder.somma : 0);
      }))
>>>>>>> prod
    ]);
    return response;
  }
}
