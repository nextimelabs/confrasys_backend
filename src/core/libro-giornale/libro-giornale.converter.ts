<<<<<<< HEAD
import * as moment from 'moment-timezone';
import { MovimentoEntity } from './entities/movimento.entity';
import { GetMovimentoDto } from './dto/get-movimento.dto';
=======
import * as moment from "moment-timezone";
import { MovimentoEntity } from "./entities/movimento.entity";
import { GetMovimentoDto } from "./dto/get-movimento.dto";
>>>>>>> prod

export function MovimentoConverterDto(
  movimento: MovimentoEntity,
): GetMovimentoDto {
  return {
    id: movimento.id,
    importo: movimento.importo,
    dataMovimento: moment(movimento.dataMovimento).format('YYYY-MM-DD'),
    causale: movimento.causale,
    categoria: movimento.categoria,
    tipo: movimento.tipo,
    annoRiferimento: movimento.annoRiferimento,
    idMembroTitolare: movimento.idMembroTitolare,
<<<<<<< HEAD
  };
=======
  }
>>>>>>> prod
}
export function MovimentiConverterDto(
  movimenti: MovimentoEntity[],
): GetMovimentoDto[] {
  let response: GetMovimentoDto[] = [];
  movimenti.map((movimento: MovimentoEntity) =>
    response.push(MovimentoConverterDto(movimento)),
  );
  return response;
}
