import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateMovimentoDto } from './dto/create-movimento.dto';
import { UpdateMovimentoDto } from './dto/update-movimento.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { MovimentoEntity } from './entities/movimento.entity';
import { Repository } from 'typeorm';
import { FilterMovimentiDto } from './dto/filter-movimenti.dto';

@Injectable()
export class LibroGiornaleService {
  constructor(
    @InjectRepository(MovimentoEntity)
    private movimentoRepository: Repository<MovimentoEntity>,
  ) {}

  async create(
    createMovimentoDto: CreateMovimentoDto,
    idAssociazione: string,
  ): Promise<MovimentoEntity> {
    const movimentoEntity: MovimentoEntity = {
      ...createMovimentoDto,
      associazioneId: idAssociazione,
      annoRiferimento: createMovimentoDto.annoRiferimento,
    };
    return this.movimentoRepository.save(movimentoEntity);
  }

  async findAll(
    idAssociazione: string,
    {
      tipo,
      importoMin,
      importoMax,
      dataMovimentoMin,
      dataMovimentoMax,
      categorie,
    }: FilterMovimentiDto,
  ): Promise<MovimentoEntity[]> {
    let movimentiQueryBuilder = this.movimentoRepository
      .createQueryBuilder('movimento')
      .where('movimento.associazioneId = :idAssociazione', { idAssociazione });

    if (tipo)
      movimentiQueryBuilder = movimentiQueryBuilder.andWhere(
        'movimento.tipo = :tipo',
        { tipo },
      );
    if (importoMin)
      movimentiQueryBuilder = movimentiQueryBuilder.andWhere(
        'movimento.importo >= :importoMin',
        { importoMin },
      );
    if (importoMax)
      movimentiQueryBuilder = movimentiQueryBuilder.andWhere(
        'movimento.importo <= :importoMax',
        { importoMax },
      );
    if (dataMovimentoMin)
      movimentiQueryBuilder = movimentiQueryBuilder.andWhere(
        'movimento.dataMovimento >= :dataMovimentoMin',
        { dataMovimentoMin },
      );
    if (dataMovimentoMax)
      movimentiQueryBuilder = movimentiQueryBuilder.andWhere(
        'movimento.dataMovimento <= :dataMovimentoMax',
        { dataMovimentoMax },
      );
    if (categorie)
      movimentiQueryBuilder = movimentiQueryBuilder.andWhere(
        'movimento.categoria in (:...categorie)',
        { categorie: categorie.split(',') },
      );

    return movimentiQueryBuilder.getMany();
  }

  async findOne(id: string, idAssociazione: string): Promise<MovimentoEntity> {
    return this.movimentoRepository
      .createQueryBuilder('movimento')
      .where('movimento.id = :id', { id })
      .andWhere('movimento.associazioneId = :idAssociazione', {
        idAssociazione,
      })
      .getOne();
  }

  async update(
    id: string,
    idAssociazione: string,
    updateMovimentoDto: UpdateMovimentoDto,
  ): Promise<MovimentoEntity> {
    let movimentoEntity = await this.findOne(id, idAssociazione);

    if (!movimentoEntity)
      throw new HttpException('Movimento non trovato', HttpStatus.NOT_FOUND);
    return this.movimentoRepository.save({
      ...movimentoEntity,
      ...updateMovimentoDto,
      id: movimentoEntity.id,
      associazioneId: movimentoEntity.associazioneId,
    });
  }

  async remove(id: string, idAssociazione: string): Promise<MovimentoEntity> {
    let movimentoEntity = await this.findOne(id, idAssociazione);
    if (!movimentoEntity)
      throw new HttpException('Movimento non trovato', HttpStatus.NOT_FOUND);
    return this.movimentoRepository.remove(movimentoEntity);
  }
}
