import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CategorieMovimentoEnum } from '../enums/categorie-movimento.enum';
import { TipiMovimentoEnum } from '../enums/tipi-movimento.enum';
import { AssociazioneEntity } from '../../associazione/entities/associazione.entity';
import { MembroEntity } from '../../membro/entities/membro.entity';

@Entity('movimenti')
export class MovimentoEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column('double precision', { nullable: false })
  importo: number;
  @Column('varchar', { nullable: false, length: 100 })
  causale: string;
  @Column('timestamp without time zone', { nullable: false })
  dataMovimento: string;
  @Column('varchar', { nullable: false })
  categoria: CategorieMovimentoEnum;
  @Column('varchar', { nullable: false })
  tipo: TipiMovimentoEnum;
  @Column('numeric', { nullable: true })
  annoRiferimento: number;

  @JoinColumn()
  membroTitolare?: MembroEntity;
  @Column({ nullable: true })
  idMembroTitolare: string;
  @ManyToOne(
    type => MembroEntity,
    membro => membro.id,
    { onDelete: 'SET NULL' },
  )
  @ManyToOne(
    type => AssociazioneEntity,
    associazione => associazione.id,
    { onDelete: 'CASCADE' },
  )
  @JoinColumn()
  associazione?: AssociazioneEntity;
  @Column({ nullable: false })
  associazioneId: string;
}
