import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { LibroGiornaleService } from './libro-giornale.service';
import { CreateMovimentoDto } from './dto/create-movimento.dto';
import { UpdateMovimentoDto } from './dto/update-movimento.dto';
import { BlockForDemo, IdAssociazione, Roles } from '../../auth/auth.decorator';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { GetMovimentoDto } from './dto/get-movimento.dto';
import {
  MovimentiConverterDto,
  MovimentoConverterDto,
} from './libro-giornale.converter';
import { RuoloEnum } from '../membro/enums/ruolo.enum';
import { AuthGuard } from '../../auth/auth.guard';
import { FilterMovimentiDto } from './dto/filter-movimenti.dto';

@Controller('libro-giornale')
@ApiTags('Libro Giornale')
export class LibroGiornaleController {
  constructor(private readonly libroGiornaleService: LibroGiornaleService) {}

  @Post('movimenti')
  @ApiOkResponse({
    description: 'Movimento aggiunto correttamente',
    type: GetMovimentoDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore, RuoloEnum.Tesoriere)
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async create(
    @Body() createMovimentoDto: CreateMovimentoDto,
    @IdAssociazione() idAssociazione: string,
  ): Promise<GetMovimentoDto> {
    const movimentoEntity = await this.libroGiornaleService.create(
      createMovimentoDto,
      idAssociazione,
    );
    return MovimentoConverterDto(movimentoEntity);
  }

  @Get('movimenti')
  @ApiOkResponse({
    description: 'Restituisce lista movimenti',
    type: [GetMovimentoDto],
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore, RuoloEnum.Tesoriere)
  @UseGuards(AuthGuard)
  async findAll(
    @IdAssociazione() idAssociazione: string,
    @Query() filter: FilterMovimentiDto,
  ): Promise<GetMovimentoDto[]> {
    Logger.log(typeof filter.categorie);
    Logger.log(filter.categorie);
    const movimentiEntity = await this.libroGiornaleService.findAll(
      idAssociazione,
      filter,
    );
    return MovimentiConverterDto(movimentiEntity);
  }

  @Get('movimenti/:id')
  @ApiOkResponse({
    description: 'Restituisce movimento da id',
    type: GetMovimentoDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore, RuoloEnum.Tesoriere)
  @UseGuards(AuthGuard)
  async findOne(
    @Param('id') id: string,
    @IdAssociazione() idAssociazione: string,
  ): Promise<GetMovimentoDto> {
    const movimentoEntity = await this.libroGiornaleService.findOne(
      id,
      idAssociazione,
    );
    return MovimentoConverterDto(movimentoEntity);
  }

  @Put('movimenti/:id')
  @ApiOkResponse({
    description: 'Restituisce movimento modificato',
    type: GetMovimentoDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore, RuoloEnum.Tesoriere)
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async update(
    @Param('id') id: string,
    @Body() updateMovimentoDto: UpdateMovimentoDto,
    @IdAssociazione() idAssociazione: string,
  ): Promise<GetMovimentoDto> {
    const movimentoEntity = await this.libroGiornaleService.update(
      id,
      idAssociazione,
      updateMovimentoDto,
    );
    return MovimentoConverterDto(movimentoEntity);
  }

  @Delete('movimenti/:id')
  @ApiOkResponse({
    description: 'Restituisce movimento eliminato',
    type: GetMovimentoDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore, RuoloEnum.Tesoriere)
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async remove(
    @Param('id') id: string,
    @IdAssociazione() idAssociazione: string,
  ): Promise<GetMovimentoDto> {
    const movimentoEntity = await this.libroGiornaleService.remove(
      id,
      idAssociazione,
    );
    return MovimentoConverterDto(movimentoEntity);
  }
}
