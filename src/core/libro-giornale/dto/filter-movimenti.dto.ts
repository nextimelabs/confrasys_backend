import { TipiMovimentoEnum } from '../enums/tipi-movimento.enum';

export class FilterMovimentiDto {
  tipo?: TipiMovimentoEnum;
  importoMin?: number;
  importoMax?: number;
  dataMovimentoMin?: string;
  dataMovimentoMax?: string;
  categorie?: string;
}
