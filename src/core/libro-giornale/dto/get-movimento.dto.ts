<<<<<<< HEAD
import { ApiProperty } from '@nestjs/swagger';
import {
  IsEnum,
  IsISO8601,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUUID,
  MaxLength,
} from 'class-validator';
import { CategorieMovimentoEnum } from '../enums/categorie-movimento.enum';
import { TipiMovimentoEnum } from '../enums/tipi-movimento.enum';
=======
import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { IsEnum, IsISO8601, IsNotEmpty, IsNumber, IsString, IsUUID, MaxLength } from "class-validator";
import { CategorieMovimentoEnum } from "../enums/categorie-movimento.enum";
import { TipiMovimentoEnum } from "../enums/tipi-movimento.enum";
>>>>>>> prod

export class GetMovimentoDto {
  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  id: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  importo: number;

  @ApiProperty({ maxLength: 100 })
  @IsString()
  @MaxLength(100)
  @IsNotEmpty()
  causale: string;

  @ApiProperty()
  @IsISO8601()
  @IsNotEmpty()
  dataMovimento: string;



  @ApiProperty()
  @IsNumber()
  annoRiferimento: number;

  @ApiProperty()
  idMembroTitolare: string;


  @ApiProperty()
  @IsEnum(CategorieMovimentoEnum)
  @IsNotEmpty()
  categoria: CategorieMovimentoEnum;

  @ApiProperty()
  @IsEnum(TipiMovimentoEnum)
  @IsNotEmpty()
  tipo: TipiMovimentoEnum;

  @ApiProperty()
  idMembroTitolare: string;

  @ApiProperty()
  @IsNumber()
  annoRiferimento: number;
}
