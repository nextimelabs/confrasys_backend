import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsEnum,
  IsISO8601,
  IsNotEmpty,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsString,
  IsUUID,
  MaxLength,
} from 'class-validator';
import { CategorieMovimentoEnum } from '../enums/categorie-movimento.enum';
import { TipiMovimentoEnum } from '../enums/tipi-movimento.enum';

export class CreateMovimentoDto {
  @ApiProperty()
  @IsNumberString()
  @IsNotEmpty()
  importo: number;

  @ApiProperty()
  @IsEnum(TipiMovimentoEnum)
  @IsNotEmpty()
  tipo: TipiMovimentoEnum;

  @ApiProperty({ maxLength: 100 })
  @IsString()
  @MaxLength(100)
  @IsNotEmpty()
  causale: string;

  @ApiProperty()
  @IsEnum(CategorieMovimentoEnum)
  @IsNotEmpty()
  categoria: CategorieMovimentoEnum;

  @ApiProperty()
  @IsISO8601()
  @IsNotEmpty()
  dataMovimento: string;

  @ApiProperty()
  @IsNumber()
  annoRiferimento: number;

  @ApiProperty()
  idMembroTitolare: string;
}
