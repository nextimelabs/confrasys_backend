import { Module } from '@nestjs/common';
import { LibroGiornaleService } from './libro-giornale.service';
import { LibroGiornaleController } from './libro-giornale.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MovimentoEntity } from './entities/movimento.entity';
import { MembroEntity } from '../membro/entities/membro.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MovimentoEntity, MembroEntity])],
  controllers: [LibroGiornaleController],
  providers: [LibroGiornaleService],
  exports: [LibroGiornaleService],
})
export class LibroGiornaleModule {}
