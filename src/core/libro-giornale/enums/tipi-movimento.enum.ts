export enum TipiMovimentoEnum {
  Entrata = 'Entrata',
  Uscita = 'Uscita',
}
export const TipiMovimento = ['Entrata', 'Uscita'];
