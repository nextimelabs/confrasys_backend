export enum CategorieMovimentoEnum {
  QuotaAssociativa = 'Quota Associativa',
  Contratto = 'Contratto',
  Donazione = 'Donazione',

  Utenza = 'Utenza',

  Altro = 'Altro',
}
export const CategorieMovimento = [
  'Quota Associativa',
  'Contratto',
  'Donazione',
  'Utenza',
  'Altro',
];

export enum CategorieMovimentoEntrataEnum {
  QuotaAssociativa = 'Quota Associativa',
  Contratto = 'Contratto',
  Donazione = 'Donazione',
  Altro = 'Altro',
}
export const CategorieMovimentoEntrata = [
  'Quota Associativa',
  'Contratto',
  'Donazione',
  'Altro',
];

export enum CategorieMovimentoUscitaEnum {
  Contratto = 'Contratto',
  Utenza = 'Utenza',
  Donazione = 'Donazione',
  Altro = 'Altro',
}
export const CategorieMovimentoUscita = [
  'Contratto',
  'Utenza',
  'Donazione',
  'Altro',
];
