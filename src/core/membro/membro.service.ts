import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { MembroEntity } from './entities/membro.entity';
import { makeNumericCode, makeRandomPassword } from '../../shared/shared.utils';
import * as bcryptjs from 'bcryptjs';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { AccountEntity } from './entities/account.entity';
import { DomicilioEntity } from './entities/domicilio.entity';
import { ResidenzaEntity } from './entities/residenza.entity';
import { RuoloEnum } from './enums/ruolo.enum';
import { CreateMembroDto } from './dto/create-membro.dto';
import { UpdateMembroDto } from './dto/update-membro.dto';
import { FilterTypeEnum } from './enums/filter-type.enum';
import { StatoEnum } from './enums/stato.enum';
import { EmailService } from '../../shared/email.service';
import { EmailAccountsDto } from '../../auth/dto/email-accounts.dto';
import { EmailAccountInterface } from '../../auth/interfaces/email-accounts.interface';

@Injectable()
export class MembroService {
  constructor(
    @InjectRepository(MembroEntity)
    private membroModel: Repository<MembroEntity>,
    @InjectRepository(ResidenzaEntity)
    private residenzaEntityRepository: Repository<ResidenzaEntity>,
    @InjectRepository(DomicilioEntity)
    private domicilioEntityRepository: Repository<DomicilioEntity>,
    @InjectRepository(AccountEntity)
    private accountEntityRepository: Repository<AccountEntity>,
    private emailService: EmailService,
  ) {}

  async create(
    createMembroDto: CreateMembroDto,
    idAssociazione: string,
  ): Promise<MembroEntity> {
    let membroEntity: MembroEntity = {
      numero: await this.getNextNumero(idAssociazione),
      nome: createMembroDto.nome,
      cognome: createMembroDto.cognome,
      genere: createMembroDto.genere,
      dataDiNascita: createMembroDto.dataDiNascita,
      provinciaDiNascita: createMembroDto.provinciaDiNascita,
      comuneDiNascita: createMembroDto.comuneDiNascita,
      codiceFiscale: createMembroDto.codiceFiscale,
      residente: createMembroDto.residente,
      vedovanza: createMembroDto.vedovanza,
      deceduto: createMembroDto.deceduto,
      condizioniSelected: createMembroDto.condizioniSelected,
      dataAccettazione: createMembroDto.dataAccettazione,
      dataProfessione: createMembroDto.dataProfessione,
      qualifica: createMembroDto.qualifica,
      email: createMembroDto.email,
      telefono: createMembroDto.telefono,
      cellulare: createMembroDto.cellulare,
      padreId: createMembroDto.idMembroPadre,
      madreId: createMembroDto.idMembroMadre,
      coniugeId: createMembroDto.idMembroConiuge,
      sezione: createMembroDto.sezione,
      paternitaEsterna: createMembroDto.paternitaEsterna,
      maternitaEsterna: createMembroDto.maternitaEsterna,
      coniugeEsterno: createMembroDto.coniugeEsterno,
      associazioneId: idAssociazione,
    };
    await this.membroModel.save(membroEntity);

    membroEntity = await this.findOneById(membroEntity.id);

    const residenzaEntity = await this.createResidenza({
      ...createMembroDto.residenza,
      membro: membroEntity,
    });
    const domicilioEntity = await this.createDomicilio({
      ...createMembroDto.domicilio,
      membro: membroEntity,
    });

    // Se è richiesta la creazione dell'account per il membro
    if (createMembroDto.account) {
      // Controllo l'esistenza della stessa email all'interno dell'associazione
      const controlloMembro = await this.findOneByEmail(
        createMembroDto.account.email,
        idAssociazione,
      );
      if (controlloMembro)
        throw new HttpException('Email già utilizzata', HttpStatus.BAD_REQUEST);

      let accountEntity = {
        ...createMembroDto.account,
        email: createMembroDto.account.email,
        oneTimeCode: null,
        membro: membroEntity,
        ruolo: createMembroDto.account.ruolo,
        stato: null,
      };

      if (
        createMembroDto.account &&
        (createMembroDto.account.password === '' ||
          createMembroDto.account.password === null ||
          createMembroDto.account.password === undefined)
      ) {
        // La creazione dell'account e' stata effettuata da un amministratore
        let password = makeRandomPassword(10);
        Logger.log(password);
        accountEntity = {
          ...accountEntity,
          password: await bcryptjs.hash(password, 10),
          stato: StatoEnum.PrimoAccesso,
        };
        await this.createAccount(accountEntity);
        await this.emailService.nuovoAccountMembro(
          accountEntity.email,
          membroEntity.nome,
          membroEntity.cognome,
          accountEntity.ruolo,
          membroEntity.associazione.alias,
          membroEntity.associazione.nome,
          password,
        );
      } else {
        // La creazione dell'account e' stata effettauta da una nuova iscrizione
        accountEntity = {
          ...accountEntity,
          oneTimeCode: makeNumericCode(10),
          password: await bcryptjs.hash(createMembroDto.account.password, 10),
          stato: StatoEnum.AttesaDiConferma,
        };
        let accountCreato = await this.createAccount(accountEntity);
        await this.emailService.nuovaRegistrazione(
          accountEntity.email,
          membroEntity.nome,
          membroEntity.cognome,
          membroEntity.associazione.nome,
          accountCreato.id,
          accountEntity.oneTimeCode,
          membroEntity.associazione.alias,
        );
      }
    }
    return this.findOne(membroEntity.id, membroEntity.associazioneId);
  }

  async find(
    idAssociazione: string,
    {
      type,
      genere,
      sezione,
      annoRiferimentoQuote,
      qualifica,
      ruolo,
      vedovanza,
      residente,
      deceduto,
      query,
    },
  ): Promise<MembroEntity[]> {
    let membriQueryBuilder = await this.membroModel
      .createQueryBuilder('membro')
      .leftJoinAndSelect('membro.domicilio', 'domicilio')
      .leftJoinAndSelect('membro.residenza', 'residenza')
      .leftJoinAndSelect('membro.quoteAssociative', 'quoteAssociative')
      .leftJoinAndSelect('membro.associazione', 'associazione')
      .leftJoinAndSelect('membro.account', 'account')
      .where('associazione.id = :idAssociazione', { idAssociazione });

    let conditions = {};
    switch (type) {
      case FilterTypeEnum.Membri: {
        break;
      }
      case FilterTypeEnum.Aspiranti: {
        membriQueryBuilder = membriQueryBuilder.andWhere(
          'membro.dataAccettazione is null',
        );
        break;
      }
      case FilterTypeEnum.Associati: {
        membriQueryBuilder = membriQueryBuilder.andWhere(
          'membro.dataAccettazione is not null',
        );
        break;
      }
      case FilterTypeEnum.Morosi: {
        membriQueryBuilder = membriQueryBuilder
          .andWhere('membro.dataAccettazione is not null')
          .andWhere(
            'NOT EXISTS (SELECT * FROM "quote_associative" where "quote_associative"."membroId" = membro.id AND "quote_associative"."dataPagamento" <= :data)',
            { data: new Date() },
          );
        break;
      }
    }

    console.log("annoRiferimentoQuote--------> " + annoRiferimentoQuote)

       if (annoRiferimentoQuote)
       membriQueryBuilder = membriQueryBuilder
          .andWhere(
            'EXISTS (SELECT * FROM "quote_associative" where "quote_associative"."membroId" = membro.id AND "quote_associative"."annoRiferimento" = :anno )',
            { anno: annoRiferimentoQuote },
      );


    if (genere)
      membriQueryBuilder = membriQueryBuilder.andWhere(
        'membro.genere = :genere',
        { genere },
      );
    if (sezione)
      membriQueryBuilder = membriQueryBuilder.andWhere(
        'membro.sezione = :sezione',
        { sezione },
      );

   
      
    if (qualifica)
      membriQueryBuilder = membriQueryBuilder.andWhere(
        'membro.qualifica = :qualifica',
        { qualifica },
      );
    if (ruolo)
      membriQueryBuilder = membriQueryBuilder.andWhere(
        'account.ruolo = :ruolo',
        { ruolo },
      );
    if (vedovanza)
      membriQueryBuilder = membriQueryBuilder.andWhere(
        'membro.vedovanza = :vedovanza',
        { vedovanza },
      );
    if (residente)
      membriQueryBuilder = membriQueryBuilder.andWhere(
        'membro.residente = :residente',
        { residente },
      );
    if (deceduto)
      membriQueryBuilder = membriQueryBuilder.andWhere(
        'membro.deceduto = :deceduto',
        { deceduto },
      );
    if (query) {
      membriQueryBuilder = membriQueryBuilder.andWhere(
        '(' +
          'membro.nome like :query or ' +
          'membro.cognome like :query or ' +
          'upper(membro.codiceFiscale) like upper(:query) or ' +
          "upper(concat(membro.nome, membro.cognome)) like upper(replace(:query, ' ', '')) or " +
          "upper(concat(membro.cognome, membro.nome)) like upper(replace(:query, ' ', ''))" +
          ')',
        { query: `%${query}%` },
      );
    }


    return membriQueryBuilder.getMany();
  }
  async findOne(id: string, idAssociazione: string): Promise<MembroEntity> {
    return await this.membroModel.findOne({
      relations: ['domicilio', 'residenza', 'account', 'associazione'],
      where: {
        id: id,
        associazioneId: idAssociazione,
      },
    });
  }
  async findOneById(id: string): Promise<MembroEntity> {
    return await this.membroModel.findOne({
      relations: ['domicilio', 'residenza', 'account', 'associazione'],
      where: {
        id: id,
      },
    });
  }
  async findOneByEmail(
    email: string,
    idAssociazione: string,
  ): Promise<MembroEntity> {
    let accountEntity = await this.accountEntityRepository.findOne({
      relations: ['membro', 'membro.domicilio', 'membro.residenza'],
      where: {
        email: email,
        membro: {
          associazioneId: idAssociazione,
        },
      },
    });
    if (accountEntity) {
      return accountEntity.membro;
    } else {
      return null;
    }
  }

  async update(
    id: string,
    idAssociazione: string,
    updateMembroDto: UpdateMembroDto,
    membroLoggato: MembroEntity,
  ): Promise<MembroEntity> {
    const membroEntity = await this.findOne(id, idAssociazione);

    // Modifico indirizzo di residenza
    let residenzaEntity: ResidenzaEntity = await this.residenzaEntityRepository.findOne(
      { membro: membroEntity },
    );
    residenzaEntity = {
      ...residenzaEntity,
      ...updateMembroDto.residenza,
    };
    await this.residenzaEntityRepository.save(residenzaEntity);

    // Modifico indirizzo di domicilio
    let domicilioEntity: DomicilioEntity = await this.domicilioEntityRepository.findOne(
      { membro: membroEntity },
    );
    domicilioEntity = {
      ...domicilioEntity,
      ...updateMembroDto.domicilio,
    };
    await this.domicilioEntityRepository.save(domicilioEntity);

    // Modifico account
    let accountEntity: AccountEntity = await this.accountEntityRepository.findOne(
      { membro: membroEntity },
    );
    if (accountEntity && updateMembroDto.account) {
      // Contorllo se il ruolo e' cambiato e se l'utente loggato e' amministratore
      if (
        accountEntity.ruolo !== updateMembroDto.account.ruolo &&
        membroLoggato.account.ruolo !== RuoloEnum.Amministratore
      ) {
        throw new HttpException(
          'Il ruolo può essere modificato solo da un utente Amministratore',
          HttpStatus.FORBIDDEN,
        );
      }
      // Controllo se il ruolo dell'utente sta cambiando da Amministratore
      if (
        accountEntity.ruolo === RuoloEnum.Amministratore &&
        accountEntity.ruolo !== updateMembroDto.account.ruolo &&
        this.isLastAdmin(idAssociazione)
      ) {
        throw new HttpException(
          "Non è possibile cambiare l'ultimo Amministratore rimasto",
          HttpStatus.BAD_REQUEST,
        );
      }
      accountEntity = {
        ...accountEntity,
        email: updateMembroDto.account.email,
        ruolo: updateMembroDto.account.ruolo,
      };
      await this.accountEntityRepository.save(accountEntity);
    }

    await this.membroModel.save({
      ...membroEntity,
      nome: updateMembroDto.nome,
      cognome: updateMembroDto.cognome,
      genere: updateMembroDto.genere,
      dataDiNascita: updateMembroDto.dataDiNascita,
      provinciaDiNascita: updateMembroDto.provinciaDiNascita,
      comuneDiNascita: updateMembroDto.comuneDiNascita,
      codiceFiscale: updateMembroDto.codiceFiscale,
      residente: updateMembroDto.residente,
      vedovanza: updateMembroDto.vedovanza,
      deceduto: updateMembroDto.deceduto,
      condizioniSelected: updateMembroDto.condizioniSelected,
      dataAccettazione: updateMembroDto.dataAccettazione,
      dataProfessione: updateMembroDto.dataProfessione,
      qualifica: updateMembroDto.qualifica,
      email: updateMembroDto.email,
      telefono: updateMembroDto.telefono,
      cellulare: updateMembroDto.cellulare,
      padreId: updateMembroDto.idMembroPadre,
      madreId: updateMembroDto.idMembroMadre,
      coniugeId: updateMembroDto.idMembroConiuge,
      sezione: updateMembroDto.sezione,
      paternitaEsterna: updateMembroDto.paternitaEsterna,
      maternitaEsterna: updateMembroDto.maternitaEsterna,
      coniugeEsterno: updateMembroDto.coniugeEsterno,
    });

    return this.findOne(membroEntity.id, membroEntity.associazioneId);
  }

  async remove(id: string, idAssociazione: string) {
    const membro = await this.findOne(id, idAssociazione);
    // Controllo se il ruolo dell'utente sta cambiando da Amministratore
    if (
      membro.account.ruolo === RuoloEnum.Amministratore &&
      this.isLastAdmin(idAssociazione)
    ) {
      throw new HttpException(
        "Non è possibile eliminare l'ultimo Amministratore rimasto",
        HttpStatus.BAD_REQUEST,
      );
    }
    return await this.membroModel.remove(membro);
  }

  async changePassword(
    idMembro: string,
    oldPassword: string,
    newPassword: string,
  ) {
    let accountEntity: AccountEntity = await this.accountEntityRepository.findOne(
      {
        relations: ['membro', 'membro.residenza', 'membro.domicilio'],
        where: {
          membro: {
            id: idMembro,
          },
        },
      },
    );
    if (!accountEntity)
      throw new HttpException('Account non trovato', HttpStatus.BAD_REQUEST);

    if (await bcryptjs.compare(oldPassword, accountEntity.password)) {
      accountEntity.password = await bcryptjs.hash(newPassword, 10);

      if (
        accountEntity.stato === StatoEnum.PrimoAccesso ||
        accountEntity.stato === StatoEnum.CambioPassword
      ) {
        accountEntity.stato = StatoEnum.Attivo;
      }

      await this.accountEntityRepository.save(accountEntity);
      await this.emailService.resetPasswordPasswordChanged(
        accountEntity.email,
        accountEntity.membro.nome,
        accountEntity.membro.cognome,
      );
      return accountEntity.membro;
    } else {
      throw new HttpException('Password errata', HttpStatus.BAD_REQUEST);
    }
  }

  async resetPasswordVerificationCode(
    aliasAssociazione: string,
    email: string,
  ): Promise<MembroEntity> {
    let accountEntity = await this.accountEntityRepository
      .createQueryBuilder('account')
      .leftJoinAndSelect('account.membro', 'membro')
      .leftJoinAndSelect('membro.associazione', 'associazione')
      .where('account.email = :email', { email })
      .andWhere('associazione.alias = :aliasAssociazione', {
        aliasAssociazione,
      })
      .getOne();
    if (!accountEntity)
      throw new HttpException('Account non trovato', HttpStatus.BAD_REQUEST);

    const code = makeNumericCode(5);
    accountEntity.oneTimeCode = code;
    await this.accountEntityRepository.save(accountEntity);
    await this.emailService.resetPasswordSendVerificationCode(
      accountEntity.email,
      accountEntity.membro.nome,
      accountEntity.membro.cognome,
      code,
    );
    return accountEntity.membro;
  }

  async resetPasswordChangePassword(
    aliasAssociazione: string,
    email: string,
    verificationCode: string,
    newPassword: string,
  ): Promise<MembroEntity> {
    let accountEntity = await this.accountEntityRepository
      .createQueryBuilder('account')
      .leftJoinAndSelect('account.membro', 'membro')
      .leftJoinAndSelect('membro.associazione', 'associazione')
      .where('account.email = :email', { email })
      .andWhere('associazione.alias = :aliasAssociazione', {
        aliasAssociazione,
      })
      .getOne();
    if (!accountEntity)
      throw new HttpException('Account non trovato', HttpStatus.BAD_REQUEST);

    if (accountEntity.oneTimeCode === null)
      throw new HttpException(null, HttpStatus.INTERNAL_SERVER_ERROR);

    if (accountEntity.oneTimeCode.toString() !== verificationCode.toString())
      throw new HttpException(
        'Codice di verifica errato',
        HttpStatus.BAD_REQUEST,
      );
    accountEntity.oneTimeCode = null;
    accountEntity.password = await bcryptjs.hash(newPassword, 10);

    await this.accountEntityRepository.save(accountEntity);
    await this.emailService.resetPasswordPasswordChanged(
      accountEntity.email,
      accountEntity.membro.nome,
      accountEntity.membro.cognome,
    );
    return accountEntity.membro;
  }

  async getNextNumero(idAssociazione: string): Promise<number> {
    const ultimoMembro: MembroEntity = await this.membroModel.findOne({
      where: {
        associazioneId: idAssociazione,
      },
      order: {
        numero: -1,
      },
    });
    if (!ultimoMembro) return 1;
    else return ultimoMembro.numero + 1;
  }

  async createResidenza(indirizzoEntity: ResidenzaEntity) {
    return this.residenzaEntityRepository.save(indirizzoEntity);
  }

  async createDomicilio(domicilioEntity: DomicilioEntity) {
    return this.domicilioEntityRepository.save(domicilioEntity);
  }

  async createAccount(account: AccountEntity): Promise<AccountEntity> {
    return await this.accountEntityRepository.save(account);
  }

  async activateAccount(idAccount: string, code: string): Promise<boolean> {
    let accountEntity = await this.accountEntityRepository
      .createQueryBuilder('account')
      .leftJoinAndSelect('account.membro', 'membro')
      .where('account.id = :idAccount', { idAccount })
      .andWhere('account.oneTimeCode = :code', { code })
      .andWhere('account.stato = :stato', { stato: StatoEnum.AttesaDiConferma })
      .getOne();
    if (!accountEntity)
      throw new HttpException('Richiesta non valida', HttpStatus.BAD_REQUEST);

    accountEntity.oneTimeCode = null;
    accountEntity.stato = StatoEnum.Attivo;
    await this.accountEntityRepository.save(accountEntity);
    await this.emailService.emailConfermata(
      accountEntity.email,
      accountEntity.membro.nome,
      accountEntity.membro.cognome,
    );
    return true;
  }

  async emailAccounts(emailAccounts: EmailAccountsDto): Promise<boolean> {
    let accountEntities = await this.accountEntityRepository
      .createQueryBuilder('account')
      .leftJoinAndSelect('account.membro', 'membro')
      .leftJoinAndSelect('membro.associazione', 'associazione')
      .where('account.email = :email', { email: emailAccounts.email })
      .getMany();

    Logger.log(accountEntities);

    if (!accountEntities || accountEntities.length === 0) return true;

    let _emailAccounts: EmailAccountInterface[] = [];
    accountEntities.map(account => {
      _emailAccounts.push({
        associazione: account.membro.associazione.nome,
        aliasAssociazione: account.membro.associazione.alias,
        ruolo: account.ruolo,
      });
    });
    Logger.log(_emailAccounts);
    await this.emailService.sendEmailAccounts(
      emailAccounts.email,
      _emailAccounts,
    );
    return true;
  }

  async isLastAdmin(idAssociazione: string) {
    let adminCount = await this.membroModel
      .createQueryBuilder('membro')
      .leftJoinAndSelect('membro.account', 'account')
      .where('membro.associazioneId = :idAssociazione', { idAssociazione })
      .andWhere('account.ruolo = :ruolo', { ruolo: RuoloEnum.Amministratore })
      .getCount();
    return adminCount < 2;
  }
}
