import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Logger,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { MembroService } from './membro.service';
import { CreateMembroDto } from './dto/create-membro.dto';
import { UpdateMembroDto } from './dto/update-membro.dto';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { MembriConverterDto, MembroConverterDto } from './membro.converter';
import { GetMembroDto } from './dto/get-membro.dto';
import {
  BlockForDemo,
  EntityEnum,
  IdAssociazione,
  IdMembro,
  Roles,
  RolesOrPrivate,
} from '../../auth/auth.decorator';
import { RuoloEnum } from './enums/ruolo.enum';
import { AuthGuard } from '../../auth/auth.guard';
import { QuotaAssociativaService } from '../quota-associativa/quota-associativa.service';
import { FilterTypeEnum } from './enums/filter-type.enum';
import { GenereEnum } from './enums/genere.enum';
import { MembroEntity } from './entities/membro.entity';
import { SezioneEnum } from './enums/sezione.enum';
import { QueryBuilder } from 'typeorm';
import { GetQuotaAssociativaDto } from '../quota-associativa/dto/get-quota-associativa.dto';

@Controller('membri')
@ApiTags('Membro')
export class MembroController {
  constructor(
    private membroService: MembroService,
    private quotaAssociativaService: QuotaAssociativaService,
  ) { }

  @Post()
  @ApiOkResponse({
    description: 'Creazione nuovo membro',
    type: GetMembroDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore, RuoloEnum.Segretario)
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async create(
    @Body() createMembroDto: CreateMembroDto,
    @IdAssociazione() idAssociazione: string,
  ): Promise<GetMembroDto> {
    const membro: MembroEntity = await this.membroService.create(
      createMembroDto,
      idAssociazione,
    );
    return await MembroConverterDto(membro);
  }

  @Get()
  @ApiOkResponse({
    description: 'Restituisce lista membri',
    type: [GetMembroDto],
  })
  @Roles(
    RuoloEnum.Amministratore,
    RuoloEnum.Operatore,
    RuoloEnum.Segretario,
    RuoloEnum.Tesoriere,
  )
  @UseGuards(AuthGuard)
  async find(
    @IdAssociazione() idAssociazione: string,
    @Query('type') type: FilterTypeEnum,
    @Query('genere') genere: GenereEnum,
    @Query('sezione') sezione: SezioneEnum,
    @Query('annoRiferimentoQuote') annoRiferimentoQuote: string,
    @Query('qualifica') qualifica: string,
    @Query('ruolo') ruolo: string,
    @Query('vedovanza') vedovanza: boolean,
    @Query('residente') residente: boolean,
    @Query('deceduto') deceduto: boolean,
    @Query('query') query: string,
  ) {
    const membri: MembroEntity[] = await this.membroService.find(
      idAssociazione,
      {
        type,
        genere,
        sezione,
        annoRiferimentoQuote,
        qualifica,
        ruolo,
        vedovanza,
        residente,
        deceduto,
        query,
      },
    );
    console.log("----------- GET /membri -------- ");
    console.log(membri);

    return MembriConverterDto(membri);
  }

  @Get(':id')
  @ApiOkResponse({
    description: 'Restituisce un membro',
    type: GetMembroDto,
  })
  @RolesOrPrivate({
    roles: [
      RuoloEnum.Amministratore,
      RuoloEnum.Operatore,
      RuoloEnum.Segretario,
      RuoloEnum.Tesoriere,
    ],
    entity: EntityEnum.Membro,
  })
  @UseGuards(AuthGuard)
  async findOne(
    @Param('id') id: string,
    @IdAssociazione() idAssociazione: string,
  ) {
    let membro = await this.membroService.findOne(id, idAssociazione);
    if (!membro)
      throw new HttpException('Membro non trovato', HttpStatus.NOT_FOUND);
    return MembroConverterDto(membro);
  }

  @Put(':id')
  @ApiOkResponse({
    description: 'Modifica i dati del membro',
    type: GetMembroDto,
  })
  @RolesOrPrivate({
    roles: [
      RuoloEnum.Amministratore,
      RuoloEnum.Operatore,
      RuoloEnum.Segretario,
    ],
    entity: EntityEnum.Membro,
  })
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async update(
    @Param('id') id: string,
    @IdAssociazione() idAssociazione: string,
    @Body() updateMembroDto: UpdateMembroDto,
    @IdMembro() idMembroLoggato: string,
  ) {
    let membroLoggato = await this.membroService.findOne(
      idMembroLoggato,
      idAssociazione,
    );

    let membro = await this.membroService.update(
      id,
      idAssociazione,
      updateMembroDto,
      membroLoggato,
    );
    if (!membro)
      throw new HttpException('Membro non trovato', HttpStatus.NOT_FOUND);
    return MembroConverterDto(membro);
  }

  @Delete(':id')
  @ApiOkResponse({
    description: 'Elimina membro',
    type: GetMembroDto,
  })
  @Roles(RuoloEnum.Amministratore, RuoloEnum.Operatore, RuoloEnum.Segretario)
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async remove(
    @Param('id') id: string,
    @IdMembro() idMembroLoggato: string,
    @IdAssociazione() idAssociazione: string,
  ) {
    if (idMembroLoggato === id) {
      throw new HttpException(
        'Impossibile eliminare il membro loggato',
        HttpStatus.FORBIDDEN,
      );
    }
    let membro = await this.membroService.remove(id, idAssociazione);
    if (!membro)
      throw new HttpException('Membro non trovato', HttpStatus.NOT_FOUND);
    return true;
  }
}
