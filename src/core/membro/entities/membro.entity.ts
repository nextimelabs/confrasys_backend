<<<<<<< HEAD
import { Qualifica, QualificaEnum } from "../enums/qualifica.enum";
import { Ruolo, RuoloEnum } from "../enums/ruolo.enum";
import { Genere, GenereEnum } from "../enums/genere.enum";
import {
  QuotaAssociativaEntity,
} from "../../quota-associativa/entities/quota-associativa.entity";
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { AccountEntity } from "./account.entity";
import { AssociazioneEntity } from "../../associazione/entities/associazione.entity";
import { DomicilioEntity } from "./domicilio.entity";
import { ResidenzaEntity } from "./residenza.entity";
import { SezioneEnum } from '../enums/sezione.enum';

=======
import { Qualifica, QualificaEnum } from '../enums/qualifica.enum';
import { Ruolo, RuoloEnum } from '../enums/ruolo.enum';
import { Genere, GenereEnum } from '../enums/genere.enum';
import { QuotaAssociativaEntity } from '../../quota-associativa/entities/quota-associativa.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { AccountEntity } from './account.entity';
import { AssociazioneEntity } from '../../associazione/entities/associazione.entity';
import { DomicilioEntity } from './domicilio.entity';
import { ResidenzaEntity } from './residenza.entity';
import { SezioneEnum } from '../enums/sezione.enum';
>>>>>>> master

@Entity('membri')
export class MembroEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
<<<<<<< HEAD
  @Column("integer", { nullable: false })
  numero: number;
  @Column("varchar", { length: 50, nullable: false })
  nome: string;
  @Column("varchar", { length: 50, nullable: false })
  cognome: string;
  @Column("varchar", { length: 1, nullable: false })
  genere: GenereEnum;
  @Column("timestamp without time zone", { nullable: false })
  dataDiNascita: string;
  @Column("varchar", { length: 30, nullable: false })
  comuneDiNascita: string;
  @Column("varchar", { length: 30, nullable: false })
  provinciaDiNascita: string;
  @Column("varchar", { length: 20, nullable: false })
  codiceFiscale: string;
  @Column("boolean", { default: false })
  residente: boolean;
  @Column("boolean", { default: false })
  vedovanza: boolean;
  @Column("boolean", { default: false })
=======
  @Column('integer', { nullable: false })
  numero: number;
  @Column('varchar', { length: 50, nullable: false })
  nome: string;
  @Column('varchar', { length: 50, nullable: false })
  cognome: string;
  @Column('varchar', { length: 1, nullable: false })
  genere: GenereEnum;
  @Column('timestamp without time zone', { nullable: false })
  dataDiNascita: string;
  @Column('varchar', { length: 30, nullable: false })
  comuneDiNascita: string;
  @Column('varchar', { length: 30, nullable: false })
  provinciaDiNascita: string;
  @Column('varchar', { length: 20, nullable: false })
  codiceFiscale: string;
  @Column('boolean', { default: false })
  residente: boolean;
  @Column('boolean', { default: false })
  vedovanza: boolean;
  @Column('boolean', { default: false })
>>>>>>> master
  deceduto: boolean;
  @Column('varchar', { array: true })
  condizioniSelected: string[];
  @OneToOne(
    type => ResidenzaEntity,
    residenza => residenza.membro,
  )
  residenza?: ResidenzaEntity;
  @OneToOne(
    type => DomicilioEntity,
    domicilio => domicilio.membro,
  )
  domicilio?: DomicilioEntity;
<<<<<<< HEAD
  @Column("timestamp with time zone", { nullable: true })
  dataAccettazione?: string;
  @Column("varchar", { length: 30, nullable: false })
  qualifica: QualificaEnum;
  @Column("varchar", { length: 50, nullable: true })
  email: string;
  @Column("varchar", { length: 50, nullable: true })
  telefono: string;
  @Column("varchar", { length: 50, nullable: true })
=======
  @Column('timestamp without time zone', { nullable: true })
  dataAccettazione: string;
  @Column('timestamp without time zone', { nullable: true })
  dataProfessione: string;
  @Column('varchar', { length: 30, nullable: false })
  qualifica: QualificaEnum;
  @Column('varchar', { length: 50, nullable: true })
  sezione: SezioneEnum;
  @Column('varchar', { length: 50, nullable: true })
  email: string;
  @Column('varchar', { length: 50, nullable: true })
  telefono: string;
  @Column('varchar', { length: 50, nullable: true })
>>>>>>> master
  cellulare: string;
  @ManyToOne(
    type => MembroEntity,
    membro => membro.id,
    { onDelete: 'SET NULL' },
  )
  @JoinColumn()
  padre?: MembroEntity;
  @Column({ nullable: true })
  padreId: string;

  @ManyToOne(
    type => MembroEntity,
    membro => membro.id,
    { onDelete: 'SET NULL' },
  )
  @JoinColumn()
  madre?: MembroEntity;
  @Column({ nullable: true })
  madreId: string;
  @ManyToOne(
    type => MembroEntity,
    membro => membro.id,
    { onDelete: 'SET NULL' },
  )
  @JoinColumn()
  coniuge?: MembroEntity;
  @Column({ nullable: true })
  coniugeId: string;
  @Column({ nullable: true })
  paternitaEsterna: string;
  @Column({ nullable: true })
  maternitaEsterna: string;
  @Column({ nullable: true })
  coniugeEsterno: string;
  @OneToMany(
    type => QuotaAssociativaEntity,
    quota => quota.membro,
  )
  quoteAssociative?: QuotaAssociativaEntity[];

  @OneToOne(
    type => AccountEntity,
    account => account.membro,
  )
  account?: AccountEntity;
  @ManyToOne(
    type => AssociazioneEntity,
    associazione => associazione.id,
    { onDelete: 'CASCADE' },
  )
  @JoinColumn()
  associazione?: AssociazioneEntity;
  @Column({ nullable: false })
  associazioneId: string;

  // Nuovi Campi
  @Column('varchar', { array: true })
  condizioniSelected: string[];

  @Column('timestamp without time zone', { nullable: true })
  dataProfessione: string;

  @Column('varchar', { length: 50, nullable: true })
  sezione: SezioneEnum;

  @Column({ nullable: true })
  paternitaEsterna: string;

  @Column({ nullable: true })
  maternitaEsterna: string;

  @Column({ nullable: true })
  coniugeEsterno: string;
}
