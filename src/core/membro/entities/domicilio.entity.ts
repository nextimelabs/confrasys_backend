import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { MembroEntity } from './membro.entity';
import { IndirizzoEntity } from './indirizzo.entity';

@Entity('domicili')
export class DomicilioEntity extends IndirizzoEntity {}
