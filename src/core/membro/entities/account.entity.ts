import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { MembroEntity } from './membro.entity';
import { RuoloEnum } from '../enums/ruolo.enum';
import { StatoEnum } from '../enums/stato.enum';

@Entity('account')
export class AccountEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column('varchar', { length: 50, nullable: false })
  email: string;
  @Column('varchar', { length: 255, nullable: false })
  password: string;
  @Column('varchar', { length: 10, nullable: true })
  oneTimeCode: string;
  @Column('varchar', { length: 30, nullable: false })
  ruolo: RuoloEnum;
  @Column('varchar', { length: 30, nullable: false })
  stato: StatoEnum;
  @ManyToOne(
    type => MembroEntity,
    membro => membro.id,
    { onDelete: 'CASCADE' },
  )
  @JoinColumn()
  membro: MembroEntity;
}
