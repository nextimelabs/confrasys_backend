import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { MembroEntity } from './membro.entity';

export class IndirizzoEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column('varchar', { length: 50, nullable: false })
  indirizzo: string;
  @Column('varchar', { length: 10, nullable: false })
  civico: string;
  @Column('varchar', { length: 50, nullable: false })
  comune: string;
  @Column('varchar', { length: 2, nullable: false })
  provincia: string;
  @Column('varchar', { length: 10, nullable: false })
  CAP: string;
  @ManyToOne(
    type => MembroEntity,
    membro => membro.id,
    { onDelete: 'CASCADE' },
  )
  @JoinColumn()
  membro: MembroEntity;
}
