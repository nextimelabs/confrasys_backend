import { Module } from '@nestjs/common';
import { MembroService } from './membro.service';
import { MembroController } from './membro.controller';
import { MembroEntity } from './entities/membro.entity';
import { QuotaAssociativaEntity } from '../quota-associativa/entities/quota-associativa.entity';
import { QuotaAssociativaService } from '../quota-associativa/quota-associativa.service';
import { AssociazioneEntity } from '../associazione/entities/associazione.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AccountEntity } from './entities/account.entity';
import { ResidenzaEntity } from './entities/residenza.entity';
import { DomicilioEntity } from './entities/domicilio.entity';
import { MovimentoEntity } from '../libro-giornale/entities/movimento.entity';
import { LibroGiornaleService } from '../libro-giornale/libro-giornale.service';
import { EmailService } from '../../shared/email.service';
import { SharedModule } from '../../shared/shared.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      AssociazioneEntity,
      MembroEntity,
      QuotaAssociativaEntity,
      ResidenzaEntity,
      DomicilioEntity,
      AccountEntity,
      MovimentoEntity,
    ]),
    SharedModule,
  ],
  controllers: [MembroController],
  providers: [
    MembroService,
    QuotaAssociativaService,
    LibroGiornaleService,
    EmailService,
  ],
  exports: [SharedModule, MembroService],
})
export class MembroModule {}
