import { GetMembroDto } from './dto/get-membro.dto';
import { QuotaAssociativaService } from '../quota-associativa/quota-associativa.service';
import { QuoteAssociativeConverterDto } from '../quota-associativa/quota-associativa.converter';
import { Logger } from '@nestjs/common';
import { MembroEntity } from './entities/membro.entity';
import * as moment from 'moment-timezone';
import { IndirizzoDto } from '../../shared/dto/indirizzo.dto';
import { ResidenzaEntity } from './entities/residenza.entity';
import { IndirizzoEntity } from './entities/indirizzo.entity';
import { AccountEntity } from './entities/account.entity';
import { GetAccountDto } from './dto/get-account.dto';

export function IndirizzoConverterDto(
  indirizzoEntity: IndirizzoEntity,
): IndirizzoDto {
  return {
    indirizzo: indirizzoEntity.indirizzo,
    civico: indirizzoEntity.civico,
    provincia: indirizzoEntity.provincia,
    comune: indirizzoEntity.comune,
    CAP: indirizzoEntity.CAP,
  };
}
export async function MembroConverterDto(
  membroDocument: MembroEntity,
): Promise<GetMembroDto> {
  return {
    id: membroDocument.id,
    numero: membroDocument.numero,
    nome: membroDocument.nome,
    cognome: membroDocument.cognome,
    genere: membroDocument.genere,
    dataDiNascita: moment(membroDocument.dataDiNascita).format('YYYY-MM-DD'),
    comuneDiNascita: membroDocument.comuneDiNascita,
    provinciaDiNascita: membroDocument.provinciaDiNascita,
    codiceFiscale: membroDocument.codiceFiscale,
    residente: membroDocument.residente,
    vedovanza: membroDocument.vedovanza,
    condizioniSelected: membroDocument.condizioniSelected,
    deceduto: membroDocument.deceduto,
    residenza: membroDocument.residenza
      ? IndirizzoConverterDto(membroDocument.residenza)
      : null,
    domicilio: membroDocument.domicilio
      ? IndirizzoConverterDto(membroDocument.domicilio)
      : null,
    dataAccettazione: moment(membroDocument.dataAccettazione).format(
      'YYYY-MM-DD',
    ),
    dataProfessione: moment(membroDocument.dataProfessione).format(
      'YYYY-MM-DD',
    ),
    qualifica: membroDocument.qualifica,
    account:
      membroDocument.account && AccountConverterDto(membroDocument.account),
    email: membroDocument.email,
    telefono: membroDocument.telefono,
    cellulare: membroDocument.cellulare,
    idMembroPadre: membroDocument.padreId,
    idMembroMadre: membroDocument.madreId,
    idMembroConiuge: membroDocument.coniugeId,
    sezione: membroDocument.sezione,
    paternitaEsterna: membroDocument.paternitaEsterna,
    maternitaEsterna: membroDocument.maternitaEsterna,
    coniugeEsterno: membroDocument.coniugeEsterno,
    idAssociazione: membroDocument.associazioneId,
    quoteAssociative:
      membroDocument.quoteAssociative !== undefined &&
      QuoteAssociativeConverterDto(membroDocument.quoteAssociative),
  };
}
export async function MembriConverterDto(
  membriDocument: MembroEntity[],
): Promise<GetMembroDto[]> {
  let response: GetMembroDto[] = [];
  for (let i = 0; i < membriDocument.length; i++) {
    response.push(await MembroConverterDto(membriDocument[i]));
  }
  return response;
}
export function AccountConverterDto(
  accountEntity: AccountEntity,
): GetAccountDto {
  return {
    email: accountEntity.email,
    ruolo: accountEntity.ruolo,
    stato: accountEntity.stato,
  };
}
