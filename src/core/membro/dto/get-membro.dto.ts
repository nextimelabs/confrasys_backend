import { RuoloEnum } from '../enums/ruolo.enum';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDate,
  IsDefined,
  IsEnum,
  IsISO8601,
  IsMongoId,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
  MaxLength,
  ValidateNested,
} from 'class-validator';
<<<<<<< HEAD
import {ContattoDto} from "../../../shared/dto/contatto.dto";
import {IndirizzoDto} from "../../../shared/dto/indirizzo.dto";
import {GenereEnum} from "../enums/genere.enum";
import {GetQuotaAssociativaDto} from "../../quota-associativa/dto/get-quota-associativa.dto";
import {QualificaEnum} from "../enums/qualifica.enum";
import {Type} from "class-transformer";
import {StatoEnum} from "../enums/stato.enum";
import {AccountDto} from "./account.dto";
import {GetAccountDto} from "./get-account.dto";
import {SezioneEnum } from '../enums/sezione.enum';

=======
import { ContattoDto } from '../../../shared/dto/contatto.dto';
import { IndirizzoDto } from '../../../shared/dto/indirizzo.dto';
import { GenereEnum } from '../enums/genere.enum';
import { GetQuotaAssociativaDto } from '../../quota-associativa/dto/get-quota-associativa.dto';
import { QualificaEnum } from '../enums/qualifica.enum';
import { Type } from 'class-transformer';
import { StatoEnum } from '../enums/stato.enum';
import { AccountDto } from './account.dto';
import { GetAccountDto } from './get-account.dto';
import { Sezione, SezioneEnum } from '../enums/sezione.enum';
>>>>>>> master

export class GetMembroDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsUUID()
  id: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsNumber()
  numero: number;

  @ApiProperty({ required: true, maxLength: 50 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  nome: string;

  @ApiProperty({ required: true, maxLength: 50 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  cognome: string;

  @ApiProperty({ required: true, enum: GenereEnum })
  @IsNotEmpty()
  @IsString()
  @IsEnum(GenereEnum)
  genere: GenereEnum;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsISO8601()
  dataDiNascita: string;

  @ApiProperty({ required: true, maxLength: 30 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(30)
  comuneDiNascita: string;

  @ApiProperty({ required: true, maxLength: 30 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(30)
  provinciaDiNascita: string;

  @ApiProperty({ required: true, maxLength: 20 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(20)
  codiceFiscale: string;

  @ApiPropertyOptional({ default: false })
  @IsOptional()
  @IsBoolean()
  residente: boolean;

  @ApiPropertyOptional({ default: false })
  @IsOptional()
  @IsBoolean()
  vedovanza: boolean;

  @ApiPropertyOptional({ default: false })
  @IsOptional()
  @IsBoolean()
  deceduto: boolean;

  @ApiProperty({ required: true, maxLength: 30 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(30)
  condizioniSelected: string[];

  @ApiProperty()
  @IsDefined()
  @ValidateNested()
  @Type(() => IndirizzoDto)
  residenza: IndirizzoDto;

  @ApiProperty()
  @IsDefined()
  @ValidateNested()
  @Type(() => IndirizzoDto)
  domicilio: IndirizzoDto;

  @ApiPropertyOptional()
  @IsOptional()
  @IsISO8601()
  dataAccettazione: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsISO8601()
  dataProfessione: string;

  @ApiProperty({ required: true, enum: QualificaEnum })
  @IsNotEmpty()
  @IsString()
  @IsEnum(QualificaEnum)
  qualifica: QualificaEnum;

  @ApiProperty({ required: true, enum: SezioneEnum })
  @IsNotEmpty()
  @IsString()
  @IsEnum(SezioneEnum)
  sezione: SezioneEnum;

  @ApiProperty({ required: false, maxLength: 50 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  email: string;

  @ApiProperty({ required: false, maxLength: 50 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  telefono: string;

  @ApiProperty({ required: false, maxLength: 50 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  cellulare: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsUUID()
  idMembroPadre: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsUUID()
  idMembroMadre: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsUUID()
  idMembroConiuge: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsUUID()
  paternitaEsterna: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsUUID()
  maternitaEsterna: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsUUID()
  coniugeEsterno: string;

  @ApiProperty()
  @ValidateNested()
  quoteAssociative: GetQuotaAssociativaDto[];

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsMongoId()
  idAssociazione: string;

  @ApiPropertyOptional()
  @IsOptional()
  @ValidateNested()
  @Type(() => GetAccountDto)
  account?: GetAccountDto;

  // Nuovi campi
   @ApiProperty({ required: true, maxLength: 30 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(30)
  condizioniSelected: string[];

    @ApiPropertyOptional()
  @IsOptional()
  @IsISO8601()
  dataProfessione: string;

   @IsNotEmpty()
  @IsString()
  @IsEnum(SezioneEnum)
  sezione: SezioneEnum;

    @ApiPropertyOptional()
  @IsOptional()
  paternitaEsterna: string;

  @ApiPropertyOptional()
  @IsOptional()
  maternitaEsterna: string;

  @ApiPropertyOptional()
  @IsOptional()
  coniugeEsterno: string;
}
