<<<<<<< HEAD
import { Ruolo, RuoloEnum } from "../enums/ruolo.enum";
=======
import { Ruolo, RuoloEnum } from '../enums/ruolo.enum';
>>>>>>> master
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDefined,
  IsEnum,
  IsArray,
  IsDate,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
<<<<<<< HEAD
  IsString, IsUUID, MaxLength,
  ValidateNested, IsISO8601, IsArray
} from 'class-validator';
import { ContattoDto } from "../../../shared/dto/contatto.dto";
import { IndirizzoDto } from "../../../shared/dto/indirizzo.dto";
import { AccountDto } from "./account.dto";
import { Type } from "class-transformer";
import { GenereEnum } from "../enums/genere.enum";
import { QualificaEnum } from "../enums/qualifica.enum";
import { SezioneEnum } from '../enums/sezione.enum';
=======
  IsString,
  IsUUID,
  MaxLength,
  ValidateNested,
  IsISO8601,
  IsDateString,
} from 'class-validator';
import { ContattoDto } from '../../../shared/dto/contatto.dto';
import { IndirizzoDto } from '../../../shared/dto/indirizzo.dto';
import { AccountDto } from './account.dto';
import { Type } from 'class-transformer';
import { GenereEnum } from '../enums/genere.enum';
import { QualificaEnum } from '../enums/qualifica.enum';
import { Sezione, SezioneEnum } from '../enums/sezione.enum';
>>>>>>> master

export class CreateMembroDto {
  @ApiProperty({ required: true, maxLength: 50 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  nome: string;

  @ApiProperty({ required: true, maxLength: 50 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  cognome: string;

  @ApiProperty({ required: true, enum: GenereEnum })
  @IsNotEmpty()
  @IsString()
  @IsEnum(GenereEnum)
  genere: GenereEnum;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  @IsISO8601()
  dataDiNascita: string;

  @ApiProperty({ required: true, maxLength: 30 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(30)
  comuneDiNascita: string;

  @ApiProperty({ required: true, maxLength: 30 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(30)
  provinciaDiNascita: string;

  @ApiProperty({ required: true, maxLength: 20 })
  @IsNotEmpty()
  @IsString()
  @MaxLength(20)
  codiceFiscale: string;

  @ApiPropertyOptional({ default: false })
  @IsOptional()
  @IsBoolean()
  residente: boolean;

  @ApiPropertyOptional({ default: false })
  @IsOptional()
  @IsBoolean()
  vedovanza: boolean;

  @ApiPropertyOptional({ default: false })
  @IsOptional()
  @IsBoolean()
  deceduto: boolean;

  @ApiProperty({ required: false })
  @IsArray()
  @IsString({ each: true })
  condizioniSelected: string[];

  @ApiProperty()
  @IsDefined()
  @ValidateNested()
  @Type(() => IndirizzoDto)
  residenza: IndirizzoDto;

  @ApiProperty()
  @IsDefined()
  @ValidateNested()
  @Type(() => IndirizzoDto)
  domicilio: IndirizzoDto;

  @ApiPropertyOptional()
  @IsOptional()
  @IsISO8601()
  dataAccettazione: string;

<<<<<<< HEAD
=======
  @ApiPropertyOptional()
  @IsOptional()
  @IsISO8601()
  dataProfessione: string;

>>>>>>> master
  @ApiProperty({ required: true, enum: QualificaEnum })
  @IsNotEmpty()
  @IsString()
  @IsEnum(QualificaEnum)
  qualifica: QualificaEnum;

<<<<<<< HEAD
=======
  @ApiProperty({ required: true, enum: SezioneEnum })
  @IsNotEmpty()
  @IsString()
  @IsEnum(SezioneEnum)
  sezione: SezioneEnum;

>>>>>>> master
  @ApiProperty({ required: false, maxLength: 50 })
  @IsOptional()
  @IsString()
  @MaxLength(50)
  email: string;

  @ApiProperty({ required: false, maxLength: 50 })
  @IsOptional()
  @IsString()
  @MaxLength(50)
  telefono: string;

  @ApiProperty({ required: false, maxLength: 50 })
  @IsOptional()
  @IsString()
  @MaxLength(50)
  cellulare: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsUUID()
  idMembroPadre: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsUUID()
  idMembroMadre: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsUUID()
  idMembroConiuge: string;

  @ApiProperty()
  @IsString()
  paternitaEsterna: string;

  @ApiProperty()
  @IsString()
  maternitaEsterna: string;

  @ApiProperty()
  @IsString()
  coniugeEsterno: string;

  @ApiPropertyOptional()
  @IsOptional()
  @ValidateNested()
  @Type(() => AccountDto)
  account?: AccountDto;


  // Nuovi campi
  @ApiProperty({ required: false })
  @IsArray()
  @IsString({ each: true })
  condizioniSelected: string[];

  @ApiPropertyOptional()
  @IsOptional()
  @IsISO8601()
  dataProfessione: string;

  @IsNotEmpty()
  @IsString()
  @IsEnum(SezioneEnum)
  sezione: SezioneEnum;


  @ApiPropertyOptional()
  @IsOptional()
  paternitaEsterna: string;

  @ApiPropertyOptional()
  @IsOptional()
  maternitaEsterna: string;

  @ApiPropertyOptional()
  @IsOptional()
  coniugeEsterno: string;
}
