import { OmitType, IntersectionType, ApiProperty } from '@nestjs/swagger';
import { AccountDto } from './account.dto';
import { RuoloEnum } from '../enums/ruolo.enum';
import { IsEnum, IsNotEmpty } from 'class-validator';
import { StatoEnum } from '../enums/stato.enum';

class AdditionalAccountInfo {
  @ApiProperty({ enum: StatoEnum })
  @IsEnum(StatoEnum)
  @IsNotEmpty()
  stato: StatoEnum;
}

export class GetAccountDto extends IntersectionType(
  OmitType(AccountDto, ['password'] as const),
  AdditionalAccountInfo,
) {}
