import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsEnum,
  IsHash,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  Length,
  MaxLength,
  MinLength,
} from 'class-validator';
import { RuoloEnum } from '../enums/ruolo.enum';

export class AccountDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  @MaxLength(50)
  email: string;

  @ApiProperty()
  @MinLength(7)
  @IsOptional()
  password: string;

  @ApiProperty({ enum: RuoloEnum })
  @IsEnum(RuoloEnum)
  @IsNotEmpty()
  ruolo: RuoloEnum;
}
