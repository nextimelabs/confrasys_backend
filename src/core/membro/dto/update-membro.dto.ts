import { PartialType, OmitType } from '@nestjs/swagger';
import { CreateMembroDto } from './create-membro.dto';

export class UpdateMembroDto extends PartialType(CreateMembroDto) {}
