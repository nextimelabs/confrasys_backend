export enum SezioneEnum {
  ISezione = 'I Sezione',
  IISezione = 'II Sezione',
  IIISezione = 'III Sezione',
}
export const Sezione = ['I Sezione', 'II Sezione', 'III Sezione'];
