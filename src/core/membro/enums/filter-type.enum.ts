export enum FilterTypeEnum {
  Membri = 'Membri',
  Associati = 'Associati',
  Aspiranti = 'Aspiranti',
  Morosi = 'Morosi',
}
