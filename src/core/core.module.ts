import { Module } from '@nestjs/common';
import { AssociazioneModule } from './associazione/associazione.module';
import { MembroModule } from './membro/membro.module';
import { QuotaAssociativaModule } from './quota-associativa/quota-associativa.module';
import { CappellaModule } from './cappella/cappella.module';
import { ContrattoModule } from './contratto/contratto.module';
import { LibroGiornaleModule } from './libro-giornale/libro-giornale.module';
import { DashboardModule } from './dashboard/dashboard.module';

@Module({
  imports: [
    DashboardModule,
    AssociazioneModule,
    MembroModule,
    QuotaAssociativaModule,
    CappellaModule,
    ContrattoModule,
    LibroGiornaleModule,
  ],
})
export class CoreModule {}
