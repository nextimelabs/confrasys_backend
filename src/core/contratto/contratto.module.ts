import { Module } from '@nestjs/common';
import { ContrattoService } from './contratto.service';
import { ContrattoController } from './contratto.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ContrattoEntity } from './entities/contratto.entity';
import { MembroEntity } from '../membro/entities/membro.entity';
import { MovimentoEntity } from '../libro-giornale/entities/movimento.entity';
import { LibroGiornaleService } from '../libro-giornale/libro-giornale.service';
import { LibroGiornaleModule } from '../libro-giornale/libro-giornale.module';
import { MembroModule } from '../membro/membro.module';
import { MembroService } from '../membro/membro.service';
import { ResidenzaEntity } from '../membro/entities/residenza.entity';
import { DomicilioEntity } from '../membro/entities/domicilio.entity';
import { AccountEntity } from '../membro/entities/account.entity';
import { CappellaEntity } from '../cappella/entities/cappella.entity';
import { CappellaModule } from '../cappella/cappella.module';
import { CappellaService } from '../cappella/cappella.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ContrattoEntity,
      MembroEntity,
      MovimentoEntity,
      ResidenzaEntity,
      AccountEntity,
      DomicilioEntity,
      CappellaEntity,
    ]),
    LibroGiornaleModule,
    MembroModule,
    CappellaModule,
  ],
  controllers: [ContrattoController],
  providers: [
    ContrattoService,
    LibroGiornaleService,
    MembroService,
    CappellaService,
  ],
})
export class ContrattoModule {}
