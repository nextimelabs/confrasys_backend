export enum TipoContrattoEnum {
  Loculo = 'Loculo',
  Ossario = 'Ossario',
}
export const TipoContratto = ['Loculo', 'Ossario'];
