import { PartialType } from '@nestjs/mapped-types';
import { CreateContrattoDto } from './create-contratto.dto';

export class UpdateContrattoDto extends PartialType(CreateContrattoDto) {}
