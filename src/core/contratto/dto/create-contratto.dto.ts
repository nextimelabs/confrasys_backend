import { TipoContrattoEnum } from '../enums/tipo-contratto.enum';
import { tipologiaCappellaEnum } from '../../cappella/enum/tipologia-cappella.enum';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsEnum,
  IsISO8601,
  IsNotEmpty,
  IsNumber,
  IsNumberString,
  IsUUID,
} from 'class-validator';

import { JoinColumn } from 'typeorm';

export class CreateContrattoDto {
  @ApiProperty()
  @IsISO8601()
  dataContratto: string;

  @ApiProperty()
  @IsISO8601()
  dataScadenza: string;

  @ApiProperty()
  @IsNumberString()
  @IsNotEmpty()
  importo: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  anniContratto: number;

  @ApiProperty()
  @IsEnum(TipoContrattoEnum)
  @IsNotEmpty()
  tipoContratto: TipoContrattoEnum;

  @ApiProperty()
  @IsEnum(tipologiaCappellaEnum)
  @IsNotEmpty()
  tipologiaCappella: tipologiaCappellaEnum;

  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  idCappella: string;

  @ApiProperty()
  @IsNotEmpty()
  nomeCappella: string;

  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  idMembroTitolare: string;

  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  idMembroDeceduto: string;

}
