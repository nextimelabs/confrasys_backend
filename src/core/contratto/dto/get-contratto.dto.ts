import { TipoContrattoEnum } from '../enums/tipo-contratto.enum';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsEnum,
  IsISO8601,
  IsNotEmpty,
  IsNumber,
  IsString,
  IsUUID,
} from 'class-validator';
import { tipologiaCappellaEnum } from '../../cappella/enum/tipologia-cappella.enum';

export class GetContrattoDto {
  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  id: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  numeroContratto: number;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  anniContratto: number;

  @ApiProperty()
  @IsISO8601()
  dataContratto: string;

  @ApiProperty()
  @IsISO8601()
  dataScadenza: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  importo: number;

  @ApiProperty()
  @IsEnum(TipoContrattoEnum)
  @IsNotEmpty()
  tipoContratto: TipoContrattoEnum;

  @ApiProperty()
  @IsEnum(tipologiaCappellaEnum)
  @IsNotEmpty()
  tipologiaCappella: tipologiaCappellaEnum;

  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  idCappella: string;

  @ApiProperty()
  @IsString()
  nomeCappella: string;

  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  idMembroTitolare: string;

  @ApiProperty()
  @IsUUID()
  @IsNotEmpty()
  idMembroDeceduto: string;
}
