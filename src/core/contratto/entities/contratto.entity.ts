import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CappellaEntity } from '../../cappella/entities/cappella.entity';
import { MembroEntity } from '../../membro/entities/membro.entity';
import { AssociazioneEntity } from '../../associazione/entities/associazione.entity';
import { TipoContrattoEnum } from '../enums/tipo-contratto.enum';
import { MovimentoEntity } from '../../libro-giornale/entities/movimento.entity';
import { tipologiaCappellaEnum } from '../../cappella/enum/tipologia-cappella.enum';

@Entity('contratti')
export class ContrattoEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column('integer', { nullable: false })
  numeroContratto: number;

  @Column('integer', { nullable: false })
  anniContratto: number;

  @Column('timestamp without time zone', { nullable: false })
  dataContratto: string;
  @Column('timestamp without time zone', { nullable: false })
  dataScadenza: string;
  @Column('double precision', { nullable: false })
  importo: number;
  @Column('varchar', { length: 30, nullable: false })
  tipoContratto: TipoContrattoEnum;
  @Column('varchar', { length: 30, nullable: false })
  tipologiaCappella: tipologiaCappellaEnum;

  @ManyToOne(
    () => CappellaEntity,
    cappella => cappella.id,
    { onDelete: 'CASCADE' },
  )
  @JoinColumn()
  cappella?: CappellaEntity;

  @Column({ nullable: false })
  cappellaId: string;

  @Column({ nullable: false })
  nomeCappella: string;
  // @ManyToOne(
  //   () => CappellaEntity,
  //   cappella => cappella.nome,
  //   { onDelete: 'CASCADE' },
  // )
  // @JoinColumn()
  // nome?: CappellaEntity;
  // @Column({ nullable: false })
  // nome: string;

  @ManyToOne(
    () => MembroEntity,
    membro => membro.id,
    { onDelete: 'SET NULL' },
  )
  @JoinColumn()
  deceduto?: MembroEntity;
  @Column({ nullable: false })
  decedutoId: string;

  @ManyToOne(
    () => MembroEntity,
    membro => membro.id,
    { onDelete: 'SET NULL' },
  )
  @JoinColumn()
  titolare?: MembroEntity;
  @Column({ nullable: false })
  titolareId: string;
  @OneToOne(
    () => MovimentoEntity,
    movimento => movimento.id,
    { onDelete: 'SET NULL' },
  )
  @JoinColumn()
  movimento?: MovimentoEntity;
  @Column({ nullable: true })
  movimentoId: string;

  @ManyToOne(
    type => AssociazioneEntity,
    associazione => associazione.id,
    { onDelete: 'CASCADE' },
  )
  @JoinColumn()
  associazione?: AssociazioneEntity;
  @Column({ nullable: false })
  associazioneId: string;
}
