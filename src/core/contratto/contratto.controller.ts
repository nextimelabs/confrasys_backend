import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseGuards,
  Query,
} from '@nestjs/common';
import { ContrattoService } from './contratto.service';
import { CreateContrattoDto } from './dto/create-contratto.dto';
import { UpdateContrattoDto } from './dto/update-contratto.dto';
import { ApiTags, ApiOkResponse } from '@nestjs/swagger';
import { GetContrattoDto } from './dto/get-contratto.dto';
import {
  BlockForDemo,
  IdAssociazione,
  Plans,
  Roles,
} from '../../auth/auth.decorator';
import { RuoloEnum } from '../membro/enums/ruolo.enum';
import { AuthGuard } from '../../auth/auth.guard';
import {
  ContrattiConverterDto,
  ContrattoConverterDto,
} from './contratto.converter';
import { SubscriptionPlanEnum } from '../../auth/interfaces/IWordPressSubscriptionResponse';

@Controller('contratti')
@ApiTags('Contratto')
export class ContrattoController {
  constructor(private readonly contrattoService: ContrattoService) {}

  @Post()
  @ApiOkResponse({
    description: 'Contratto creato correttamente',
    type: GetContrattoDto,
  })
  @Roles(
    RuoloEnum.Amministratore,
    RuoloEnum.Operatore,
    RuoloEnum.Segretario,
    RuoloEnum.Tesoriere,
  )
  @BlockForDemo()
  @Plans(SubscriptionPlanEnum.STANDARD)
  @UseGuards(AuthGuard)
  async create(
    @Body() createContrattoDto: CreateContrattoDto,
    @IdAssociazione() idAssociazione: string,
  ): Promise<GetContrattoDto> {
    const contrattoEntity = await this.contrattoService.create(
      createContrattoDto,
      idAssociazione,
    );
    return ContrattoConverterDto(contrattoEntity);
  }

  @Get()
  @ApiOkResponse({
    description: 'Lista dei contratti',
    type: [GetContrattoDto],
  })
  @Roles(
    RuoloEnum.Amministratore,
    RuoloEnum.Operatore,
    RuoloEnum.Segretario,
    RuoloEnum.Tesoriere,
  )
  @Plans(SubscriptionPlanEnum.STANDARD)
  @UseGuards(AuthGuard)
  async findAll(
    @IdAssociazione() idAssociazione: string,
    @Query('idCappella') idCappella: string,
    @Query('idTitolare') idTitolare: string,
  ): Promise<GetContrattoDto[]> {
    let contrattiEntity;
    if (idCappella)
      contrattiEntity = await this.contrattoService.findAllCappella(
        idAssociazione,
        idCappella,
      );
    else if (idTitolare)
      contrattiEntity = await this.contrattoService.findAllTitolare(
        idAssociazione,
        idTitolare,
      );
    else contrattiEntity = await this.contrattoService.findAll(idAssociazione);
    return ContrattiConverterDto(contrattiEntity);
  }

  @Get(':id')
  @ApiOkResponse({
    description: 'Restituisce contratto',
    type: GetContrattoDto,
  })
  @Roles(
    RuoloEnum.Amministratore,
    RuoloEnum.Operatore,
    RuoloEnum.Segretario,
    RuoloEnum.Tesoriere,
  )
  @Plans(SubscriptionPlanEnum.STANDARD)
  @UseGuards(AuthGuard)
  async findOne(
    @Param('id') id: string,
    @IdAssociazione() idAssociazione: string,
  ): Promise<GetContrattoDto> {
    const contrattoEntity = await this.contrattoService.findOne(
      id,
      idAssociazione,
    );
    return ContrattoConverterDto(contrattoEntity);
  }

  @Put(':id')
  @ApiOkResponse({
    description: 'Restituisce contratto',
    type: GetContrattoDto,
  })
  @Roles(
    RuoloEnum.Amministratore,
    RuoloEnum.Operatore,
    RuoloEnum.Segretario,
    RuoloEnum.Tesoriere,
  )
  @BlockForDemo()
  @Plans(SubscriptionPlanEnum.STANDARD)
  @UseGuards(AuthGuard)
  async update(
    @Param('id') id: string,
    @IdAssociazione() idAssociazione: string,
    @Body() updateContrattoDto: UpdateContrattoDto,
  ): Promise<GetContrattoDto> {
    const contrattoEntity = await this.contrattoService.update(
      id,
      idAssociazione,
      updateContrattoDto,
    );
    return ContrattoConverterDto(contrattoEntity);
  }

  @Delete(':id')
  @ApiOkResponse({
    description: 'Restituisce contratto',
    type: GetContrattoDto,
  })
  @Roles(
    RuoloEnum.Amministratore,
    RuoloEnum.Operatore,
    RuoloEnum.Segretario,
    RuoloEnum.Tesoriere,
  )
  @BlockForDemo()
  @Plans(SubscriptionPlanEnum.STANDARD)
  @UseGuards(AuthGuard)
  async remove(
    @Param('id') id: string,
    @IdAssociazione() idAssociazione: string,
  ): Promise<GetContrattoDto> {
    const contrattoEntity = await this.contrattoService.remove(
      id,
      idAssociazione,
    );
    return ContrattoConverterDto(contrattoEntity);
  }
}
