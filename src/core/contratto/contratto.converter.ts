import * as moment from 'moment';
import { GetContrattoDto } from './dto/get-contratto.dto';
import { ContrattoEntity } from './entities/contratto.entity';


export function ContrattoConverterDto(
  contrattoEntity: ContrattoEntity,
): GetContrattoDto {
  return {
    id: contrattoEntity.id,
    numeroContratto: contrattoEntity.numeroContratto,
    anniContratto: contrattoEntity.anniContratto,
    dataContratto: moment(contrattoEntity.dataContratto).format('YYYY-MM-DD'),
    dataScadenza: moment(contrattoEntity.dataScadenza).format('YYYY-MM-DD'),
    tipoContratto: contrattoEntity.tipoContratto,
    tipologiaCappella: contrattoEntity.tipologiaCappella,
    importo: contrattoEntity.importo,
    idCappella: contrattoEntity.cappellaId,
    nomeCappella: contrattoEntity.nomeCappella,
    idMembroTitolare: contrattoEntity.titolareId,
    idMembroDeceduto: contrattoEntity.decedutoId,
  };
}

export function ContrattiConverterDto(
  cappelleEntity: ContrattoEntity[],
): GetContrattoDto[] {
  let response: GetContrattoDto[] = [];
  for (let i = 0; i < cappelleEntity.length; i++) {
    response.push(ContrattoConverterDto(cappelleEntity[i]));
  }
  console.log(GetContrattoDto);
  return response;
}
