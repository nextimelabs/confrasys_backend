import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateContrattoDto } from './dto/create-contratto.dto';
import { UpdateContrattoDto } from './dto/update-contratto.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ContrattoEntity } from './entities/contratto.entity';
import { LibroGiornaleService } from '../libro-giornale/libro-giornale.service';
import { MembroService } from '../membro/membro.service';
import { CategorieMovimentoEnum } from '../libro-giornale/enums/categorie-movimento.enum';
import { TipiMovimentoEnum } from '../libro-giornale/enums/tipi-movimento.enum';
import { CappellaService } from '../cappella/cappella.service';
import { TipoContrattoEnum } from './enums/tipo-contratto.enum';
import { httpstatus } from 'aws-sdk/clients/glacier';
import { MovimentoConverterDto } from '../libro-giornale/libro-giornale.converter';
import { CappellaEntity } from '../cappella/entities/cappella.entity';
import { tipologiaCappellaEnum } from '../cappella/enum/tipologia-cappella.enum';

@Injectable()
export class ContrattoService {
  constructor(
    @InjectRepository(ContrattoEntity)
    private contrattoRepository: Repository<ContrattoEntity>,
    private membroService: MembroService,
    private cappellaService: CappellaService,
    private libroGiornaleService: LibroGiornaleService,
  ) { }

  async create(
    createContrattoDto: CreateContrattoDto,
    idAssociazione: string,
  ): Promise<ContrattoEntity> {
    // Controllare se ci sono posti disponibili
    let postiTotali;
    if (createContrattoDto.tipoContratto === TipoContrattoEnum.Loculo)
      postiTotali = (
        await this.cappellaService.findOne(
          createContrattoDto.idCappella,
          idAssociazione,
        )
      ).numeroLoculi;
    if (createContrattoDto.tipoContratto === TipoContrattoEnum.Ossario)
      postiTotali = (
        await this.cappellaService.findOne(
          createContrattoDto.idCappella,
          idAssociazione,
        )
      ).numeroOssari;
    const postiDisponibili = await this.getNumeroContratti(
      idAssociazione,
      createContrattoDto.idCappella,
    );
    if (postiDisponibili == postiTotali)
      throw new HttpException(
        'Posti disponibili terminati',
        HttpStatus.BAD_REQUEST,
      );

    const numeroContratto = await this.getNextNumero(idAssociazione);
    //Creo movimento in contabilità
    let membro = await this.membroService.findOne(
      createContrattoDto.idMembroTitolare,
      idAssociazione,
    );
    const movimento = await this.libroGiornaleService.create(
      {
        importo: createContrattoDto.importo,
        dataMovimento: createContrattoDto.dataContratto,
        causale: `CONTRATTO N. ${numeroContratto} INTESTATO A ${membro.cognome} ${membro.nome}`,
        categoria: CategorieMovimentoEnum.Contratto,
        tipo: TipiMovimentoEnum.Entrata,
        idMembroTitolare: '',
        annoRiferimento: null,
      },
      idAssociazione,
    );

    let contrattoEntity: ContrattoEntity = {
      ...createContrattoDto,
      numeroContratto: numeroContratto,
      cappellaId: createContrattoDto.idCappella,
      titolareId: createContrattoDto.idMembroTitolare,
      decedutoId: createContrattoDto.idMembroDeceduto,
      anniContratto: createContrattoDto.anniContratto,
      dataScadenza: createContrattoDto.dataScadenza,
      importo: createContrattoDto.importo,
      tipologiaCappella: createContrattoDto.tipologiaCappella,
      associazioneId: idAssociazione,
      movimentoId: movimento.id,
    };
    contrattoEntity = await this.contrattoRepository.save(contrattoEntity);

    return contrattoEntity;
  }

  async findAll(idAssociazione: string): Promise<ContrattoEntity[]> {
    return this.contrattoRepository
      .createQueryBuilder('contratto')
      .innerJoinAndSelect('contratto.cappella', 'cappella')
      .where('contratto.associazioneId = :idAssociazione', { idAssociazione })
      .getMany();
  }

  async findAllTitolare(
    idAssociazione: string,
    idTitolare: string,
  ): Promise<ContrattoEntity[]> {
    return this.contrattoRepository
      .createQueryBuilder('contratto')
      .innerJoinAndSelect('contratto.cappella', 'cappella')
      .where('contratto.associazioneId = :idAssociazione', { idAssociazione })
      .where('contratto.titolareId = :idTitolare', { idTitolare })
      .getMany();
  }

  async findAllCappella(
    idAssociazione: string,
    idCappella: string,
  ): Promise<ContrattoEntity[]> {
    return this.contrattoRepository
      .createQueryBuilder('contratto')
      .innerJoinAndSelect('contratto.cappella', 'cappella')
      .where('contratto.associazioneId = :idAssociazione', { idAssociazione })
      .where('contratto.cappellaId = :idCappella', { idCappella })
      .getMany();
  }

  async findOne(id: string, idAssociazione: string): Promise<ContrattoEntity> {
    const contrattoEntity = await this.contrattoRepository
      .createQueryBuilder('contratto')
      .innerJoinAndSelect('contratto.cappella', 'cappella')
      .where('contratto.associazioneId = :idAssociazione', { idAssociazione })
      .andWhere('contratto.id = :id', { id })
      .getOne();
    if (!contrattoEntity)
      throw new HttpException('Contratto non trovato', HttpStatus.NOT_FOUND);
    return contrattoEntity;
  }

  async update(
    id: string,
    idAssociazione: string,
    updateContrattoDto: UpdateContrattoDto,
  ) {
    let contrattoEntity = await this.findOne(id, idAssociazione);
    if (!contrattoEntity)
      throw new HttpException('Contratto non trovato', HttpStatus.NOT_FOUND);

    // Controllare se cambiando la cappella ci sono posti disponibili
    if (contrattoEntity.cappellaId !== updateContrattoDto.idCappella) {
      let postiTotali;
      if (updateContrattoDto.tipoContratto === TipoContrattoEnum.Loculo)
        postiTotali = (
          await this.cappellaService.findOne(
            updateContrattoDto.idCappella,
            idAssociazione,
          )
        ).numeroLoculi;
      if (updateContrattoDto.tipoContratto === TipoContrattoEnum.Ossario)
        postiTotali = (
          await this.cappellaService.findOne(
            updateContrattoDto.idCappella,
            idAssociazione,
          )
        ).numeroOssari;
      const postiDisponibili = await this.getNumeroContratti(
        idAssociazione,
        updateContrattoDto.idCappella,
      );
      if (postiDisponibili == postiTotali)
        throw new HttpException(
          'Posti disponibili terminati',
          HttpStatus.BAD_REQUEST,
        );
    }

    contrattoEntity = {
      ...contrattoEntity,
      ...updateContrattoDto,
      id: contrattoEntity.id,
      numeroContratto: contrattoEntity.numeroContratto,
      // importo: contrattoEntity.importo,
      // anniContratto: contrattoEntity.anniContratto,
      // dataScadenza: contrattoEntity.dataScadenza,
      cappellaId: updateContrattoDto.idCappella,
      titolareId: updateContrattoDto.idMembroTitolare,
      decedutoId: updateContrattoDto.idMembroDeceduto,
      tipologiaCappella: updateContrattoDto.tipologiaCappella,
      associazioneId: idAssociazione,
    };
    // Aggiorno movimento in contabilità
    if (contrattoEntity.movimentoId) {
      let membro = await this.membroService.findOne(
        updateContrattoDto.idMembroTitolare,
        idAssociazione,
      );
      await this.libroGiornaleService.update(
        contrattoEntity.movimentoId,
        idAssociazione,
        {
          importo: updateContrattoDto.importo,
          dataMovimento: updateContrattoDto.dataContratto,
          causale: `CONTRATTO N. ${contrattoEntity.numeroContratto} INTESTATO A ${membro.cognome} ${membro.nome}`,
        },
      );
    }

    return this.contrattoRepository.save(contrattoEntity);
  }

  async remove(id: string, idAssociazione: string): Promise<ContrattoEntity> {
    let contrattoEntity = await this.findOne(id, idAssociazione);
    if (!contrattoEntity)
      throw new HttpException('Contratto non trovato', HttpStatus.NOT_FOUND);

    // Rimuovo movimento dalla contabilità
    if (contrattoEntity.movimentoId)
      await this.libroGiornaleService.remove(
        contrattoEntity.movimentoId,
        idAssociazione,
      );

    return this.contrattoRepository.remove(contrattoEntity);
  }

  async getNextNumero(idAssociazione: string): Promise<number> {
    const ultimoContratto: ContrattoEntity = await this.contrattoRepository.findOne(
      {
        where: {
          associazioneId: idAssociazione,
        },
        order: {
          numeroContratto: -1,
        },
      },
    );
    if (!ultimoContratto) return 1;
    else return ultimoContratto.numeroContratto + 1;
  }

  async getNumeroContratti(
    idAssociazione: string,
    idCappella: string,
  ): Promise<number> {
    return this.contrattoRepository
      .createQueryBuilder('contratto')
      .where('contratto.associazioneId = :idAssociazione', { idAssociazione })
      .where('contratto.cappellaId = :idCappella', { idCappella })
      .getCount();
  }

  async getNumeroContrattiAttivi(
    idAssociazione: string,
    idCappella: string,
  ): Promise<number> {
    return this.contrattoRepository
      .createQueryBuilder('contratto')
      .where('contratto.data = :idAssociazione', { idAssociazione })
      .where('contratto.associazioneId = :idAssociazione', { idAssociazione })
      .where('contratto.cappellaId = :idCappella', { idCappella })
      .getCount();
  }
}
