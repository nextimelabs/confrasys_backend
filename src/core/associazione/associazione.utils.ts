import { HttpException, HttpStatus, Logger } from '@nestjs/common';

export const documentFileFilter = (req, file, callback) => {
  if (!file.originalname.match(/\.(pdf|PDF)$/)) {
    return callback(
      new HttpException(
        'Sono ammessi solo file di tipo PDF!',
        HttpStatus.BAD_REQUEST,
      ),
      false,
    );
  }
  callback(null, true);
};

export const documentLogoFileFilter = (req, file, callback) => {
  if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG)$/)) {
    return callback(
      new HttpException(
        'Sono ammessi solo file di tipo JPG e PNG!',
        HttpStatus.BAD_REQUEST,
      ),
      false,
    );
  }
  callback(null, true);
};
