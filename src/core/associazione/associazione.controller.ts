import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  Post,
  Put,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiTags,
  ApiOkResponse,
  ApiBadRequestResponse,
  ApiConsumes,
  ApiBody,
} from '@nestjs/swagger';
import { Response } from 'express';
import {
  BlockForDemo,
  EntityEnum,
  IdAssociazione,
  Plans,
  Private,
  Roles,
  RolesAndPrivate,
  RolesOrPrivate,
} from '../../auth/auth.decorator';
import { AssociazioneService } from './associazione.service';
import { CreateAssociazioneDto } from './dto/create-associazione.dto';
import { RuoloEnum } from '../membro/enums/ruolo.enum';
import { AssociazioneConverterDto } from './associazione.converter';
import { GetAssociazioneDto } from './dto/get-associazione.dto';
import { AuthGuard } from '../../auth/auth.guard';
import { UpdateAssociazioneDto } from './dto/update-associazione.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  documentFileFilter,
  documentLogoFileFilter,
} from './associazione.utils';
import { AssociazioneEntity } from './entities/associazione.entity';
import {
  SubscriptionPlanEnum,
  SubscriptionPlans,
  SubscriptionPlanTypes,
} from '../../auth/interfaces/IWordPressSubscriptionResponse';

@ApiBearerAuth()
@ApiTags('Associazione')
@Controller('associazione')
export class AssociazioneController {
  constructor(private readonly associazioneService: AssociazioneService) {}

  @Get(':id')
  @RolesAndPrivate({
    roles: [],
    entity: EntityEnum.Associazione,
  })
  @ApiOkResponse({
    description: "Restituisce l'associazione da id",
    type: GetAssociazioneDto,
  })
  @UseGuards(AuthGuard)
  async findOne(@Param('id') id: string) {
    let associazione = await this.associazioneService.findOne(id);
    if (!associazione)
      throw new HttpException('Associazione non trovata', HttpStatus.NOT_FOUND);
    return AssociazioneConverterDto(associazione);
  }

  @UsePipes(new ValidationPipe())
  @Put(':id')
  @RolesAndPrivate({
    roles: [RuoloEnum.Amministratore],
    entity: EntityEnum.Associazione,
  })
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async update(
    @Body() updateAssociazioneDto: UpdateAssociazioneDto,
    @IdAssociazione() idAssociazione: string,
  ) {
    const updateAssociazione: Partial<AssociazioneEntity> = {
      ...updateAssociazioneDto,
    };
    let associazione = await this.associazioneService.update(
      idAssociazione,
      updateAssociazione,
    );
    return AssociazioneConverterDto(associazione);
  }

  @Post(':id/statuto')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        documento: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiOkResponse({
    description: 'Documento caricato correttamente',
    type: GetAssociazioneDto,
  })
  @ApiBadRequestResponse({
    description: 'Sono ammessi solo file di tipo PDF e massimo 2MB!',
  })
  @UseInterceptors(
    FileInterceptor('statuto', {
      limits: {
        fieldSize: 2,
      },
      fileFilter: documentFileFilter,
    }),
  )
  @RolesAndPrivate({
    roles: [RuoloEnum.Amministratore],
    entity: EntityEnum.Associazione,
  })
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async uploadDocumento(
    @IdAssociazione() idAssociazione: string,
    @UploadedFile() file,
  ) {
    if (!file || file.buffer === undefined)
      throw new HttpException(
        'Il documento è obbligatorio',
        HttpStatus.BAD_REQUEST,
      );
    let associazione = await this.associazioneService.findOne(idAssociazione);
    if (!associazione)
      throw new HttpException('Associazione non trovata', HttpStatus.NOT_FOUND);

    await this.associazioneService.uploadStatuo(associazione, file);
    return AssociazioneConverterDto(
      await this.associazioneService.findOne(idAssociazione),
    );
  }

  @Get(':id/statuto')
  @ApiOkResponse({
    description: 'Viene scaricato il documento dello statuto associativo',
  })
  @RolesAndPrivate({
    roles: [RuoloEnum.Amministratore],
    entity: EntityEnum.Associazione,
  })
  @UseGuards(AuthGuard)
  async downloadDocumento(
    @IdAssociazione() idAssociazione: string,
  ): Promise<any> {
    let associazione = await this.associazioneService.findOne(idAssociazione);
    if (!associazione)
      throw new HttpException('Associazione non trovata', HttpStatus.NOT_FOUND);
    if (associazione.statuto === '' || associazione.statuto === null) {
      throw new HttpException(
        'Documento non presente nel sistema',
        HttpStatus.NOT_FOUND,
      );
    }
    let fileUri = await this.associazioneService.downloadStatuto(
      associazione.statuto,
    );

    return {
      uri: fileUri,
    };
  }

  @Delete(':id/statuto')
  @ApiOkResponse({
    description: 'Viene eliminato il documento dello statuto associativo',
  })
  @RolesAndPrivate({
    roles: [RuoloEnum.Amministratore],
    entity: EntityEnum.Associazione,
  })
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async deleteDocumento(
    @IdAssociazione() idAssociazione: string,
  ): Promise<any> {
    let associazione = await this.associazioneService.findOne(idAssociazione);
    if (!associazione)
      throw new HttpException('Associazione non trovata', HttpStatus.NOT_FOUND);
    if (associazione.statuto === '' || associazione.statuto === null) {
      throw new HttpException(
        'Documento non presente nel sistema',
        HttpStatus.NOT_FOUND,
      );
    }
    return await this.associazioneService.deleteStatuto(
      associazione,
      associazione.statuto,
    );
  }

  @Post(':id/regolamento')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        documento: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiOkResponse({
    description: 'Documento caricato correttamente',
    type: GetAssociazioneDto,
  })
  @ApiBadRequestResponse({
    description: 'Sono ammessi solo file di tipo PDF e massimo 2MB!',
  })
  @UseInterceptors(
    FileInterceptor('regolamento', {
      limits: {
        fieldSize: 2,
      },
      fileFilter: documentFileFilter,
    }),
  )
  @RolesAndPrivate({
    roles: [RuoloEnum.Amministratore],
    entity: EntityEnum.Associazione,
  })
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async uploadRegolamento(
    @IdAssociazione() idAssociazione: string,
    @UploadedFile() file,
  ) {
    if (!file || file.buffer === undefined)
      throw new HttpException(
        'Il documento è obbligatorio',
        HttpStatus.BAD_REQUEST,
      );
    let associazione = await this.associazioneService.findOne(idAssociazione);
    if (!associazione)
      throw new HttpException('Associazione non trovata', HttpStatus.NOT_FOUND);

    await this.associazioneService.uploadRegolamento(associazione, file);
    return AssociazioneConverterDto(
      await this.associazioneService.findOne(idAssociazione),
    );
  }

  @Get(':id/regolamento')
  @ApiOkResponse({
    description: 'Viene scaricato il documento dello statuto associativo',
  })
  @RolesAndPrivate({
    roles: [RuoloEnum.Amministratore],
    entity: EntityEnum.Associazione,
  })
  @UseGuards(AuthGuard)
  async downloadRegolamento(
    @IdAssociazione() idAssociazione: string,
  ): Promise<any> {
    let associazione = await this.associazioneService.findOne(idAssociazione);
    if (!associazione)
      throw new HttpException('Associazione non trovata', HttpStatus.NOT_FOUND);
    if (associazione.regolamento === '' || associazione.regolamento === null) {
      throw new HttpException(
        'Documento non presente nel sistema',
        HttpStatus.NOT_FOUND,
      );
    }
    let fileUri = await this.associazioneService.downloadRegolamento(
      associazione.regolamento,
    );

    return {
      uri: fileUri,
    };
  }

  @Delete(':id/regolamento')
  @ApiOkResponse({
    description: 'Viene eliminato il regolamento dello statuto associativo',
  })
  @RolesAndPrivate({
    roles: [RuoloEnum.Amministratore],
    entity: EntityEnum.Associazione,
  })
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async deleteRegolamento(
    @IdAssociazione() idAssociazione: string,
  ): Promise<any> {
    let associazione = await this.associazioneService.findOne(idAssociazione);
    if (!associazione)
      throw new HttpException('Associazione non trovata', HttpStatus.NOT_FOUND);
    if (associazione.regolamento === '' || associazione.regolamento === null) {
      throw new HttpException(
        'Documento non presente nel sistema',
        HttpStatus.NOT_FOUND,
      );
    }
    return await this.associazioneService.deleteRegolamento(
      associazione,
      associazione.regolamento,
    );
  }

  @Post(':id/logo')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        documento: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @ApiOkResponse({
    description: 'Logo caricato correttamente',
    type: GetAssociazioneDto,
  })
  @ApiBadRequestResponse({
    description: 'Sono ammessi solo file di tipo PNG e JPG e massimo 1MB!',
  })
  @UseInterceptors(
    FileInterceptor('logo', {
      limits: {
        fieldSize: 1,
      },
      fileFilter: documentLogoFileFilter,
    }),
  )
  @RolesAndPrivate({
    roles: [RuoloEnum.Amministratore],
    entity: EntityEnum.Associazione,
  })
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async uploadLogo(
    @IdAssociazione() idAssociazione: string,
    @UploadedFile() file,
  ) {
    if (!file || file.buffer === undefined)
      throw new HttpException(
        'Il documento è obbligatorio',
        HttpStatus.BAD_REQUEST,
      );
    let associazione = await this.associazioneService.findOne(idAssociazione);
    if (!associazione)
      throw new HttpException('Associazione non trovata', HttpStatus.NOT_FOUND);

    await this.associazioneService.uploadLogo(associazione, file);
    return AssociazioneConverterDto(
      await this.associazioneService.findOne(idAssociazione),
    );
  }

  @Get(':id/logo')
  @ApiOkResponse({
    description: 'Viene scaricato il logo',
  })
  async downloadlogo(@Param('id') idAssociazione: string): Promise<any> {
    let associazione = await this.associazioneService.findOne(idAssociazione);
    if (!associazione)
      throw new HttpException('Associazione non trovata', HttpStatus.NOT_FOUND);
    if (associazione.logo === '' || associazione.logo === null) {
      throw new HttpException(
        'Logo non presente nel sistema',
        HttpStatus.NOT_FOUND,
      );
    }
    let fileUri = await this.associazioneService.downloadLogo(
      associazione.logo,
    );

    return {
      uri: fileUri,
    };
  }

  @Delete(':id/logo')
  @ApiOkResponse({
    description: 'Viene eliminato il logo dello statuto associativo',
  })
  @RolesAndPrivate({
    roles: [RuoloEnum.Amministratore],
    entity: EntityEnum.Associazione,
  })
  @BlockForDemo()
  @UseGuards(AuthGuard)
  async deleteLogo(@IdAssociazione() idAssociazione: string): Promise<any> {
    let associazione = await this.associazioneService.findOne(idAssociazione);
    if (!associazione)
      throw new HttpException('Associazione non trovata', HttpStatus.NOT_FOUND);
    if (associazione.logo === '' || associazione.logo === null) {
      throw new HttpException(
        'Documento non presente nel sistema',
        HttpStatus.NOT_FOUND,
      );
    }
    return await this.associazioneService.deleteLogo(
      associazione,
      associazione.logo,
    );
  }
}
