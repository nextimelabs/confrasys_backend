import {
  IsAlphanumeric,
  IsArray,
  IsEmail,
  IsISO8601,
  IsMobilePhone,
  IsNotEmpty,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { PartialType, ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { CreateAssociazioneDto } from './create-associazione.dto';
import { TipoAssociazioneEnum } from '../enums/tipo-associazione.enum';

export class UpdateAssociazioneDto {
  @ApiProperty()
  @IsNotEmpty()
  nome: string;

  @ApiProperty()
  @IsISO8601()
  dataFondazione: string;

  @ApiProperty()
  codiceFiscale: string;

  @ApiPropertyOptional()
  @IsMobilePhone()
  numeroTelefono: string;

  @ApiPropertyOptional()
  @IsEmail()
  email: string;

  @ApiPropertyOptional()
  @IsNumberString()
  @IsOptional()
  importoQuotaDefault: number;

  @ApiPropertyOptional()
  @IsNumberString()
  @IsOptional()
  importoQuotaNonResidentiDefault: number;

  @ApiProperty()
  @IsString()
  @IsOptional()
  titoloComunicazione: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
  messaggioComunicazione: string;
}
