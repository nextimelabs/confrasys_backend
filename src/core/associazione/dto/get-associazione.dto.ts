import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { TipoAssociazioneEnum } from '../enums/tipo-associazione.enum';
import {
  IsAlphanumeric,
  IsISO8601,
  IsEmail,
  IsMobilePhone,
  IsNotEmpty,
  MaxLength,
  MinLength,
  IsNumber,
  IsString,
  IsPostalCode,
  IsDateString,
} from 'class-validator';

export class GetAssociazioneDto {
  @ApiProperty()
  @IsNotEmpty()
  id: string;

  @ApiProperty()
  @IsNotEmpty()
  nome: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsAlphanumeric()
  @MinLength(5)
  @MaxLength(15)
  alias: string;

  @ApiProperty()
  @IsISO8601()
  dataFondazione: string;

  @ApiProperty()
  codiceFiscale: string;

  @ApiPropertyOptional()
  @IsNumber()
  importoQuotaDefault: number;

  @ApiPropertyOptional()
  @IsNumber()
  importoQuotaNonResidentiDefault: number;

  @ApiPropertyOptional()
  @IsNumber()
  durataMesiQuotaDefault: number;

  @ApiPropertyOptional()
  @IsDateString()
  giornoInizioQuotaDefault: Date;

  @ApiPropertyOptional()
  statuto: string;

  @ApiPropertyOptional()
  regolamento: string;

  @ApiProperty()
  @IsString()
  titoloComunicazione: string;

  @ApiProperty()
  @IsString()
  messaggioComunicazione: string;

  @ApiPropertyOptional()
  @IsMobilePhone()
  numeroTelefono: string;

  @ApiPropertyOptional()
  @IsEmail()
  email: string;

  @ApiPropertyOptional()
  logo: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  indirizzo: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(10)
  civico: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  comune: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(2)
  provincia: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsPostalCode('IT')
  CAP: string;
}
