import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { TipoAssociazioneEnum } from '../enums/tipo-associazione.enum';
import {
  IsAlphanumeric,
  IsISO8601,
  IsEmail,
  IsMobilePhone,
  IsNotEmpty,
  MaxLength,
  MinLength,
  IsString,
  IsPostalCode,
} from 'class-validator';

export class CreateAssociazioneDto {
  @ApiProperty()
  @IsNotEmpty()
  wpUserId: number;

  @ApiProperty()
  @IsNotEmpty()
  nome: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsAlphanumeric()
  @MinLength(5)
  @MaxLength(20)
  alias: string;

  @ApiProperty()
  @IsISO8601()
  dataFondazione: string;

  @ApiProperty()
  codiceFiscale: string;

  @ApiPropertyOptional()
  @IsMobilePhone()
  numeroTelefono: string;

  @ApiPropertyOptional()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  indirizzo: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(10)
  civico: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(50)
  comune: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(2)
  provincia: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsPostalCode('IT')
  CAP: string;
}
