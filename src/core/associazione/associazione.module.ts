import { Module } from '@nestjs/common';
import { AssociazioneService } from './associazione.service';
import { AssociazioneController } from './associazione.controller';
import { AssociazioneEntity } from './entities/associazione.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MembroEntity } from '../membro/entities/membro.entity';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [
    TypeOrmModule.forFeature([MembroEntity, AssociazioneEntity]),
    HttpModule,
  ],
  providers: [AssociazioneService],
  controllers: [AssociazioneController],
  exports: [AssociazioneService, HttpModule],
})
export class AssociazioneModule {}
