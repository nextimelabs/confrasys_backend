import * as mongoose from 'mongoose';
import {
  TipoAssociazione,
  TipoAssociazioneEnum,
} from '../enums/tipo-associazione.enum';
import { Qualifica } from '../../membro/enums/qualifica.enum';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('associazioni')
export class AssociazioneEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column('integer', { nullable: true })
  wpUserId: number;
  @Column('varchar', { nullable: false })
  nome: string;
  @Column('varchar', { length: 20, nullable: false })
  alias: string;
  @Column('timestamp without time zone', { nullable: false })
  dataFondazione: string;
  @Column('varchar', { length: 22, nullable: false })
  codiceFiscale: string;
  @Column('timestamp without time zone', { nullable: false })
  dataIscrizione: string;
  @Column('varchar', { length: 255, nullable: true })
  statuto?: string;
  @Column('varchar', { length: 255, nullable: true })
  regolamento?: string;
  @Column('text', { nullable: true })
  titoloComunicazione?: string;
  @Column('text', { nullable: true })
  messaggioComunicazione?: string;
  @Column('varchar', { length: 50, nullable: false })
  numeroTelefono: string;
  @Column('varchar', { length: 50, nullable: false })
  email: string;
  @Column('varchar', { length: 255, nullable: true })
  logo: string;

  // Indirizzo Sede Operativa
  @Column('varchar', { length: 50, nullable: false, default: '' })
  indirizzo: string;
  @Column('varchar', { length: 10, nullable: false, default: '' })
  civico: string;
  @Column('varchar', { length: 50, nullable: false, default: '' })
  comune: string;
  @Column('varchar', { length: 2, nullable: false, default: '' })
  provincia: string;
  @Column('varchar', { length: 10, nullable: false, default: '' })
  CAP: string;

  // Impostazione quota associativa
  @Column('double precision', { nullable: true })
  importoQuotaDefault?: number;
  @Column('double precision', { nullable: true })
  importoQuotaNonResidentiDefault?: number;
  @Column('integer', { nullable: true })
  durataMesiQuotaDefault?: number;
  @Column('timestamp without time zone', { nullable: true })
  giornoInizioQuotaDefault?: Date;
}
