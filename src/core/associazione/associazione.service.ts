import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import S3 = require('aws-sdk/clients/s3');
import { AssociazioneEntity } from './entities/associazione.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import * as environment from '../../../environment/dev.environment.json';
import { IWordPressSubscriptionResponse } from '../../auth/interfaces/IWordPressSubscriptionResponse';
import { HttpService } from '@nestjs/axios';

@Injectable()
export class AssociazioneService {
  BASE_PATH = 'confraternite';

  constructor(
    @InjectRepository(AssociazioneEntity)
    private associazioneModel: Repository<AssociazioneEntity>,
    private httpService: HttpService,
  ) {}

  async create(associazione: AssociazioneEntity): Promise<AssociazioneEntity> {
    const controlloAssociazione = await this.associazioneModel.findOne({
      alias: associazione.alias,
    });
    if (controlloAssociazione)
      throw new HttpException(
        'Alias associazione già utilizzato',
        HttpStatus.BAD_REQUEST,
      );

    return this.associazioneModel.save(associazione);
  }

  async findOne(idAssociazione: string) {
    return await this.associazioneModel.findOne({ id: idAssociazione });
  }

  async checkAlias(alias: string) {
    return await this.associazioneModel.findOne({ alias: alias });
  }

  async checkPreSignup(userId: number): Promise<boolean> {
    const res = await this.httpService
      .get<IWordPressSubscriptionResponse>(
        `${process.env.WP_SUBSCRIPTION_API ||
          environment.WP_SUBSCRIPTION_API}/${userId}`,
      )
      .toPromise();
    let valid = false;
    if (res.data && res.data.length > 0) {
      const subscriptions: IWordPressSubscriptionResponse = res.data;
      for (const subscription of subscriptions) {
        if (subscription.status === 'active') valid = true;
      }
    }
    if (!valid) {
      throw new HttpException(
        { code: 'INVALID_SUBSCRIPTION' },
        HttpStatus.BAD_REQUEST,
      );
    }

    const controlloAssociazione = await this.associazioneModel.findOne({
      wpUserId: userId,
    });
    if (controlloAssociazione)
      throw new HttpException(
        { code: 'USER_ALREADY_SIGNED_UP' },
        HttpStatus.BAD_REQUEST,
      );

    return true;
  }

  async update(
    id: string,
    { alias, ...updateAssociazione }: Partial<AssociazioneEntity>,
  ) {
    return await this.associazioneModel.save({
      id: id,
      ...updateAssociazione,
    });
  }

  async uploadStatuo(associazione: AssociazioneEntity, file: any) {
    const key = `associazioni/${associazione.id}/statuto associativo.pdf`;

    const s3 = new S3();
    const uploadResult = await s3
      .upload({
        Bucket: process.env.S3_BUCKET || environment.S3_BUCKET,
        Body: file.buffer,
        ContentEncoding: 'base64',
        ContentType: file.mimetype,
        Key: key,
      })
      .promise();

    return await this.associazioneModel.save({
      id: associazione.id,
      statuto: key,
    });
  }

  async downloadStatuto(fileKey: string) {
    const s3 = new S3();
    const fileUri = await s3.getSignedUrlPromise('getObject', {
      Bucket: process.env.S3_BUCKET || environment.S3_BUCKET,
      Key: fileKey,
    });
    return fileUri;
  }

  async deleteStatuto(associazione: AssociazioneEntity, fileKey: string) {
    const s3 = new S3();
    await s3.deleteObject({
      Bucket: process.env.S3_BUCKET || environment.S3_BUCKET,
      Key: fileKey,
    });
    return await this.associazioneModel.save({
      id: associazione.id,
      statuto: null,
    });
  }

  async uploadRegolamento(associazione: AssociazioneEntity, file: any) {
    const key = `associazioni/${associazione.id}/regolamento associativo.pdf`;

    const s3 = new S3();
    const uploadResult = await s3
      .upload({
        Bucket: process.env.S3_BUCKET || environment.S3_BUCKET,
        Body: file.buffer,
        ContentEncoding: 'base64',
        ContentType: file.mimetype,
        Key: key,
      })
      .promise();

    return await this.associazioneModel.save({
      id: associazione.id,
      regolamento: key,
    });
  }

  async downloadRegolamento(fileKey: string) {
    const s3 = new S3();
    const fileUri = await s3.getSignedUrlPromise('getObject', {
      Bucket: process.env.S3_BUCKET || environment.S3_BUCKET,
      Key: fileKey,
    });
    return fileUri;
  }

  async deleteRegolamento(associazione: AssociazioneEntity, fileKey: string) {
    const s3 = new S3();
    await s3.deleteObject({
      Bucket: process.env.S3_BUCKET || environment.S3_BUCKET,
      Key: fileKey,
    });
    return await this.associazioneModel.save({
      id: associazione.id,
      regolamento: null,
    });
  }

  async uploadLogo(associazione: AssociazioneEntity, file: any) {
    Logger.log('UPLOAD LOGO');
    Logger.log(file);
    Logger.log(file.buffer);

    const key = `associazioni/${associazione.id}/logo.${file.originalname
      .split('.')
      .pop()}`;

    const s3 = new S3();
    const uploadResult = await s3
      .upload({
        Bucket: process.env.S3_BUCKET || environment.S3_BUCKET,
        Body: file.buffer,
        ContentEncoding: 'base64',
        ContentType: file.mimetype,
        Key: key,
      })
      .promise();

    return await this.associazioneModel.save({
      id: associazione.id,
      logo: key,
    });
  }

  async downloadLogo(fileKey: string) {
    const s3 = new S3();
    const fileUri = await s3.getSignedUrlPromise('getObject', {
      Bucket: process.env.S3_BUCKET || environment.S3_BUCKET,
      Key: fileKey,
    });
    return fileUri;
  }

  async deleteLogo(associazione: AssociazioneEntity, fileKey: string) {
    const s3 = new S3();
    await s3.deleteObject({
      Bucket: process.env.S3_BUCKET || environment.S3_BUCKET,
      Key: fileKey,
    });
    return await this.associazioneModel.save({
      id: associazione.id,
      logo: null,
    });
  }
}
