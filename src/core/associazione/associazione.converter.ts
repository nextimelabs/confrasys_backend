import { GetAssociazioneDto } from './dto/get-associazione.dto';
import { AssociazioneEntity } from './entities/associazione.entity';
import * as moment from 'moment-timezone';
// @ts-ignore
import * as environment from '../../../environment/dev.environment';
import S3 = require('aws-sdk/clients/s3');

export function AssociazioneConverterDto(
  associazioneDocument: AssociazioneEntity,
): GetAssociazioneDto {
  const s3 = new S3();
  let statuto = null;
  let regolamento = null;
  let logo = null;
  if (associazioneDocument.statuto) {
    statuto = s3.getSignedUrl('getObject', {
      Bucket: process.env.S3_BUCKET || environment.S3_BUCKET,
      Key: associazioneDocument.statuto,
    });
  }
  if (associazioneDocument.regolamento) {
    regolamento = s3.getSignedUrl('getObject', {
      Bucket: process.env.S3_BUCKET || environment.S3_BUCKET,
      Key: associazioneDocument.regolamento,
    });
  }
  if (associazioneDocument.logo) {
    logo = s3.getSignedUrl('getObject', {
      Bucket: process.env.S3_BUCKET || environment.S3_BUCKET,
      Key: associazioneDocument.logo,
    });
  }
  return {
    id: associazioneDocument.id,
    nome: associazioneDocument.nome,
    alias: associazioneDocument.alias,
    dataFondazione: moment(associazioneDocument.dataFondazione).format(
      'YYYY-MM-DD',
    ),
    codiceFiscale: associazioneDocument.codiceFiscale,
    statuto: statuto,
    regolamento: regolamento,
    importoQuotaDefault: associazioneDocument.importoQuotaDefault,
    importoQuotaNonResidentiDefault:
      associazioneDocument.importoQuotaNonResidentiDefault,
    durataMesiQuotaDefault: associazioneDocument.durataMesiQuotaDefault,
    giornoInizioQuotaDefault: associazioneDocument.giornoInizioQuotaDefault,
    numeroTelefono: associazioneDocument.numeroTelefono,
    email: associazioneDocument.email,
    titoloComunicazione: associazioneDocument.titoloComunicazione,
    messaggioComunicazione: associazioneDocument.messaggioComunicazione,
    logo: logo,
    indirizzo: associazioneDocument.indirizzo,
    civico: associazioneDocument.civico,
    provincia: associazioneDocument.provincia,
    comune: associazioneDocument.comune,
    CAP: associazioneDocument.CAP,
  };
}
export function AssociazioniConverterDto(
  membriDocument: AssociazioneEntity[],
): GetAssociazioneDto[] {
  let response: GetAssociazioneDto[] = [];
  membriDocument.map((associazioneDocument: AssociazioneEntity) =>
    response.push(AssociazioneConverterDto(associazioneDocument)),
  );
  return response;
}
