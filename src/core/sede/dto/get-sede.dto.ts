<<<<<<< HEAD
import {ApiProperty} from '@nestjs/swagger';
import * as mongoose from "mongoose";
import {IndirizzoDto} from "../../../shared/dto/indirizzo.dto";
import {IsDefined, IsNotEmpty, isNotEmpty, ValidateNested} from "class-validator";
import {Type} from "class-transformer";
=======
import { ApiProperty } from '@nestjs/swagger';
import { IndirizzoDto } from '../../../shared/dto/indirizzo.dto';
import { IsDefined, IsNotEmpty, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
>>>>>>> master

export class GetSedeDto {
  @ApiProperty({ required: true })
  @IsNotEmpty()
  id: string;

  @ApiProperty({ required: true })
  @IsNotEmpty()
  nome: string;

  @ApiProperty({ required: true })
  @IsDefined()
  @ValidateNested()
  @Type(() => IndirizzoDto)
  indirizzo: IndirizzoDto;
}
