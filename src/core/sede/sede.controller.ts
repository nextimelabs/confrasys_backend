import { Controller } from '@nestjs/common';
import { SedeService } from './sede.service';
import { ApiTags } from '@nestjs/swagger';

@Controller('sede')
@ApiTags('Sede')
export class SedeController {
  constructor(private readonly sedeService: SedeService) {}
}
