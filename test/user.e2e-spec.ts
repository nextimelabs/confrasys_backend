import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { MongooseModule } from '@nestjs/mongoose';
import { User } from '../src/user/user.schema';
import { UserService } from '../src/user/user.service';
import { RolesEnum } from '../src/auth/roles.enum';
import { UserInterface } from '../src/user/interface/user.interface';

//Login admin1
//----ADD USER
//nessuno prova a creare utente1
//admin crea utente1
//admin crea utente2
//admin crea admin2
//login utente1
//login utente2
//login admin2
//-----EDIT USER
//nessuno prova a modificare il profilo di admin
//Admin modifica il suo profilo
//utente1 prova modifica profilo di admin
//admin2 modifica profilo di admin
//nessuno prova a modificare il profilo di utente1
//Utente1 modifica il suo profilo
//Utente1 prova a modificare il profilo dell'utente2
//Utente1 prova a creare un nuovo utente3
//-----GET USER
//utente1 prova a get admin
//utente1 prova a get utente2
//utente1 get utente1
//------GET USERS
//utente1 prova get utenti
//admin get utenti
//------DELETE USER
//utente1 prova delete utente2
//utente1 delete utente1
//admin2 delete utente2
//admin1 delete utente2
//Crea admin3 (tenant2)
//------TENANTS
//admin3 prova a get utente1

jest.setTimeout(999999);

interface UserData {
  token: string;
  user: UserInterface;
  credentials: {
    username: string;
    password: string;
  };
}

describe('AppController (e2e)', () => {
  let app;
  let admin1: UserData = {
    token: null,
    user: null,
    credentials: {
      username: 'admin1',
      password: 'cisco,123',
    },
  };
  let admin2: UserData = {
    token: null,
    user: null,
    credentials: {
      username: 'admin2',
      password: 'cisco,123',
    },
  };
  let user1: UserData = {
    token: null,
    user: null,
    credentials: {
      username: 'user1',
      password: 'cisco,123',
    },
  };
  let user2: UserData = {
    token: null,
    user: null,
    credentials: {
      username: 'user2',
      password: 'cisco,123',
    },
  };
  let user3: UserData = {
    token: null,
    user: null,
    credentials: {
      username: 'user3',
      password: 'cisco,123',
    },
  };
  let admin3: UserData = {
    token: null,
    user: null,
    credentials: {
      username: 'admin3',
      password: 'cisco,123',
    },
  };
  let userService: UserService;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(
          'mongodb://admin:cisco,123@cluster0-shard-00-00.r05wh.mongodb.net:27017,cluster0-shard-00-01.r05wh.mongodb.net:27017,cluster0-shard-00-02.r05wh.mongodb.net:27017/test?ssl=true&replicaSet=atlas-1u88tb-shard-0&authSource=admin&retryWrites=true&w=majority',
        ),
        AppModule,
      ],
    }).compile();

    app = moduleFixture.createNestApplication(null, {
      logger: false,
    });
    await app.init();
    userService = moduleFixture.get<UserService>(UserService);
    await userService.clearDb();
  });

  afterAll(async () => {
    //await userService.userModel.remove({});
    await app.close();
  });

  it('Signin Admin1', async () => {
    await request(app.getHttpServer())
      .post('/signin')
      .send({
        ...admin1.credentials,
        tenantName: 'Società',
      })
      .expect(200);
  });

  it('Login admin1', async () => {
    return request(app.getHttpServer())
      .post('/login')
      .send(admin1.credentials)
      .expect(200)
      .then(res => {
        admin1.token = res.body.token;
        admin1.user = res.body.user;
      });
  });

  it('Nessuno prova a creare utente1', async () => {
    return request(app.getHttpServer())
      .post('/users')
      .send({
        ...user1.credentials,
        role: RolesEnum.User,
      })
      .expect(403);
  });

  it('Admin crea utente1', async () => {
    return request(app.getHttpServer())
      .post('/users')
      .send({
        ...user1.credentials,
        role: RolesEnum.User,
      })
      .set({ Authorization: 'Bearer: ' + admin1.token })
      .expect(200)
      .then(res => {
        user1.user = res.body;
      });
  });
  it('Admin crea utente2', async () => {
    return request(app.getHttpServer())
      .post('/users')
      .send({
        ...user2.credentials,
        role: RolesEnum.User,
      })
      .set({ Authorization: 'Bearer: ' + admin1.token })
      .expect(200)
      .then(res => {
        user2.user = res.body;
      });
  });
  it('Creazione admin2', async () => {
    return request(app.getHttpServer())
      .post('/users')
      .send({
        ...admin2.credentials,
        role: RolesEnum.Admin,
      })
      .set({ Authorization: 'Bearer: ' + admin1.token })
      .expect(200)
      .then(res => {
        admin2.user = res.body;
      });
  });

  it('Login utente1', async () => {
    return request(app.getHttpServer())
      .post('/login')
      .send(user1.credentials)
      .expect(200)
      .then(res => {
        user1.token = res.body.token;
      });
  });
  it('Login utente2', async () => {
    return request(app.getHttpServer())
      .post('/login')
      .send(user2.credentials)
      .expect(200)
      .then(res => {
        user2.token = res.body.token;
      });
  });
  it('Login admin2', async () => {
    return request(app.getHttpServer())
      .post('/login')
      .send(admin2.credentials)
      .expect(200)
      .then(res => {
        admin2.token = res.body.token;
      });
  });

  it('Nessuno prova a modificare il profilo di admin', async () => {
    return request(app.getHttpServer())
      .put('/users/' + admin1.user.id)
      .send({
        username: 'utente2',
      })
      .expect(403);
  });
  it('Admin modifica il suo profilo', async () => {
    return request(app.getHttpServer())
      .put('/users/' + admin1.user.id)
      .send({
        username: 'amministratore',
      })
      .set({ Authorization: 'Bearer: ' + admin1.token })
      .expect(200)
      .then(res => {
        admin1.user = res.body;
      });
  });

  it('utente1 prova a modificare il profilo di admin1', async () => {
    return request(app.getHttpServer())
      .put('/users/' + admin1.user.id)
      .send({
        username: 'utente2',
      })
      .set({ Authorization: 'Bearer: ' + user1.token })
      .expect(403);
  });

  it('Admin2 modifica il profilo di admin1', async () => {
    return request(app.getHttpServer())
      .put('/users/' + admin1.user.id)
      .send({
        username: 'amministratore brutto',
      })
      .set({ Authorization: 'Bearer: ' + admin2.token })
      .expect(200)
      .then(res => {
        admin1.user = res.body;
      });
  });

  it('Nessuno prova a modificare il profilo di utente1', async () => {
    return request(app.getHttpServer())
      .put('/users/' + user1.user.id)
      .send({
        username: 'utente1nuovo',
      })
      .expect(403);
  });
  it('Utente1 modifica il suo profilo', async () => {
    return request(app.getHttpServer())
      .put('/users/' + user1.user.id)
      .send({
        username: 'utente1nuovo',
      })
      .set({ Authorization: 'Bearer: ' + user1.token })
      .expect(200)
      .then(res => {
        user1.user = res.body;
      });
  });

  it('Utente1 prova a modificare il profilo di utente2', async () => {
    return request(app.getHttpServer())
      .put('/users/' + user2.user.id)
      .send({
        username: 'utente2nuovo',
      })
      .set({ Authorization: 'Bearer: ' + user1.token })
      .expect(403);
  });

  it('Utente1 prova a creare un nuovo utente3', async () => {
    return request(app.getHttpServer())
      .post('/users')
      .send({
        ...user3.credentials,
        role: RolesEnum.User,
      })
      .set({ Authorization: 'Bearer: ' + user1.token })
      .expect(403);
  });

  it('utente1 prova get admin1', async () => {
    return request(app.getHttpServer())
      .get('/users/' + admin1.user.id)
      .set({ Authorization: 'Bearer: ' + user1.token })
      .expect(403);
  });

  it('utente1 prova get utente2', async () => {
    return request(app.getHttpServer())
      .get('/users/' + user2.user.id)
      .set({ Authorization: 'Bearer: ' + user1.token })
      .expect(403);
  });

  it('utente1 get utente1', async () => {
    return request(app.getHttpServer())
      .get('/users/' + user1.user.id)
      .set({ Authorization: 'Bearer: ' + user1.token })
      .expect(200)
      .expect(user1.user);
  });

  it('admin2 get utente1', async () => {
    return request(app.getHttpServer())
      .get('/users/' + user1.user.id)
      .set({ Authorization: 'Bearer: ' + admin2.token })
      .expect(200)
      .expect(user1.user);
  });

  it('utente1 get utenti', async () => {
    return request(app.getHttpServer())
      .get('/users')
      .set({ Authorization: 'Bearer: ' + user1.token })
      .expect(403);
  });

  it('admin2 get utenti', async () => {
    return request(app.getHttpServer())
      .get('/users')
      .set({ Authorization: 'Bearer: ' + admin2.token })
      .expect(200)
      .expect([admin1.user, user1.user, user2.user, admin2.user]);
  });

  it('Signin Admin3', async () => {
    await request(app.getHttpServer())
      .post('/signin')
      .send({
        ...admin3.credentials,
        tenantName: 'Società2',
      })
      .expect(200);
  });

  it('Login Admin3', async () => {
    await request(app.getHttpServer())
      .post('/login')
      .send({
        ...admin3.credentials,
      })
      .expect(200)
      .then(res => {
        admin3.user = res.body.user;
        admin3.token = res.body.token;
      });
  });

  it('admin3 get utente1', async () => {
    return request(app.getHttpServer())
      .get('/users/' + user1.user.id)
      .set({ Authorization: 'Bearer: ' + admin3.token })
      .expect(404);
  });
});
